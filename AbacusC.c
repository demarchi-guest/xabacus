/*
 * @(#)AbacusC.c
 *
 * Copyright 1992 - 2017  David A. Bagley, bagleyd AT verizon.net
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Taken from a C++ group project where I was a lead contributer
 * OOP Group4!
 */

/* Methods string infix calculator file for Abacus */
#include "AbacusP.h"
#ifndef WINVER
extern int signgam;
#endif
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#ifndef M_E
#define M_E 2.7182818284590452354
#endif

/* proposed, implemented only a small portion
Letter	Meaning
------	-------
<Interrupt (^C etx 03)> kill program, or turn off calculator [quit]
<Back space (^H bs 010)> delete last digit [numeric]
<Line feed (^J lf 012), Carriage return (^M cr 015), Space (040)> [ignore]
!	factorial
"
#	number of values for statistics
$	summation
%	mod e.g. 7%2=1 [medium]
&	bitwise and [medium]
'
(	( [equate]
)	) [equate]
*	multipication [medium]
+	addition [low]
,	[numeric]
-	subtraction (not sign change) [low]
.	decimal point (or octal pt if in octal, etc.) [numeric]
/	divide e.g. 1/2=0.5 [medium]
0	0 [numeric]
1	1 [numeric]
2	2 [numeric]
3	3 [numeric]
4	4 [numeric]
5	5 [numeric]
6	6 [numeric]
7	7 [numeric]
8	8 [numeric]
9	9 [numeric]
:
;
<	bitwise shift left [high]
=	= [equate]
>	bitwise shift right [high]
?	data input for statistics
@	average
A	hexadecimal 10 [numeric]
B	hexadecimal 11 [numeric]
C	hexadecimal 12 [numeric]
D	hexadecimal 13 [numeric]
E	hexadecimal 14 [numeric]
F	hexadecimal 15 [numeric]
G	16 [numeric]
H	17 [numeric]
I	18 [numeric]
J	19 [numeric]
K
L	logarithm base 2
M	memory recall ('M', ('0'-'9' | 'A'-'F'))
N	natural logarithm
O	bitwise xor [high]
P	permutation [low]
Q	quit [quit]
R	"to the root of" e.g. 8R3=2 [high]
S	sample variance (s^2)
T	square summation
U	x^3
V	x^2
W	convert from MinSec
X	e^x (unfortunately 'e' & 'E' are used already)
Y	hyperbolic
Z	inverse
[
\	Pascal's div i.e 28\8=3 [medium]
]
^	"power of" e.g. 2^3=8 [high]
_	reset statistics
`
a	clear almost all [clear]
b	base mode ('b', (['2'-'9'] | '1',['0'-'6']))
c	combination [low]
d	decimal hot key (also 'b', '1', '0')
e	used for exponents e.g. 6.02*10^22 = 6.02e22(input) = 6.02 22(output)
f
g	gradient mode
h	hexadecimal hot key (also 'b', '1', '6')
i	invert 1/x
j	variance (sigma^2)
k	cosine (kosine)
l	logarithm base 10
m	memory ('m', ('0'-'9' | 'A'-'F'))
n	bitwise negation
o	degree mode
p	pi
q	clear everything (quit all calulations) [clear]
r	radian mode
s	sine
t	tangent
u	cube root
v	unary operation square root (v-) UNIX's dc & bc use this also
w	convert to MinSec
x	complex mode (base 10 only)
y
z	clear (zero) [clear]
{
|	bitwise or [low]
}
~	negate +/- [numeric (and also unary)]
*/
enum OrderType {notused, low, medium, high, unary, mode, constant, equate};
static const short unsigned int orderASCII[] =
{
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,
   notused,  notused,  notused,  notused,

   notused,    unary,  notused, constant, /* Space ! " # */
  constant,   medium,   medium,  notused, /* $ % & ' */
    equate,   equate,   medium,      low, /* ( ) * + */
   notused,      low,  notused,   medium, /* , - . / */
   notused,  notused,  notused,  notused, /* 0 1 2 3 */
   notused,  notused,  notused,  notused, /* 4 5 6 7 */
   notused,  notused,  notused,  notused, /* 8 9 : ; */
      high,   equate,     high,    unary, /* < = > ? */


  constant,  notused,  notused,  notused, /* @ A B C */
   notused,  notused,  notused, constant, /* D E F G */
   notused,  notused,  notused,  notused, /* H I J K */
     unary, constant,    unary,     high, /* L M N O */
       low,  notused,     high, constant, /* P Q R S */
  constant,    unary,    unary,    unary, /* T U V W */
     unary,     mode,     mode,  notused, /* X Y Z [ */
    medium,  notused,     high,     mode, /* \ ] ^ _ */

   notused,  notused,     mode,      low, /* ` a b c */
      mode, constant,  notused,     mode, /* d e f g */
      mode,    unary, constant,    unary, /* h i j k */
     unary,     mode,    unary,     mode, /* l m n o */
  constant,  notused,     mode,    unary, /* p q r s */
     unary,    unary,    unary,    unary, /* t u v w */
      mode,  notused,  notused,  notused, /* x y z { */
       low,  notused,    unary,  notused  /* | } ~ Delete */
};

enum OperationType {undefined, ignore, numeric, operation, clear, quit};
static const short unsigned int operationASCII[] =
{
  undefined, undefined, undefined,      quit, /* Null SOH STX Interrupt */
  undefined, undefined, undefined, undefined, /* EOT ENQ ACK Bell */
    numeric, undefined,    ignore, undefined, /* Backspace Tab Linefeed VT */
  undefined,    ignore, undefined, undefined, /* Formfeed Carriagereturn SO SI */
  undefined, undefined, undefined, undefined, /* DLE DC1 DC2 DC3 */
  undefined, undefined, undefined, undefined, /* DC4 NAK SYN ETB */
  undefined, undefined, undefined, undefined, /* CAN EM SUB Escape */
  undefined, undefined, undefined, undefined, /* FS GS RS US */

     ignore, operation, undefined, operation, /* Space ! " # */
  operation, operation, operation, undefined, /* $ % & ' */
  operation, operation, operation, operation, /* ( ) * + */
    numeric, operation,   numeric, operation, /* , - . / */
    numeric,   numeric,   numeric,   numeric, /* 0 1 2 3 */
    numeric,   numeric,   numeric,   numeric, /* 4 5 6 7 */
    numeric,   numeric, undefined, undefined, /* 8 9 : ; */
  operation, operation, operation, operation, /* < = > ? */

  operation,   numeric,   numeric,   numeric, /* @ A B C */
    numeric,   numeric,   numeric,   numeric, /* D E F G */
    numeric,   numeric,   numeric, undefined, /* H I J K */
  operation, operation, operation, operation, /* L M N O */
  operation,      quit, operation, operation, /* P Q R S */
  operation, operation, operation, operation, /* T U V W */
  operation, undefined, undefined, undefined, /* X Y Z [ */
  operation, undefined, operation, operation, /* \ ] ^ _ */

  undefined,     clear, operation, operation, /* ` a b c */
  operation, operation, undefined, operation, /* d e f g */
  operation, operation, operation, operation, /* h i j k */
  operation, operation, operation, operation, /* l m n o */
  operation,     clear, operation, operation, /* p q r s */
  operation, operation, operation, operation, /* t u v w */
  operation, undefined,     clear, undefined, /* x y z { */
  operation, undefined,   numeric, undefined  /* | } ~ Delete */
};

#define CHAR_TO_DIGIT(c) ((c >= 'A') ? c - 'A' + 10 : c - '0')
#define DIGIT_TO_CHAR(d) ((d >= 10) ? (char) ('A' + d - 10) : (char) ('0' + d))
#define IS_DIGIT(c) ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'J'))
#define IS_NUMBER(c) (IS_DIGIT(c) || c == decimalPoint)
/* True for period, +/-, or operator */
#define IS_VALID(v, b) ((IS_DIGIT(v)) ? (CHAR_TO_DIGIT(v) < b) : True)
#define MAX_VALUE_LENGTH 64
#define CALC_STRING_SIZE 256

enum Stackusage {paren, order};

typedef struct _Term
{
	double variable;
	char operation;
	Boolean orderusage;
	struct _Term *previous;
} Term;

typedef struct _Expression
{
	/* char *variable; */
	char variable[MAX_VALUE_LENGTH]; /* Hard code a limit for now */
	char operation;
	struct _Expression *previous;
} Expression;

/* struct Stack */
static Term *term;
#ifdef EXTRA
static Expression *expression;
#endif

#if 0
#define DEBUG 1
#endif
#define DEFAULT_BASE 10
#define DEFAULT_HALFBASE 5
#define DEFAULT_DECIMAL_DIGITS 16

/* Inputter & Parser & Paren Manager */
static Boolean period = False;
static Boolean gotFirstDigit = False; /* handles special case of just a '0' */
static Boolean negateNext = False; /* handles unary '-' */
static double left, right; /* sides of a binary operation */
static char pendingOperation; /* operation to be performed on left (& right if not unary) */
static Boolean hub; /* intermediate result? usually a ')' or pi pressed */
static char memoryBuf[CALC_STRING_SIZE];
static char displayBuf[CALC_STRING_SIZE];
static char stringBuf[CALC_STRING_SIZE];
static int digits = 1;
static int currentBase = DEFAULT_BASE, currentDigits = DEFAULT_DECIMAL_DIGITS;
static char decimalPoint = '.';
static char groupSymbol = ',';
static int nestingLevel = 0;
#ifdef EXTRA
static Expression parseExpression;
#endif

#ifdef WINVER
/* Some versions have, some do not, change to 1 if you do not */
#if 0
static double
pow(double x, double y)
{
	return exp(y * log(x));
}

static double
cbrt(double x) /* -inf < x < inf */
{
	return ((x < 0.0) ? -pow(-x, 1.0 / 3.0) : pow(x, 1.0 / 3.0));
}
#endif
#endif

static double
powInt(int base, int y)
{
	int i;
	double z = 1.0;

	if (y > 0)
		for (i = 0; i < y; i++)
			z *= base;
	else if (y < 0)
		for (i = 0; i > y; i--)
			z /= base;
	else
		z = 1.0;
	return z;
}

/* More exact (int) (log(x) / log(base)) */
static int
logInt(double x, int base)
{
	double quotient = x;
	int i = 0;

	if (quotient >= base) {
		while (quotient >= base) {
			quotient /= base;
			i++;
		}
		if (quotient + 0.0000000001 >= base)
			i++;
	} else if (quotient > 0.0) {
		while (quotient < 1.0) {
			quotient *= base;
			i--;
		}
	}
	return i;
}

#ifdef EXTRA
double
rootInt(double x, int y)
{
	/* y != 0 && (x >= 0 || (y odd)) */
	if (x < 0.0 && 2 * (y / 2) != y)
		return -pow(-x, 1.0 / y);
	else if (x <= 0.0)
		return 0.0;
	else
		return pow(x, 1.0 / y);
}
#endif

double
convertToDecimal(int base, char *inputString)
{
	int k = 0;
	Boolean negative = False;
	int digit;
	int length = 0;
	double number = 0.0;
	double factor;

	/* Convert Integer Part */
	k = 0;
	if (inputString[k] == '-') {
		negative = True;
		k++;
	}
	while (IS_DIGIT(inputString[k + length]))
		length++;
	factor = powInt(base, length);
	for (; length > 0; length--, k++) {
		digit = CHAR_TO_DIGIT(inputString[k]);
		factor /= base;
		number += factor * digit;
	}

	/* Convert Fractional Part */
	if (inputString[k] == decimalPoint) {
		k++;
		while (IS_DIGIT(inputString[k])) {
			digit = CHAR_TO_DIGIT(inputString[k]);
			factor /= base;
			number += factor * digit;
			k++;
		}
	}

	if (negative)
		number = -number;
	negative = False;

	return number;
}

void
convertFromInteger(char *outputString, int base, int x)
{
	char string[MAX_VALUE_LENGTH];
	int number = x;
	int placesBase;
	int l = 0, i = 0;
	short unsigned int digit;
	double factor;
	int fractDigits = currentDigits;

	(void) sprintf(string, "%d", number);
	if (string[i] == '-') {
		outputString[l++] = '-';
		number = -number;
	}
	while (string[i] != '\0') {
		i++;
	}
	{
		/* Chicken and egg problem:
		   rounding might increase placesBase */
		placesBase = logInt(number, base);
		fractDigits -= (placesBase + 1);
		/* rounder */
		number += (int) (pow((double) base, -fractDigits) / 2.0);

		/* Convert Integer Part */
		if (number < 1.0) {
			outputString[l++] = '0';
			factor = 1.0 / base;
		} else {
			placesBase = logInt(number, base);
			factor = pow((double) base, (double) placesBase);
			for (; placesBase >= 0; placesBase--) {
				digit = (short unsigned int) (number / factor);
				outputString[l++] = DIGIT_TO_CHAR(digit);
				number -= (int) (factor * digit);
				factor /= base;
			}
		}
	}
	outputString[l] = '\0';
}

void
convertFromDouble(char *outputString, int base, double x)
{
	char string[MAX_VALUE_LENGTH];
	Boolean localPeriod = False;
	double number = x;
	int placesBase;
	int l = 0, i = 0;
	short unsigned int digit;
	double factor;
	int fractDigits = currentDigits;

	(void) sprintf(string, "%g", number);
	if (string[i] == '-') {
		outputString[l++] = '-';
		number = -number;
	}
	while (string[i] != '\0') {
		if (string[i] == '.') /* DECIMAL_POINT LOCALE C */
			localPeriod = True;
		i++;
	}
	{
		/* Chicken and egg problem:
		   rounding might increase placesBase */
		placesBase = logInt(number, base);
		fractDigits -= (placesBase + 1);
		/* rounder */
		number += (pow((double) base, -fractDigits) / 2.0);

		/* Convert Integer Part */
		if (number < 1.0) {
			outputString[l++] = '0';
			factor = 1.0 / base;
		} else {
			placesBase = logInt(number, base);
			factor = pow((double) base, (double) placesBase);
			for (; placesBase >= 0; placesBase--) {
				digit = (short unsigned int) (number / factor);
				outputString[l++] = DIGIT_TO_CHAR(digit);
				number -= factor * digit;
				factor /= base;
			}
		}
		/* Convert Fractional Part */
		if (localPeriod) {
			outputString[l++] = decimalPoint;
			for (placesBase = 1; placesBase <= fractDigits;
					placesBase++) {
				digit = (short unsigned int) (number / factor);
				outputString[l++] = DIGIT_TO_CHAR(digit);
				number -= factor * digit;
				factor /= base;
			}
			while (outputString[l - 1] == '0')
				l--;
		}
	}
	outputString[l] = '\0';
}

static double
formatFromDisplay(int base, char *string)
{
	char floatString[MAX_VALUE_LENGTH];
	Boolean got1Digit = False;
	short unsigned int periods = 0;
	int s = 0, k = 0;

	while (string[s] != '\0') { /* Look at each input character */
		if (IS_DIGIT(string[s])) {
			got1Digit = True;
			floatString[k++] = string[s];
		} else if (string[s] == decimalPoint) {
			if (periods == 1)
				(void) printf("'%c' handler not implemented in Format_From_Display\n", string[s]);
			periods++;
			if (!got1Digit) {
				got1Digit = True;
				floatString[k++] ='0';
			}
			floatString[k++] = decimalPoint;
		} else if (string[s] == '-') {
			floatString[k++] = '-';
		} else {
			(void) printf("'%c' handler not implemented in Format_From_Display\n", string[s]);
		}
		s++;
	}
	if (!got1Digit) {
		got1Digit = True;
		floatString[k++] = '0';
	}
	floatString[k] = '\0';
	return convertToDecimal(base, floatString);
}

static void
formatToDisplay (char *string, double z, int base)
{
	int i = 0, j, length;
	short unsigned int periods = 0;

	convertFromDouble(string, base, z);
	length = (int) strlen(string);
	while (i <= length) {
		if (string[i] == '\0' && periods == 0) {
			j = length;
			while (j >= i) {
				string[j + 1] = string[j];
				j--;
			}
			length++;
			string[i++] = decimalPoint;
			periods++;
		}
		if (string[i] == decimalPoint)
			periods++;
		if (string[i] == '+') {
			j = i;
			while (j < length) {
				string[j] = string[j + 1];
				j++;
			}
			length--;
		}
		else
			i++;
	}
	string[length] = '\0';
}


static void reset(void)
{
	period = False;
	gotFirstDigit = False;
	digits = 1;
	memoryBuf[0] = '0';
	memoryBuf[1] = '\0';
}

static void setBase(int base)
{
	currentBase = base;
	currentDigits = logInt(powInt(DEFAULT_BASE, DEFAULT_DECIMAL_DIGITS), base);
}

static void setDecimalComma(Boolean dpc)
{
	if (dpc) {
		decimalPoint = ',';
		groupSymbol = '.';
	} else {
		decimalPoint = '.';
		groupSymbol = ',';
	}
}

#ifdef EXTRA
static int getNestingLevel(void)
{
	return nestingLevel;
}
#endif

static void incNestingLevel(void)
{
	nestingLevel++;
}

static void decNestingLevel(void)
{
	nestingLevel--;
	if (nestingLevel < 0) /* Ignore extra right parentheses */
		nestingLevel = 0;
}

static void resetNestingLevel(void)
{
	nestingLevel = 0;
}

static double
evaluateSingle(double arg, char unaryOperation)
{
	switch(unaryOperation) {
	case 'i':
		if (arg == 0.0) {
			return 0.0;
		}
		return 1.0 / arg;
	case '!':
#ifdef WINVER
		if ((double)((int) arg) == arg) {
			long long i, j;

			if (arg < 0.0 || arg > 20.0) {
				return 0.0;
			}
			j = 1;
			for (i = 2; i <= arg; i++) {
				j = j * i;
			}
			return (double) j;
		} else {
			return 0.0;
		}
#else
		if ((arg < 0.0 && ((int) arg) == arg) ||
				arg > 20.0) {
			/* this does not catch all the errors */
			return 0.0;
		} else {
			double lg;
#ifdef _REENT_ONLY
			int signgam_r;

			lg = lgamma_r(arg + 1.0);
			return signgam_r * exp(lg);
#else
			lg = lgamma(arg + 1.0);
			return signgam * exp(lg);
#endif
		}
#endif
	case 'v':
		if (arg <= 0.0) {
			return 0.0;
		}
		return sqrt(arg);
	case 'u':
		return cbrt(arg);
	default:
		return 0.0;
	}
}

static double
evaluateDouble(double arg1, char binaryOperation, double arg2)
{
	double comp = 0.0;

	switch(binaryOperation) {
	case '+':
		comp = arg1 + arg2;
		break;
	case '-':
		comp = arg1 - arg2;
		break;
	case '*':
		comp = arg1 * arg2;
		break;
	case '/':
		if (arg2 == 0.0)
			return 0.0;
		comp = arg1 / arg2;
		break;
	case '^':
		comp = pow(arg1, arg2);
		break;
	default:
		return 0.0;
	}
	return comp;
}

#ifdef EXTRA
static Boolean
newStack(void)
{
	Term *plate;

	if (!(plate = (Term *) malloc(sizeof(Term))))
		return False;
	plate->previous = NULL;
	term = plate;
	return True;
}
#endif

static Boolean
emptyStack(void)
{
	return ((Boolean) (term == NULL));
}

static void
flushStack(void)
{
	while (!emptyStack()) {
		Term *plate = term;

		term = plate->previous;
		free(plate);
	}
}

#ifdef EXTRA
static void
deleteStack(void)
{
	flushStack();
	/* free(term); */
}
#endif

static Boolean
pushStack(double z, char c, Boolean u)
{
	Term *plate;

	if (!(plate = (Term *) malloc(sizeof(Term))))
		return False;
	plate->variable = z;
	plate->operation = c;
	plate->orderusage = u;
	plate->previous = term;
	term = plate;
	return True;
}

static void popStack(double *z, char *c, Boolean *u)
{
	Term *plate = term;

	*z = term->variable;
	*c = term->operation;
	*u = term->orderusage;
	term = plate->previous;
	free(plate);
}

static char topOp(void)
{
	return term->operation;
}

static Boolean topusage(void)
{
	return term->orderusage;
}

static Boolean emptyExpression(void)
{
	return (emptyStack());
}

static Boolean previousOrder(void)
{
	return (!emptyExpression() && topusage() == order);
}

static Boolean canReduceExpression(char binaryOperation)
{
	return (previousOrder() && orderASCII[(int) topOp()] >=
		orderASCII[(int) binaryOperation]);
}

static void resetWholeExpression(void)
{
	flushStack();
}

static void getPreviousExpressionPart(double *myLeft, char *binaryOperation)
{
	Boolean dummy;

	popStack(myLeft, binaryOperation, &dummy);
}

static Boolean storeExpressionParen(double *myLeft, char *binaryOperation)
{
	if (pushStack(*myLeft, *binaryOperation, paren)) {
		*myLeft = 0.0;
		*binaryOperation = '\0';
		return True;
	}
	return False;
}

static void evaluateExpressionPart(double *myLeft, char binaryOperation,
		double myRight)
{
	if (binaryOperation == '\0')
		*myLeft = myRight;
	else
		*myLeft = evaluateDouble(*myLeft, binaryOperation, myRight);
}

static Boolean evaluateExpressionOrder(double *myLeft, char *binaryOperation,
	double *myRight, char newOperation)
{
	if (*binaryOperation == '\0') {
		*myLeft = *myRight;
	} else {
		if (orderASCII[(int) *binaryOperation] >=
				orderASCII[(int) newOperation]) {
			*myLeft = evaluateDouble(*myLeft, *binaryOperation,
				*myRight);
			while (canReduceExpression(newOperation)) {
				*myRight = *myLeft;
				getPreviousExpressionPart (myLeft,
					binaryOperation);
				evaluateExpressionPart(myLeft,
					*binaryOperation, *myRight);
			}
		} else {
			if (pushStack(*myLeft, *binaryOperation, order)) {
				*myLeft = *myRight;
				return True;
			} else {
				return False;
			}
		}
	}
	return True;
}

static void evaluateExpressionParen(double *myLeft, char *binaryOperation)
{
	double myRight;

	while (!emptyExpression() && topusage() == order) {
		myRight = *myLeft;
		getPreviousExpressionPart(myLeft, binaryOperation);
		evaluateExpressionPart(myLeft, *binaryOperation, myRight);
	}
}

static void evaluateExpression(double *myLeft, char *binaryOperation)
{
	double myRight;

	while (!emptyExpression()) {
		myRight = *myLeft;
		getPreviousExpressionPart(myLeft, binaryOperation);
		if (binaryOperation == NULL)
			*myLeft = myRight;
		else
			*myLeft = evaluateDouble(*myLeft, *binaryOperation,
				myRight);
	} /* ignore uneven "(()" */
	*binaryOperation = '\0';
}

#ifdef EXTRA
static void
initParser(void)
{
	left = 0.0;
	right = 0.0;
	pendingOperation = '\0';
	hub = False;
	/* FIXME expression = Expression(); */
}

static void
resetRight(void)
{
	right = 0.0;
	hub = True;
}
#endif

static void
resetExpression(void)
{
	right = left = 0.0;
	pendingOperation = '\0';
	hub = False;
	resetWholeExpression();
}

static void
parse(double input, Boolean gotNumeral, char newOperation)
{
	Boolean numeral = gotNumeral;

	if (numeral) {
		if (negateNext) {
			negateNext = False;
			right = -input;
			pendingOperation = '\0';
		} else {
			right = input;
		}
	} else if (hub) {
		numeral = True;
		hub = False;
	}
	if (orderASCII[(int) newOperation] == equate) {
		switch (newOperation) {
		case '(':
			incNestingLevel();
			(void) storeExpressionParen(&left, &pendingOperation);
			right = left; /* "#(" # is lost forever */
			break;
		case ')': /* Evaluate last term and restore variable and operation. */
			decNestingLevel();
			if (pendingOperation != '\0' && numeral) /* this stops 4+) = 8 */
				left = evaluateDouble(left, pendingOperation, right);
			if (pendingOperation == '\0' && numeral)
				left = right;
			else {
				evaluateExpressionParen(&left, &pendingOperation);
				pendingOperation = '\0';
				right = left;
				if (!emptyExpression()) /* ignore uneven "())" */
				{
					getPreviousExpressionPart (&left, &pendingOperation);
					hub = True;
				}
			}
			break;
		case '=':
			/* Evaluate term and restore variable and operation. */
			resetNestingLevel();
			if (pendingOperation != '\0' && numeral) {
				/* this stops 4+= = 8 (2+2) */
				left = evaluateDouble(left, pendingOperation,
					right);
			}
			if (pendingOperation == '\0' && numeral) {
				left = right;
			} else {
				evaluateExpression(&left, &pendingOperation);
				right = left;
			}
			break;
		default:
			resetExpression();
		}
	} else if (orderASCII[(int) newOperation] == constant) {
		switch (newOperation) {
		case 'e':	/* natural logarithm */
			right = M_E;
			break;
		case 'p':	/* pi */
			right = M_PI;
			break;
		default:
			resetExpression();
		}
		hub = True;
	} else if (orderASCII[(int) newOperation] == unary) {
		right = evaluateSingle(right, newOperation);
		if (pendingOperation == '\0')
			left = right;
		hub = True;
	} else if (orderASCII[(int) newOperation] == notused) {
		resetExpression();
	} else {
		/* binary operation */
		if (numeral) {
			(void) evaluateExpressionOrder(&left,
				&pendingOperation, &right, newOperation);
			right = left;
		} else {
			if (previousOrder()) /* this stops 2*+ = 6 (2*2+2) */ {
				getPreviousExpressionPart(&left,
					&pendingOperation);
				(void) evaluateExpressionOrder(&left,
					&pendingOperation, &right,
					newOperation);
				right = left;
			} else if (pendingOperation != '\0') {
				resetExpression();
			} else if (newOperation == '-') {
				negateNext = True;
			}
		}
		pendingOperation = newOperation;
	}
	formatToDisplay(memoryBuf, right, currentBase);
	(void) strcpy(displayBuf, memoryBuf); /* update display */
}

static void
inputOperator(char input)
{
	if (gotFirstDigit) {
		parse(formatFromDisplay(currentBase, memoryBuf), True, input);
	} else {
		parse(0.0, False, input);
	}
	reset();
}

static Boolean
inputNumeric(char input)
{
	int i = digits - 1, j;

	if (!IS_VALID(input, currentBase)) {
		reset();
		return False;
	}
	if (IS_DIGIT(input)) {
		gotFirstDigit = True;
		if (memoryBuf[0] == '0' && memoryBuf[1] == '\0') {
			memoryBuf[0] = input;
		} else {
			memoryBuf[digits] = input;
			digits++;
			memoryBuf[digits] = '\0';
		}
	} else if (input == decimalPoint) {
		if (period) {
			reset(); /* Handle multiple decimalPoint */
		}
		period = True;
		memoryBuf[digits] = input;
		digits++;
		memoryBuf[digits] = '\0';
	} else if (input == '~') { /* Handle +/- */
		/* Note: if `memoryBuf == "0"' then "+/-" should act
		   as a unary operation, that is, if no number is entered,
		   it should act on the result. */
		if (!gotFirstDigit) {
			inputOperator ('~');
			return False;
		}
		if (memoryBuf[0] == '0' && memoryBuf[1] == '\0')
			return False;
		while (memoryBuf[i] != ' ' && memoryBuf[i] != '-' &&
			memoryBuf[i] != groupSymbol && i != 0)
			i--;
		if (memoryBuf[i] == '-') {
			/* Take a '-' from front of a negative number */
			while (memoryBuf[i] != '\0') {
				memoryBuf[i] = memoryBuf[i + 1];
				i++;
			}
			digits--;
		} else {
			/* Put the '-' in the front of a positive number */
			if (digits == MAX_VALUE_LENGTH - 1)
				return False;
			j = digits;
			while (j >= i) {
				memoryBuf[j + 1] = memoryBuf[j];
				j--;
			}
			memoryBuf[i] = '-';
			digits++;
		}
	} else {
		reset();
		return False;
	}
	return True;
}

void
convertStringToAbacus(AbacusWidget w, const char *string, int aux)
{
	int decimal = 0;
	int num, factor, i, sign = 0, len = (int) strlen(string);
	AbacusWidget *wap = &w, wa;

	if (aux == 1 && w->abacus.leftAux != NULL)
		wap = (AbacusWidget *) w->abacus.leftAux;
	else if (aux == 2 && w->abacus.rightAux != NULL)
		wap = (AbacusWidget *) w->abacus.rightAux;
	wa = *wap;
	if (aux == 0) {
		clearRails(w);
#ifndef WINVER
	} else {
		clearAuxRails(w, aux);
#endif
	}
	while (string[decimal] != '\0' && string[decimal] != decimalPoint)
		decimal++;
	if (string[0] == '-' || string[0] == '+') {
		sign = 1;
	}
	for (i = 0; i < decimal - sign; i++) {
		/* wa->abacus.displayBase == wa->abacus.base and all that ... */
		num = char2Int(string[decimal - 1 - i]);
		if (wa->abacus.decks[BOTTOM].number <= wa->abacus.base / 2) {
			factor = num / wa->abacus.decks[TOP].factor;
			if (factor > 0) {
				setAbacusMove(w, ACTION_MOVE, aux, 1, i, factor);
				num = num - factor * wa->abacus.decks[TOP].factor;
			}
		}
		factor = num / wa->abacus.decks[BOTTOM].factor;
		if (factor > 0) {
			setAbacusMove(w, ACTION_MOVE, aux, 0, i, factor);
		}
	}
	if (wa->abacus.sign && string[0] == '-') {
		setAbacusMove(w, ACTION_MOVE, aux, 0, wa->abacus.rails -
			wa->abacus.decimalPosition - 1, 1);
	}
	for (i = 0; i < len - decimal - 1 &&
			i < wa->abacus.decimalPosition; i++) {
		int offset = 0;

		num = char2Int(string[decimal + i + 1]);
		if  (wa->abacus.decks[BOTTOM].piece != 0)
			offset++;
		if  (wa->abacus.decks[BOTTOM].piecePercent != 0 &&
				i >= wa->abacus.shiftPercent)
			offset++;
		if (wa->abacus.decks[BOTTOM].number <= wa->abacus.base / 2) {
			factor = num / wa->abacus.decks[TOP].factor;
			if (factor > 0) {
				setAbacusMove(w, ACTION_MOVE, aux, 1, -i - 1 - offset, factor);
				num = num - factor * wa->abacus.decks[TOP].factor;
			}
		}
		factor = num / wa->abacus.decks[BOTTOM].factor;
		if (factor > 0) {
			setAbacusMove(w, ACTION_MOVE, aux, 0, -i - 1 - offset, factor);
		}
	}
}

void
addBackAnomaly(char * buf, int anomaly, int shift, int base)
{
	double anom;
	int factor;
	int shiftValue = (int) multPower(1, base, shift);
	int anomalyValue = anomaly * shiftValue / base;

	anom = convertToDecimal(base, buf);
	factor = (int) (anom / (shiftValue - anomalyValue));
	anom = anom + factor * anomalyValue;
	convertFromDouble(buf, base, anom);
}

void
zeroFractionalPart(char * buf)
{
	unsigned int i;

	for (i = 0; i < strlen(buf); i++) {
		if (buf[i] == '.') {
			buf[i + 1] = '\0';
			return;
		}
	}
}

char *strAddChar(char *buf, char c)
{
	char *s;

	s = buf;
	s = s + strlen(buf); /* the position to write */
	*s = c; /* this should be on the place where the null char first was. */
	s++;
	*s = '\0'; /* add a new null string */
	return buf;
}

char *strAddInt(char *buf, int value)
{
	char *s;

	s = buf;
	s = s + strlen(buf); /* the position to write */
	(void) sprintf(s, "%d", value); /* always base 10 */
	return buf;
}

static void
convertStringBases(char *sb, char *buf, int displayBase, int base) {
	int i;

	for (i = 0; buf[i] != '\0'; i++) {
		if (IS_NUMBER(buf[i])) {
			char  numberString[CALC_STRING_SIZE];
			double num;
			int j = 0;

			while (buf[i] != '\0' && IS_NUMBER(buf[i])) {
				numberString[j] = buf[i];
				i++;
				j++;
			}
			i--;
			numberString[j] = '\0';
			num = convertToDecimal(displayBase,
				numberString);
			convertFromDouble(numberString, base, num);
			(void) strcat(sb, numberString);
#ifdef DEBUG
			(void) printf("ns =%s\n", numberString);
#endif
		} else {
			sb = strAddChar(sb, buf[i]);
		}
	}
}

void calculate(AbacusWidget w, char * buffer, int aux)
{
	unsigned int i;

#ifdef DEBUG
	(void) printf("buffer =%s\n", buffer);
#endif
	memoryBuf[0] = '0';
	memoryBuf[1] = '\0';
	displayBuf[0] = '0';
	displayBuf[1] = '\0';
	stringBuf[0] = '0';
	stringBuf[1] = '\0';
	setBase(w->abacus.base);
	setDecimalComma(w->abacus.decimalComma);
	if (w->abacus.displayBase != w->abacus.base) {
		convertStringBases(stringBuf, buffer, w->abacus.displayBase,
			w->abacus.base);
#ifdef DEBUG
		(void) printf("conv buffer =%s\n", stringBuf);
#endif
	} else {
		(void) strcpy(stringBuf, buffer);
	}
	resetExpression();
	for (i = 0; i < strlen(stringBuf); i++) {
		switch (operationASCII[(int) stringBuf[i]]) {
		case undefined:
#ifdef DEBUG
			(void) printf("undefined\n");
#endif
			break;
		case ignore:
#ifdef DEBUG
			(void) printf("ignore\n");
#endif
			break;
		case numeric:
#ifdef DEBUG
			if (stringBuf[i] == groupSymbol)
				(void) printf("ignore\n");
			else
				(void) printf("number %c %s\n", stringBuf[i], memoryBuf);
#endif
			if (stringBuf[i] != groupSymbol)
				(void) inputNumeric(stringBuf[i]);
			break;
		case operation:
#ifdef DEBUG
			(void) printf("operate %c %s\n", stringBuf[i], memoryBuf);
#endif
			inputOperator(stringBuf[i]);
			break;
		case clear:
#ifdef DEBUG
			(void) printf("clear\n");
#endif
			break;
		default:
#ifdef DEBUG
			(void) printf("QUIT\n");
#endif
			break;
		}
	}
	inputOperator('=');
#ifdef DEBUG
	(void) printf("display =%s\n", displayBuf);
#endif
	if (w->abacus.anomaly != 0) {
		addBackAnomaly(displayBuf, w->abacus.anomaly,
			w->abacus.shiftAnomaly,
			w->abacus.base);
	}
	if (w->abacus.anomalySq != 0) {
		addBackAnomaly(displayBuf, w->abacus.anomalySq,
			w->abacus.shiftAnomaly + w->abacus.shiftAnomalySq,
			w->abacus.base);
	}
	if (w->abacus.subdeck != 0) {
		zeroFractionalPart(displayBuf);
	}
	convertStringToAbacus(w, displayBuf, aux);
}
