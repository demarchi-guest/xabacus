/*
 * @(#)AbacusP.h
 *
 * Copyright 1994 - 2018  David A. Bagley, bagleyd AT verizon.net
 *
 * Abacus demo and neat pointers from
 * Copyright 1991 - 1998  Luis Fernandes, elf AT ee.ryerson.ca
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Private header file for Abacus */

#ifndef _AbacusP_h
#define _AbacusP_h
#include "xwin.h"
#include "timer.h"
#include "Abacus.h"

#ifdef HAVE_LONG_LONG
#define LONG long long
#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX 9223372036854775807LL
#endif
#define MAX_INT LONG_LONG_MAX
#else
#define LONG long
#ifndef LONG_MAX
#define LONG_MAX 2147483647L
#endif
#define MAX_INT LONG_MAX
#endif

#ifdef WINVER
#ifndef DEMOPATH
#ifdef UNIXDELIM
#define DEMOPATH "c:/WINDOWS"
#else
#define DEMOPATH "c:\\WINDOWS"
#endif
#endif

extern void destroyAbacus(AbacusWidget w, HBRUSH brush);
extern void resizeAbacus(AbacusWidget w);
extern void initializeAbacus(AbacusWidget w, HBRUSH brush);
extern void exposeAbacus(AbacusWidget w);
extern void hideAbacus(AbacusWidget w);
extern void selectAbacus(AbacusWidget w, const int x, const int y);
extern void releaseAbacus(AbacusWidget w, const int x, const int y);
extern void clearAbacus(AbacusWidget w);
extern void clearDecimalAbacus(AbacusWidget w);
extern void complementAbacus(AbacusWidget w);
extern void incrementAbacus(AbacusWidget w);
extern void decrementAbacus(AbacusWidget w);
extern void changeFormatAbacus(AbacusWidget w);
extern void changeMuseumAbacus(AbacusWidget w);
extern void toggleRomanNumeralsAbacus(AbacusWidget w);
extern void toggleAncientRomanAbacus(AbacusWidget w);
extern void toggleLatinAbacus(AbacusWidget w);
extern void toggleGroupingAbacus(AbacusWidget w);
extern void toggleNegativeSignAbacus(AbacusWidget w);
extern void toggleQuartersAbacus(AbacusWidget w);
extern void toggleQuarterPercentsAbacus(AbacusWidget w);
extern void toggleTwelfthsAbacus(AbacusWidget w);
extern void toggleSubdecksAbacus(AbacusWidget w);
extern void toggleModernRomanAbacus(AbacusWidget w);
extern void toggleEighthsAbacus(AbacusWidget w);
extern void toggleAnomalyAbacus(AbacusWidget w);
extern void toggleWatchAbacus(AbacusWidget w);
extern void speedUpAbacus(AbacusWidget w);
extern void slowDownAbacus(AbacusWidget w);
extern void toggleSoundAbacus(AbacusWidget w);
extern void toggleRightToLeftAddAbacus(AbacusWidget w);
extern void toggleRightToLeftMultAbacus(AbacusWidget w);
extern void enterAbacus(AbacusWidget w);
extern void leaveAbacus(AbacusWidget w);
extern void moveAbacusInput(AbacusWidget w,
        int x, int y, char letter, int control);
extern Boolean checkSubdeck(AbacusWidget w, int position);
extern void setAbacusString(AbacusWidget w, int reason, char *string);

extern void initializeAbacusDemo(AbacusWidget w);
extern void exposeAbacusDemo(AbacusWidget w);
extern void clearAbacusDemo(AbacusWidget w);
extern void toggleDemoAbacusDemo(AbacusWidget w);
extern void showNextAbacusDemo(AbacusWidget w);
extern void showRepeatAbacusDemo(AbacusWidget w);
extern void showJumpAbacusDemo(AbacusWidget w);
extern void showMoreAbacusDemo(AbacusWidget w);
extern void showChapterAbacusDemo(AbacusWidget w, unsigned int chapt);

#else
extern Pixel darker(AbacusWidget w, Pixel pixel);

#ifdef VMS
#ifndef DEMOPATH
#define DEMOPATH "[.]"
#endif
#else
#ifndef DEMOPATH
#if 0
#define DEMOPATH "/usr/share/games/xabacus"
#endif
#define DEMOPATH "/usr/local/share/games/xabacus"
#endif
#endif

typedef struct _AbacusClassPart {
	int         ignore;
} AbacusClassPart;

typedef struct _AbacusClassRec {
	CoreClassPart coreClass;
	AbacusClassPart abacusClass;
} AbacusClassRec;

extern AbacusClassRec abacusClassRec;
#endif

#ifndef BUMPSOUND
#define BUMPSOUND "bump"
#endif
#ifndef MOVESOUND
#define MOVESOUND "move"
#endif
#ifndef DRIPSOUND
#define DRIPSOUND "drip"
#endif
#ifndef XML_FILE
#define XML_FILE "abacusDemo.xml"
#endif

#define MAX_DECKS 2
#define PLACE_SETTING 2
#define UP 1
#define DOWN 0
#define TOP 1
#define BOTTOM 0
#define CARRY 1
#define SPECIAL_PIECES 2 /* Pieces, PiecePercents */

#define MAX_FORMAT_LENGTH 9
#define CHAPTERS 5

#define LINES 4
#define CHARS 64

#define MAX_SLICES 10
#define COUNTRY_SIZE 3
#define STRING_SIZE 256
#define MAX_BEAD_SHADES 12
#define MAX_RAIL_SHADES 12

#define NORMAL 1
#define DOUBLE 2
#define INSTANT 3

#define NEWPOS(dir,x) ((((dir)==UP)?-1:1)*(x))
#define VDRAWLINE(w,dr,c,x1,y1,x2,y2) if (w->abacus.vertical) \
	{DRAWLINE(w,dr,c,y1,x1,y2,x2);} else {DRAWLINE(w,dr,c,x1,y1,x2,y2);}
#define VDRAWRECTANGLE(w,dr,c,i,j,l,h) if (w->abacus.vertical) \
	{DRAWRECTANGLE(w,dr,c,j,i,h,l);} else {DRAWRECTANGLE(w,dr,c,i,j,l,h);}
#define VFILLRECTANGLE(w,dr,c,i,j,l,h) if (w->abacus.vertical) \
	{FILLRECTANGLE(w,dr,c,j,i,h,l);} else {FILLRECTANGLE(w,dr,c,i,j,l,h);}
#define VDRAWCIRCLE(w,dr,c,d,x,y) if (w->abacus.vertical) \
	{DRAWCIRCLE(w,dr,c,d,y,x);} else {DRAWCIRCLE(w,dr,c,d,x,y);}
#define VFILLCIRCLE(w,dr,c,d,x,y) if (w->abacus.vertical) \
	{FILLCIRCLE(w,dr,c,d,y,x);} else {FILLCIRCLE(w,dr,c,d,x,y);}
#define VPOLYGON(w,dr,c,cl,l,n,b1,b2) if (w->abacus.vertical) \
	{int i,t; for (i=0;i<=n;i++) {t=l[i].x; l[i].x=l[i].y; l[i].y=t;} \
	POLYGON(w,dr,c,cl,l,n,b1,b2);} else {POLYGON(w,dr,c,cl,l,n,b1,b2);}
#define vfillRectClip(w,dr,c,dx,dy,sx,sy,o,wo,ws) if (w->abacus.vertical) \
	{fillRectClipX(w,dr,c,dy,dx,sy,sx,o,wo,ws);} else \
	{fillRectClipY(w,dr,c,dx,dy,sx,sy,o,wo,ws);}

typedef struct _MuseumPart {
	char        museum[MAX_MUSEUMS];	/* 2 char country code of museum */
	Boolean     separateSlots;
} MuseumPart;

typedef struct _SubdeckPart {
	int         number;
	int         factor;
	int         position;
	int         spaces;	/* spaces between beads */
	int         room;	/* spaces + number */
	Position    height;
} SubdeckPart;

typedef struct _DeckPart {
	int         number;
	Boolean     orientation;
	int         factor;
	int        *position;
	int         spaces;	/* spaces between beads */
	int         room;	/* spaces + number */
	Position    height;
	int         piece, piecePercent;
} DeckPart;

typedef struct _AbacusPart {
	DeckPart    decks[MAX_DECKS];
	SubdeckPart *subdecks;
	int         currentDeck, currentRail, currentPosition;
	int         rails;	/* number of columns of beads */
	int         decimalPosition, numDigits;
	int         base, displayBase;	/* 10 usually */
	int         subdeck, subbead, subbase;
	int         shiftPercent, shiftAnomaly, shiftAnomalySq;
	int         groupSize, anomaly, anomalySq;
	unsigned int mode;
	int         delay, numSlices, submode, colorScheme, railIndex;
	int         leftAuxRails, rightAuxRails;
	int         aux, deck, rail, number;
	Position    middleBarHeight, middleBarPositionY, railWidth;
	Point       frameSize, beadSize;
	Point       pos, totalSize, delta, offset, pressOffset;
	Boolean     sign, minusSign, romanNumerals, group, decimalComma;
	Boolean     carryAnomaly, carryAnomalySq;
	Boolean     mono, reverse, focus, vertical;
	Boolean     script, demo, teach, lee;
	Boolean     latin, modernRoman, ancientRoman;
	Boolean     versionOnly;
	Boolean     slot, diamond, sound;
	char       *digits, *mathBuffer;
	FILE       *fp;
	GC          frameGC, borderGC, symbolGC, lineGC, inverseGC;
	GC          beadShadeGC[MAX_BEAD_SHADES];
	GC          railShadeGC[MAX_BEAD_SHADES];
	Pixmap      bufferBead[4][2][2];
	TimeVal     oldTime;
	Boolean     rightToLeftAdd, rightToLeftMult;
	int         step, carry[2], lower, upper, carryStep;
	int         state; /* explain or carrying out operation */
	int         reg, regCount, qDigit, qPosition, auxCount, primarySteps;
	double      divisor;
	char        op;
	int        *intGroup;
	struct _AbacusPart *leftAux, *rightAux;
#ifdef WINVER
	char        format[STRING_SIZE], museum[COUNTRY_SIZE];
	char        bumpSound[STRING_SIZE], moveSound[STRING_SIZE], dripSound[STRING_SIZE];
	char        aString[STRING_SIZE], bString[STRING_SIZE];
	char        cString[STRING_SIZE], rString[STRING_SIZE];
	char        sString[STRING_SIZE];
	char        helperBuffer[STRING_SIZE];
#else
	char       *format, *museum, *bumpSound, *moveSound, *dripSound;
	char       *aString, *bString, *cString, *rString, *sString;
	char       *teachBuffer;
	int         menu, pixmapSize;
	Colormap    colormap;
	Pixel       foreground, background;
	Pixel       borderColor, frameColor;
	Pixel       beadColor[4], railColor[4];
	Pixel       leftAuxColor, rightAuxColor;
	XtCallbackList select;
#endif
} AbacusPart;

typedef struct _AbacusDemoPart {
	unsigned int bookCount, chapterCount, lessonCount, moveCount;
	int         aux, deck, rail, number, lines;
	int         highlightAux, highlightRail;
	int         fontHeight;
	FILE       *fp;
	Boolean     book, chapter, query, started, framed;
	GC          foregroundGC;
#ifdef WINVER
	char        path[STRING_SIZE];
#else
	GC          inverseGC;
	char       *path, *font;
	XFontStruct *fontInfo;
	Pixel       background, foreground;
	XtCallbackList select;
#endif
} AbacusDemoPart;

typedef struct _AbacusRec {
	CorePart    core;
	AbacusPart  abacus;
	AbacusDemoPart abacusDemo;
} AbacusRec;

struct moveType {
	char *code;
	unsigned int lines;
	char **lineText;
};

struct lessonType {
	char *name;
	unsigned int moves;
	struct moveType *move;
};

struct chapterType {
	char *name;
	unsigned int lessons;
	struct lessonType *lesson;
};

struct editionType {
	char *version;
	unsigned int chapters;
	struct chapterType *chapter;
};

struct bookType {
	char *name;
	char *author;
	unsigned int editions;
	struct editionType *edition;
};

struct libraryType {
	unsigned int books;
	struct bookType *book;
};

struct libraryType abacusLibrary;

extern unsigned int getFallbackBooks(void);
extern unsigned int getFallbackEditions(void);
extern unsigned int getFallbackChapters(void);
extern unsigned int getFallbackLessons(unsigned int chapter);
extern unsigned int getFallbackMoves(unsigned int edition, unsigned int chapter, unsigned int lesson);

extern const char *bookTextFallback[1][LINES];
extern const char *chapterTextFallback[1][5][LINES];
extern const char *lessonTextFallback[1][4][5][2][41][LINES + 1];
extern void freeAbacus(struct bookType *bookAbacus, int books);

extern void setAbacus(AbacusWidget w, int reason);
extern void setAbacusDemo(AbacusWidget w, int reason);
extern void setAbacusMove(AbacusWidget w, int reason, int aux,
	int deck, int rail, int number);
extern void setAbacusHighlightRail(AbacusWidget w, int reason, int aux, int rail);
extern void setAbacusHighlightRails(AbacusWidget w, int aux);
extern void setAbacusText(AbacusWidget w, int reason, char * text, int line);
extern int char2Int(char character);
extern char int2Char(int digit);
extern char *strAddChar(char *buf, char c);
extern char *strAddInt(char *buf, int value);

extern void dividePieces(char * buf, int base, int pieces,
	int mult, int places, char decimalPoint);
extern void shiftDecimal(char *buf, char *aBuf, int shift, int place,
	char decimalPoint);
extern void addStrings(char *finalBuf, char *buf, char *pieceBuf,
	int base, char decimalPoint);
extern void subStrings(char *finalBuf, char *buf, char *pieceBuf,
	int base);
extern void convertString(char * buf, char * inbuf,
	int base, int displayBase, int decimalPosition,
	int anomaly, int shiftAnomaly, Boolean carryAnomaly,
	int anomalySq, int shiftAnomalySq, Boolean carryAnomalySq,
	char decimalPoint);

extern int sizeofRoman(int base, Boolean romanNumerals, Boolean ancientRoman);
extern int string2Roman(char *buf, char *inbuf, int base, Boolean decimalFraction,
	int pieces, int number, int subnumber, int subbase,
	char decimalPoint, Boolean ancientRoman, Boolean latin);
extern void string2Group(char *buf, char *inbuf, int groupSize,
	char decimalPoint, char groupSeparator);
extern void calculate(AbacusWidget w, char *buffer, int aux);
extern void teachStep(AbacusWidget w, char *buffer, int aux);
extern LONG multPower(int m, int x, int n);
extern Boolean checkPiece(AbacusWidget w);
extern Boolean checkPiecePercent(AbacusWidget w);
extern Boolean checkSubdeck(AbacusWidget w, int position);
extern void drawBeadRail(AbacusWidget w, int rail, Boolean highlight);
extern int convertBaseToBottom(int base);
extern int rootInt(int i, int n);

extern void clearRails(AbacusWidget w);
extern void clearDecimalRails(AbacusWidget w);
extern void addBackAnomaly(char * buf, int anomaly, int shift, int base);
extern void zeroFractionalPart(char * buf);
extern void convertStringToAbacus(AbacusWidget w, const char *string, int aux);
extern double convertToDecimal(int base, char *inputString);
extern void convertFromInteger(char *outputString, int base, int x);
extern void convertFromDouble(char *outputString, int base, double x);
#ifdef WINVER
extern void drawDemoText(const char* line, int i);
extern void drawTeachText(const char* line, int i);
#else
extern void clearAuxRails(AbacusWidget w, int aux);
#endif

extern void highlightRail(AbacusWidget w, int aux, int rail,
	Boolean highlight);
extern void highlightRails(AbacusWidget w, int aux);
extern void readParseFile(const char * fileName);

#endif /* _AbacusP_h */
