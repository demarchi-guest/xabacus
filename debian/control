Source: xabacus
Section: games
Priority: optional
Maintainer: Innocent De Marchi <tangram.peces@gmail.com>
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 11),
               dpkg-dev (>= 1.16.1~),
               libaudiofile-dev,
               libaudio-dev,
               libmotif-dev,
               libxpm-dev,
               libxt-dev
Homepage: http://www.sillycycle.com/abacus.html
Vcs-Browser: https://salsa.debian.org/demarchi-guest/xabacus
Vcs-Git: https://salsa.debian.org/demarchi-guest/xabacus.git

Package: xabacus
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: xabacus
Conflicts: xabacus
Replaces: xabacus
Description: simulation of the ancient calculator (plain X version)
 This program is an implementation of the original abacus, it provides
 the Chinese, Japanese, Korean, Roman and Russian version and can be
 modified to allow others.
 .
 This version was compiled without the Motif GUI widget library and thus
 shows limited user interface functionality. See xmabacus for the
 extended version.

Package: xmabacus
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: xabacus
Conflicts: xabacus
Replaces: xabacus
Description: simulation of the ancient calculator (Motif version)
 This program is an implementation of the original abacus, it provides
 the Chinese, Japanese, Korean, Roman and Russian version and can be
 modified to allow others.
 .
 This is the Motif version which shows additional functionality. Motif
 is a GUI widget library for the X Window system.
