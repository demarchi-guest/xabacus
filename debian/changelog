xabacus (8.1.7+dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Update Standards-Version (4.1.4).
  * Write secure copyright format uri on debian/copyright file.
  * Added Files-Excluded and Comment fields on debian/copyright.
  * Migrated repository to salsa.
  * Added debian/upstream/metadata file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sat, 14 Apr 2018 10:09:24 +0100

xabacus (8.1.6+dfsg1-1) unstable; urgency=medium

  * New upstream version.
    + Updated years on debian/copyright.
  * Deleted unnecessary sources directory win32 and repackaging.
  * Update Standards-Version (4.1.3):
    * Bumped to debhelper 11.
  * Removed unnecessary move-play-file-config.patch file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sat, 10 Feb 2018 18:28:55 +0100

xabacus (8.1.5+dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Deleted unnecessary sources directory win32 and repackaging.
  * Update Standards-Version (4.1.2): no changes required.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Fri, 08 Dec 2017 17:05:21 +0100

xabacus (8.1.4+dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Deleted unnecessary sources directory win32 and repackaging.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Fri, 29 Oct 2017 11:11:28 +0100

xabacus (8.1.3+dfsg1-1) unstable; urgency=medium

  * New maintainer (Closes: #870519).
  * Deleted unnecessary sources directory win32 and repackaging.
  * Update debian/watch file to version 4 and added dversionmangle
    option.
  * Update Standards-Version (4.1.1):
    - Removed autotools-dev dependency on debian/control.
  * Changed debian/rules to make the build reproducible,
    thanks Chris Lamb (Closes: #865994).
  * Changed the Priority field to optional from xmabacus
    on debian/control.
  * Removed duplicate field Priority on debian/control.
  * Added Keywords field on debian/*.desktop files.
  * Removed obsolete menu files on debian directory.
  * Re-written the debian/copyright file to the standard 1.0 format.
  * Install play.sh file on /usr/share/games/xabacus/:
    + Added move-play-file.patch file to change installation directory.
    + Added configuration flag on debian/rules.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sat, 30 Sep 2017 18:20:12 +0100

xabacus (8.0.8-1) unstable; urgency=medium

  * [23dfa61] change of xabacus hosting, adjusting debian/* accordingly
  * [74e3767] New upstream version 8.0.8
  * [8b58ec7] debian/copyright / debian/*doc-base: update author and copyright year
  * [ec39d90] change sound system from esound to nas (Closes: 856091)
  * [340a9a2] debian/control: update Vcs-* fields
  * [e04c568] drop lesstif2 from xmabacus package description and menu
  * [9b830c6] use up-to-date debhelper compatibility level
  * [2898189] debian/control: update Standards-Version (no changes required)

 -- Florian Ernst <florian@debian.org>  Sun, 25 Jun 2017 11:55:30 +0200

xabacus (8.0.2-1) unstable; urgency=medium

  * [215d7b8] Imported Upstream version 8.0.2
  * [dc30ec9] debian/control: Standards-Version: 3.9.6 (no changes required)

 -- Florian Ernst <florian@debian.org>  Sun, 26 Apr 2015 11:45:13 +0200

xabacus (8.0.0-2) unstable; urgency=medium

  * [a2601b4] fix xmabacus.desktop to refer to the correct binary and icon
  * [c5e4ec4] debian/rules: enable all hardening

 -- Florian Ernst <florian@debian.org>  Wed, 01 Oct 2014 21:25:08 +0200

xabacus (8.0.0-1) unstable; urgency=medium

  * [9947431] Imported Upstream version 8.0.0
    + [834d445] install new xpm
    + [fdf1028] debian/copyright: adjust Copyright year
  * [5e08f8c] debian/control: Standards-Version 3.9.5 (no changes required)

 -- Florian Ernst <florian@debian.org>  Sun, 06 Apr 2014 13:36:50 +0200

xabacus (7.7.1-3) unstable; urgency=low

  * [39e491c] B-D on libmotif-dev instead of lesstif2-dev (Closes: #714674)
  * [63883a8] Standards-Version: 3.9.4 (no changes required)

 -- Florian Ernst <florian@debian.org>  Thu, 22 Aug 2013 13:10:36 +0200

xabacus (7.7.1-2) unstable; urgency=low

  * [5a30197] debian/control: wrap-and-sort
  * [9bb7924] debian/control: explicitly Build-Depend on libxt-dev
    (Closes: #707926)

 -- Florian Ernst <florian@debian.org>  Sun, 12 May 2013 11:16:30 +0200

xabacus (7.7.1-1) unstable; urgency=low

  * [fb38951] Imported Upstream version 7.7.1
  * [d729f5c] debian/control: adjust Vcs-* to best practices
  * [a027961] debian/copyright: adjust copyright years

 -- Florian Ernst <florian@debian.org>  Sun, 05 May 2013 10:53:18 +0200

xabacus (7.6.8-3) unstable; urgency=low

  * [ef0c5aa] debian/control: explicitly add libaudiofile-dev to B-D as we
    explicitly link to this library (see #556687 for background) and indirect
    B-D via libsd doesn't suffice (Closes: #657594)
  * [aa1017e] debian/rules: add recommended targets, and list phony targets
  * [02f0cf4] debian/control: Standards-Version: 3.9.3 (no further changes
    required)
  * [de76a79] debian/changelog: s/explicitely/explicitly/ as suggested by
    lintian
  * [6ccf1ae] drop old and unused lintian override
  * [315d365] use hardening via buildflags, B-D on dpkg-dev (>= 1.16.1)
    accordingly

 -- Florian Ernst <florian@debian.org>  Fri, 08 Jun 2012 14:59:40 +0200

xabacus (7.6.8-2) unstable; urgency=low

  * [d871a3f] explicitly pass audiofile to the linker; this works for the
    current packages, but generally the configure script ought to handle this;
    tested with binutils-gold (Closes: #556687)

 -- Florian Ernst <florian@debian.org>  Thu, 03 Mar 2011 18:58:36 +0100

xabacus (7.6.8-1) unstable; urgency=low

  * [9204a1a] Imported Upstream version 7.6.8
  * [1ea93bd] debian/copyright: update copyright years
  * [fd3513b] debian/control: Standards-Version: 3.9.1 (no changes
    required)

 -- Florian Ernst <florian@debian.org>  Mon, 21 Feb 2011 20:58:40 +0100

xabacus (7.6-1) unstable; urgency=low

  * [fcc390d] Imported Upstream version 7.6
  * [da52bff] Standards-Version: 3.8.4

 -- Florian Ernst <florian@debian.org>  Wed, 17 Feb 2010 18:10:09 +0100

xabacus (7.5.5-1) unstable; urgency=low

  * [47215bc] Imported Upstream version 7.5.5
  * [d8f9613] reimport debian/ as present in 7.1.7-1 Somehow the last
    upload had been made native, including debian/ in the orig.tar.gz!
    Let's now get back to sane ways ...
  * [3a539b7] New / old maintainer (Closes: #382562)
  * [6164813] debian/copyright: restructure
  * [c3eab2f] debian/{doc-base,menu,desktop}: adjust to new standards
  * [bb1faa0] debian/{rules,control}: streamline, new standards, making
    lintian happy and allowing for Standards-Version: 3.8.3 (Closes:
    #536836)
  * [e0b3dc6] change the source package format to 3.0 (quilt)
  * [b2e34d2] debian/control: add Vcs-Browser and Vcs-Git

 -- Florian Ernst <florian@debian.org>  Tue, 29 Dec 2009 17:48:21 +0100

xabacus (7.1.7-1) unstable; urgency=low

  * New Mantainer. (closes: #344449)
  * Updated debian/copyright file with new mantainer.
  * Updated menu files.
  * Added suport to desktop menu (added xabacus.desktop and xmabacus.desktop
    files).
  * Updated debian/watch file.
  * New upstream release.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Mon, 16 Jan 2006 19:04:33 -0200

xabacus (7.1.6-1) unstable; urgency=low

  * New upstream release
  * Packaging upgrades

 -- Florian Ernst <florian@debian.org>  Wed, 30 Nov 2005 17:20:06 +0100

xabacus (7.1.5-1) unstable; urgency=low

  * New upstream release
  * debian/rules: minor improvements in style
  * debian/*doc-base: fix path / name of Abacus.ps

 -- Florian Ernst <florian@debian.org>  Mon, 24 Oct 2005 18:16:19 +0200

xabacus (7.1.4-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version 3.6.2, no changes required

 -- Florian Ernst <florian@debian.org>  Tue,  9 Aug 2005 14:42:11 +0200

xabacus (7.1.3-1) unstable; urgency=low

  * New upstream release
    + includes better fix for esound problem, dropping my patch again
  * New maintainer address, many thanks to Eric Van Buggenhaut for previous
    sponsoring
  * debian/control: start Description: with lowercase letter instead

 -- Florian Ernst <florian@debian.org>  Sun, 23 Jan 2005 15:25:18 +0100

xabacus (7.1.2-1) unstable; urgency=low

  * Didn't get uploaded!
  * New upstream release
    + xabacus demo patch included, so droppping it from diff.gz
    + patch for esound applied
  * debian/control: added libesd0-dev to Build-Depends, removed
    autotools-dev (upstream takes care himself)
  * debian/copyright: update, elaborate a little bit further
  * debian/rules: adjusted to new release

 -- Florian Ernst <florian_ernst@gmx.net>  Wed, 22 Dec 2004 23:25:59 +0100

xabacus (7.0.6-2) unstable; urgency=low

  * Include patch from upstream to fix xabacus demo crash
  * debian/rules: minor reorganisation of the install targets
  * debian/xmabacus.menu: slight renaming of pixmaps to xmabacus*

 -- Florian Ernst <florian_ernst@gmx.net>  Sat, 14 Aug 2004 17:52:11 +0200

xabacus (7.0.6-1) unstable; urgency=low

  * New upstream release, new maintainer (Closes: #261432)
    + make sure the demo lessons are included (Closes: #96353)
  * echo 4 > debian/compat
  * debian/control:
    + adjusted descriptions, thanks Marek Habersack (Closes: #209879)
    + added upstream home page
    + streamlined Build-Depends, added autotools-dev
    + Standards-Version 3.6.1
  * debian/copyright:
    + added note about previous and current maintainer(s)
    + updated upstream location
    + extended copyright notice
  * debian/*{dirs,conffiles}: removed as they are not needed anymore
  * debian/*menu:
    + quoting the entries
    + include icons
  * debian/rules:
    + complete overhaul
  * debian/watch: added
  * debian/doc-base: added, including upstream's Abacus.ps

 -- Florian Ernst <florian_ernst@gmx.net>  Wed, 28 Jul 2004 12:34:25 +0200

xabacus (5.5.4-1) unstable; urgency=low

  * New upstream release
  * Corrected Description: in debian/control (closes:#131313)
  * Updated Standards-Version

 -- Eric Van Buggenhaut <ericvb@debian.org>  Tue, 30 Jul 2002 16:20:24 +0200

xabacus (5.5.2-6) unstable; urgency=low

  * changed maintainer name in debian/control.

 -- Eric Van Buggenhaut <ericvb@debian.org>  Fri, 25 May 2001 20:44:02 +0200

xabacus (5.5.2-5) unstable; urgency=low

  * New maintainer
  * added conffiles as required by policy.
  * updated debian/xmabacus.dirs
  * Easthetic changes in debian/control

 -- Eric Van Buggenhaut <ericvb@debian.org>  Thu, 24 May 2001 03:23:42 +0200

xabacus (5.5.2-4) unstable; urgency=low

  * /usr/X11R6/lib/X11/app-defaults -> /etc/X11/app-defaults
    (closes: #86298, #86313)

 -- Adrian Bunk <bunk@fs.tum.de>  Sat, 17 Feb 2001 16:40:47 +0100

xabacus (5.5.2-3) unstable; urgency=low

  * Corrected the priority of xabacus from extra to optional in
    debian/control.
  * Corrected the section from math to games in debian/control.

 -- Adrian Bunk <bunk@fs.tum.de>  Sun,  4 Feb 2001 01:57:35 +0100

xabacus (5.5.2-2) unstable; urgency=low

  * New maintainer. (closes: #68095)
  * Removed debian/info.ex from the source.

 -- Adrian Bunk <bunk@fs.tum.de>  Fri,  2 Feb 2001 14:00:58 +0100

xabacus (5.5.2-1) unstable; urgency=low

  * New upstream release. (closes: #50821)
  * Corrected the menu entries. (closes: #60400)
  * Added missing build dependency on debhelper.
  * Upload sponsored by Tony Mancill <tmancill@debian.org>.

 -- Adrian Bunk <bunk@fs.tum.de>  Thu, 14 Sep 2000 14:25:42 +0200

xabacus (5.4.4-7) unstable; urgency=low

  * Made package policy and lintian compliant.

 -- Christian Kurz <shorty@debian.org>  Sun, 12 Mar 2000 11:13:19 +0100

xabacus (5.4.4-6) unstable; urgency=low

  * Recoompiled lesstif/motif binaries under the new library
  * Orphaning Package

 -- Darren Benham <gecko@debian.org>  Fri, 05 Mar 1999 19:01:03 -0800

xabacus (5.4.4-5) unstable; urgency=low

  * Moved menu file for both binaries

 -- Darren Benham <gecko@debian.org>  Sat, 27 Mar 1999 19:01:03 -0800

xabacus (5.4.4-4) unstable; urgency=low

  * fixed spelling errors
  * moved location to /usr/games to match policy

 -- Darren Benham <gecko@debian.org>  Wed, 13 Jan 1999 22:49:22 -0800

xabacus (5.4.4-3) unstable; urgency=low

  * added conflicts between motif and non-motif version (closes #27392)

 -- Darren Benham <gecko@debian.org>  Wed,  7 Oct 1998 12:50:33 -0700

xabacus (5.4.4-2) unstable; urgency=low

  * Linking with libc6
  * Added package for lesstif binary
  * Divided two binaries to two packages

 -- Darren Benham <gecko@debian.org>  Sat, 26 Sep 1998 03:08:39 -0700

xabacus (5.4.4-1) unstable; urgency=low

  * Initial Release.

 -- Darren Benham <gecko@debian.org>  Tue, 30 Jun 1998 17:21:40 -0700

Local variables:
mode: debian-changelog
End:
