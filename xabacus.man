.\" @(#)xabacus.man
.\"
.\" Copyright 1994 - 2017  David A. Bagley
.\"
.\" All Rights Reserved
.\"
.\" Permission to use, copy, modify, and distribute this software and
.\" its documentation for any purpose and without fee is hereby granted,
.\" provided that the above copyright notice appear in all copies and
.\" that both that copyright notice and this permission notice appear in
.\" supporting documentation, and that the name of the author not be
.\" used in advertising or publicity pertaining to distribution of the
.\" software without specific, written prior permission.
.\"
.\" This program is distributed in the hope that it will be "useful",
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
.\"
.TH XABACUS 6 "16 Nov 2017" "V8.1.5"
.SH NAME
xabacus \- Abacus X widget
.SH SYNOPSIS
.B /usr/games/xabacus
[\-geometry [{width}][x{height}][{+\-}{xoff}[{+\-}{yoff}]]]
[\-display [{host}]:[{vs}]] [\-[no]mono] [\-[no]{reverse|rv}]
[\-{foreground|fg} {color}] [\-{background|bg} {color}]
[\-{border|bd} {color}] [\-frame {color}]
[\-primaryBeadColor {color}] [\-leftAuxBeadColor {color}]
[\-rightAuxBeadColor {color}] [\-secondaryBeadColor {color}]
[\-highlightBeadColor {color}]
[\-primaryRailColor {color}] [\-secondaryRailColor {color}]
[\-highlightRailColor {color}] [\-lineRailColor {color}]
[\-bumpSound {filename}] [\-moveSound {filename}]
[\-dripSound {filename}] [\-[no]sound]
[\-delay msecs] [\-[no]script] [\-[no]demo]
[\-demopath {path}] [\-{demofont|demofn} {fontname}]
[\-{demoforeground|demofg} {color}]
[\-[no]teach] [\-[no]rightToLeftAdd] [\-[no]rightToLeftMult]
[\-[no]lee] [\-rails {int}] [\-leftAuxRails {int}]
[\-rightAuxRails {int}] [\-[no]vertical]
[\-colorScheme {int}] [\-[no]slot] [\-[no]diamond]
[\-railIndex {int}] [\-[no]topOrient] [\-[no]bottomOrient]
[\-topNumber {int}] [\-bottomNumber {int}] [\-topFactor {int}]
[\-bottomFactor {int}] [\-topSpaces {int}] [\-bottomSpaces {int}]
[\-topPiece {int}] [\-bottomPiece {int}] [\-topPiecePercent {int}]
[\-bottomPiecePercent {int}] [\-shiftPercent {int}]
[\-subdeck {int}] [\-subbead {int}] [\-[no]sign]
[\-decimalPosition {int}] [\-[no]group] [\-groupSize {int}]
[\-[no]decimalComma] [\-base {int}] [\-[no]eighth]
[\-anomaly {int}] [\-shiftAnomaly {int}] [\-anomalySq {int}]
[\-shiftAnomalySq {int}] [\-displayBase {int}]
[\-[no]pressOffset] [\-[no]romanNumerals]
[\-[no]latin] [\-[no]ancientRoman] [\-[no]modernRoman]
[\-{chinese|japanese|korean|russian|danish|roman|medieval|generic}]
[\-{it|uk|fr}] [\-version]
.SH DESCRIPTION
This is an implementation of the classic Chinese Abacus
(Suanpan) which has its origins in the 12th century.

The device has two decks.  Each deck, separated by a
partition, normally has 13 rails on which are mounted beads.
Each rail on the top deck contains 1 or 2 beads, and each
rod on the bottom deck contains 4 or 5 beads.  Each bead on
the upper deck has a value of five, while each bead on the
lower deck has value of one.  Beads are considered counted,
when moved \fItowards\fP the partition separating the decks, i.e.
to add a value of one, a bead in the bottom deck is moved
up, and to add a value of 5, a bead in the top deck is moved
down.

The basic operations of the abacus are addition and subtraction.
Multiplication can be done by mentally multiplying the
digits and adding up the intermediate results on the abacus.
Division would be similar where the intermediate results
are subtracted.  There are techniques like using your thumb
and forefinger which does not apply with mouse entry.
Also with multiplication, one can carry out calculations on
different parts of the abacus for scratch work, here it is
nice to have a long abacus.

The pre\-WWII Japanese Abacus (Soroban) (or Korean Jupan)
is similar to the Chinese Abacus but has only one bead
per rail on the top deck.  The later Japanese Abacus was
further simplified to have only 4 beads per rail on the
bottom deck.

The Roman Hand\-Abacus predates the Chinese Abacus and is
very similar to the later Japanese Abacus, but seems to
have fallen out of use with the Fall of the Roman Empire
(at least 3 are in existence).  The Roman Abaci are brass
plates where the beads move in slots.  In addition to the
normal 7 columns of beads, they generally have 2 special
columns on the right side.  In two examples: the first
special column was for 12ths (12 uncia (ounces) = 1 as) and
had one extra bead in the bottom deck.  Also the last column
was a combination of halves, quarters, and twelfths of an
ounce and had no beads in the top deck and 4 beads at the
bottom (beads did not have to come to the top to be
counted but at one of 3 marked points where the top bead
was for halves, the next bead for quarters, and the last two
beads for twelfths).  In another surviving example: the 2
special columns were switched and the combination column
was broken into 3 separate slots.  If available, decimal
input is ignored.

The Russian Abacus was invented in the 17th century, here
the beads are moved from right to left.  It has colored
beads in the middle for ease of use.  Quarters represent
1/4 Rubles and are only present historically on the Russian
Abacus (Schety).  Some of the older Schety have a extra place
for the 1/4 Kopek (quarter percent) as well as the 1/4
Ruble (quarter).

The Danish Abacus was used in the early 20th century in
elementary schools as a teaching aid.

The Medieval Counter is a primitive form of the abacus and
was used in Europe as late as the 1600s.  It was useful
considering they were using it with Roman Numerals.   This
is similar to the Salamis Greek Tablet from 4th or 5th
Century BCE.

The Mesoamerican Nepohualtzintzin is a Japanese Abacus
base 20.  The Mesoamericans had base 20 with the
exception of the 3rd decimal place where instead of
20*20=400 the third place marked 360 and the 4th place was
20*360, etc..  They independently created their own zero
(only Babylon (base 60) and India (base 10) have done this)
but the anomaly took away its true power.

An easy way of figuring out time in seconds given hours,
minutes, and seconds, can be done on the abacus with
special anomaly "watch" settings.

The Chinese Solid\-and\-Broken\-Bar System is a base 12
numbering system and not really an abacus.  When the
abacus is setup in this way though (topFactor 3, topNumber 3,
bottomNumber 2, base 12, displayBase 12), it is easy to
relate the two.

The signed bead is an invention of the author, and is not
present on any historical abacus (to his knowledge) and is
used to represent negatives.  "New & Improved" abacus
models have two auxiliary decks stacked above the principal
deck that enable multiplication, division, square\-root, and
cube\-root computations to be performed with equal ease as
addition and subtraction  (well, so I have read).
.SH FEATURES
Click "\fBmouse\-left\fP" button on a bead you want to move.  The
beads will shift themselves to vacate the area of the
column that was clicked.
.LP
Click "\fBmouse\-right\fP" button, or press "\fBC\fP" or "\fBc\fP" keys,
to clear the abacus.
.LP
Press "\fBO\fP" or "\fBo\fP" keys to toggle the demo mode.
.LP
Press "\fB$\fP" key to toggle the teach mode.
.LP
In teach mode, "\fB+\fP" key toggles starting side to sum,
"\fB"*\fP" key toggles for starting side for multiplicand.
.LP
Press "\fB~\fP" or "\fB`\fP" keys to complement the beads on the rails.
.LP
Press "\fBI\fP" or "\fBi\fP" keys to increment the number of rails.
Press "\fBD\fP" or "\fBd\fP" keys to decrement the number of rails.
.LP
Press "\fBF\fP" or "\fBf\fP" keys to switch between Chinese,
Japanese, Korean, Russian, Danish, Roman, and Medieval formats.
There is an extra "Generic" format, this allows one to
break some rules binding the other formats (for example,
if one wanted more beads on top deck than on bottom deck you
would use this, in addition to resource option changes).
.LP
Press "\fBV\fP" or "\fBv\fP" keys to toggle Roman Nvmerals.
(Pardon typo/humor, but ran out of letters).
.LP
Press "\fBX\fP" or "\fBx\fP" keys to toggle Ancient Roman
Numerals (when Roman Nvmerals is activated).
.LP
Press "\fBY\fP" or "\fBy\fP" keys to toggle Latin
Numerals (when Roman Nvmerals and quarter beads or twelfth
beads are activated).
.LP
Press "\fBS\fP" or "\fBs\fP" keys to toggle the sign bead.
.LP
Press "\fBU\fP" or "\fBu\fP" keys to toggle the availability of
quarter beads.  (Mutually exclusive to twelfth beads).
Intended for the Russian Abacus.
.LP
Press "\fBT\fP" or "\fBt\fP" keys to toggle the availability of
twelfth beads.  (Mutually exclusive to quarter beads).
Intended for the Roman Abacus.
.LP
Press "\fBP\fP" or "\fBp\fP" keys to toggle the availability of
quarter percent beads.  (Dependent on quarter beads (or
twelfth beads).  Intended for the older Russian Abacus.
.LP
Press "\fBB\fP" or "\fBb\fP" keys to toggle the availability of
subdecks.  (Dependent on twelfth beads (or quarter beads)
and Roman format).  Intended for the Roman Abacus, where
the lowest value of two at bottom of the rightmost
column of beads are a twelfth of the column second from
right.
.LP
Press "\fBE\fP" or "\fBe\fP" keys to toggle the availability of
subdecks.  (Dependent on twelfth beads (or quarter beads)
and Roman format).  Intended for the Roman Abacus, where
the lowest value of three at bottom of the rightmost
column of beads are an eighth of the column second from
right.
.LP
Press "\fBM\fP" or "\fBm\fP" keys to switch between it, uk, and fr
museum formats.
.LP
Press "\fBZ\fP" or "\fBz\fP" keys to toggle Modern Numerals
on frame (when showing Roman Hand Abacus).
.LP
Press "\fBL\fP" or "\fBl\fP" keys to toggle the availability of
anomaly bars.  Intended to be used with Japanese Abacus and
base 20 for the Mesoamerican Abacus.  (Mutually exclusive
to watch bars).
.LP
Press "\fBW\fP" or "\fBw\fP" keys to toggle the availability of
watch bars.  Intended to represent seconds where hours
and minutes can be set.  (Mutually exclusive to anomaly
bars).
.LP
Press "\fB>\fP" or "\fB.\fP" keys to speed up the movement of beads.
Press "\fB<\fP" or "\fB,\fP" keys to slow down the movement of beads.
.LP
Press "\fB@\fP" key to toggle the sound.
.LP
Press "\fBEsc\fP" key to hide program.
.LP
Press "\fBQ\fP", "\fBq\fP", or "\fBCTRL\-C\fP" keys to kill program.
.LP
The abacus may be resized.  Beads will reshape depending on the room they
have.
\fIDemo Mode:\fP
In this mode, the abacus is controlled by the
program.  When started with the demo option, a second
window is presented that should be placed directly below the
abacus\-window. Descriptive text, and user prompts are
displayed in this window.  Pressing 'q' during the demo will
quit it.  Clicking the left mouse\-button with the pointer in
the window will restart the demo (beginning of current lesson).
.SH OPTIONS
.TP 8
.B \-geometry {+|\-}\fIX\fP{+|\-}\fIY\fP
This option sets the initial position of the abacus window (resource
name "\fIgeometry\fP").
.TP 8
.B \-display \fIhost\fP:\fIdpy\fP
This option specifies the X server to contact.
.TP 8
.B \-[no]mono
This option allows you to display the abacus window on a color screen as
if it were monochrome (resource name "\fImono\fP").
.TP 8
.B \-[no]{reverse|rv}
This option allows you to see the abacus window in reverse video (resource
name "\fIreverseVideo\fP").
.TP 8
.B \-{foreground|fg} \fIcolor\fP
This option specifies the foreground of the abacus window (resource name
"\fIforeground\fP").
.TP 8
.B \-{background|bg} \fIcolor\fP
This option specifies the background of the abacus window (resource name
"\fIbackground\fP").
.TP 8
.B \-{border|bd} \fIcolor\fP
This option specifies the foreground of the bead border (resource
name "\fIborderColor\fP").
.TP 8
.B \-frame \fIcolor\fP
This option specifies the foreground of the frame (resource name
"\fIframeColor\fP").
.TP 8
.B \-primaryBeadColor \fIcolor\fP
This option specifies the foreground of the beads (resource name
"\fIprimaryBeadColor\fP").
.TP 8
.B \-leftAuxBeadColor \fIcolor\fP
This option specifies the foreground of the beads for the left
auxiliary abacus in Lee's Abacus (resource name
"\fIleftAuxBeadColor\fP").
.TP 8
.B \-rightAuxBeadColor \fIcolor\fP
This option specifies the foreground of the beads for the right
auxiliary abacus in Lee's Abacus (resource name
"\fIrightBeadColor\fP").
.TP 8
.B \-secondaryBeadColor \fIcolor\fP
This option specifies the secondary color of the beads (resource name
"\fIsecondaryBeadColor\fP").
.TP 8
.B \-highlightBeadColor \fIcolor\fP
This option specifies the highlight color of the beads (resource name
"\fIhighlightBeadColor\fP").
.TP 8
.B \-primaryRailColor \fIcolor\fP
This option specifies the foreground of the rails (resource name
"\fIprimaryRailColor\fP").
.TP 8
.B \-secondaryRailColor \fIcolor\fP
This option specifies the secondary color of the rails (resource name
"\fIsecondaryRailColor\fP").
.TP 8
.B \-highlightRailColor \fIcolor\fP
This option specifies the highlight color of the rails (resource name
"\fIhighlightRailColor\fP").
.TP 8
.B \-lineRailColor \fIcolor\fP
This option specifies the color of the lines when using checkers
(resource name "\fIlineRailColor\fP").
.TP 8
.B \-bumpSound \fIfilename\fP
This option specifies the file for the bump sound for the movement
of the beads (resource name "\fIbumpSound\fP").
.TP 8
.B \-moveSound \fIfilename\fP
This option specifies the file for the move sound for the sliding
of the decimal point marker (resource name "\fImoveSound\fP").
.TP 8
.B \-dripSound \fIfilename\fP
This option specifies the file for the drip sound for changing the
format (resource name "\fIdripSound\fP").
.TP 8
.B \-[no]sound
This option specifies if a sliding bead should make a sound or not
(resource name "\fIsound\fP").
.TP 8
.B \-delay \fImsecs\fP
This option specifies the number of milliseconds it takes to move a
bead or a group of beads one space (resource name "\fIdelay\fP").
.TP 8
.B \-[no]script
This option specifies to log application to \fIstdout\fP, every time
the user clicks to move the beads (resource name "\fIscript\fP"). The
output is a set of auxiliary, deck, rail, beads added or subtracted, and
the number of text lines (4).  This can be edited to add text to the
lesson and used as a new demo keeping the generated numbers and the
number of lines constant.  (Windows version writes to abacus.xml.)
.TP 8
.B \-[no]demo
This option specifies to run in demo mode.  In this mode, the abacus is
controlled by the current lesson (resource name "\fIdemo\fP").
When started with the demo option, a window contains descriptive text,
and user prompts are displayed in this window.  Pressing 'q' during the
demo will quit it.  Clicking the left mouse\-button with the pointer in
the window will restart the demo (beginning of current lesson).
The demo uses abacusDemo.xml and currently there are 4 editions
possible (Chinese, Japanese (and Roman), Korean, and Russian (and
Danish)).
.TP 8
.B \-demopath \fIpath\fP
This option specifies the path for the demo, possibly something like
/usr/local/share/games/xabacus (resource name "\fIdemoPath\fP"), with 
the file name of abacusDemo.xml.  For this to work, the program must be
compiled with XML2 or else will use a brief static fallback demo.
.TP 8
.B \-demofont \fIfontstring\fP
This option specifies the font for the explanatory text that appears in
the secondary window, during the demo.  The default font is 18 point
Times\-Roman (\-*\-times\-*\-r\-*\-*\-*\-180\-*). The alternate font is 8x13.
.TP 8
.B \-demofg \fIcolor\fP
This option specifies the foreground of the abacus demo window (resource
name "\fIdemoForeground\fP").
.TP 8
.B \-demobg \fIcolor\fP
This option specifies the background of the abacus demo window (resource
name "\fIdemoBackground\fP").
.TP 8
.B \-[no]teach
This option specifies to run in teach mode.  In this mode, the abacus is
controlled by 2 numbers separated by an operator: "+" for addition,
"-" for subtraction, "*" for multiplication, and  "/" for division.
The square root operation is represented by the number to
be operated on followed by the character "v" (this leaves you
with an answer from which you must divide by 2).  Similarly, the
cube root operation is represented by the number to be operated on
followed by the character "u" (this leaves you with an answer from
which you must divide by 3).  Press return key to progress through
the steps (resource name "\fIteach\fP").
.TP 8
.B \-[no]rightToLeftAdd
This option specifies the order for teach starting side for addition and
subtraction.  The default is the traditional left to right.  Right to
left seems easier though (resource name "\fIrightToLeftAdd\fP").
.TP 8
.B \-[no]rightToLeftMult
This option specifies the order for teach starting side for
multiplication.  The default is the traditional left to right.  Right to
left seems more straight forward (resource name "\fIrightToLeftMult\fP").
.TP 8
.B \-[no]lee
This option allows you to turn on and off the two extra auxiliary abaci
(resource name "\fIlee\fP").
.TP 8
.B \-rails \fIint\fP
This option specifies the number of rails (resource name "\fIrails\fP").
.TP 8
.B \-leftAuxRails \fIint\fP
This option allows you to set the number of the rails for the left
auxiliary abacus in Lee's Abacus (resource name "\fIleftAuxRails\fP").
.TP 8
.B \-rightAuxRails \fIint\fP
This option allows you to set the number of the rails for the right
auxiliary abacus in Lee's Abacus (resource name "\fIrightAuxRails\fP").
.TP 8
.B \-[no]vertical
This option allows you to set the abacus to allow a Russian orientation
(resource name "\fIvertical\fP").
.TP 8
.B \-colorScheme \fIint\fP
This option specifies the color scheme for the abacus (resource
name "\fIcolorScheme\fP") where 0\-> none, 1\-> color middle (2 beads
beads but if odd color 1 bead), 2\-> color first of group, 3\-> both 1
and 2, 4\-> color first half (but if odd color middle bead).
.TP 8
.B \-[no]slot
This option allows you to have either slots or rails (resource name
"\fIslot\fP").
.TP 8
.B \-[no]diamond
This option allows you to have either diamond or round beads (resource
name "\fIdiamond\fP").
.TP 8
.B \-railIndex \fIint\fP
This option specifies the index of color for the rails of the
abacus (resource name "\fIrailIndex\fP") where a value is 0 or 1.
.TP 8
.B \-[no]topOrient
This option specifies the orientation of the beads on top (resource name
"\fItopOrient\fP").
.TP 8
.B \-[no]bottomOrient
This option specifies the orientation of the beads on bottom (resource name
"\fIbottomOrient\fP").
.TP 8
.B \-topNumber \fIint\fP
This option specifies the number of beads on top (resource name
"\fItopNumber\fP").
.TP 8
.B \-bottomNumber \fIint\fP
This option specifies the number of beads on bottom (resource name
"\fIbottomNumber\fP").
.TP 8
.B \-topFactor \fIint\fP
This option specifies the multiply factor for the beads on top (resource
name "\fItopFactor\fP").
.TP 8
.B \-bottomFactor \fIint\fP
This option specifies the multiply factor for the beads on bottom
(resource name "\fIbottomFactor\fP").
.TP 8
.B \-topSpaces \fIint\fP
This option specifies the number of spaces on top (resource name
"\fItopSpaces\fP").
.TP 8
.B \-bottomSpaces \fIint\fP
This option specifies the number of spaces on bottom (resource name
"\fIbottomSpaces\fP").
.TP 8
.B \-topPiece \fIint\fP
This option specifies the number of pieces on top (resource name
"\fItopPiece\fP").
.TP 8
.B \-bottomPiece \fIint\fP
This option specifies the number of pieces on bottom (resource name
"\fIbottomPiece\fP").
.TP 8
.B \-topPiecePercent \fIint\fP
This option specifies the number of piece percents on top (resource name
"\fItopPiecePercent\fP").
.TP 8
.B \-bottomPiecePercent \fIint\fP
This option specifies the number of piece percents on bottom (resource
name "\fIbottomPiecePercent\fP").
.TP 8
.B \-shiftPercent \fIint\fP
This option specifies the shift of rails for piece percents and also
may influence the precision of the calculation (resource name
"\fIshiftPercent\fP").
.TP 8
.B \-subdeck \fIint\fP
This option specifies the special subdecks column (resource name
"\fIsubdeck\fP").
.TP 8
.B \-subbead \fIint\fP
This option specifies the special subbeads (resource name
"\fIsubbead\fP").
.TP 8
.B \-[no]sign
This option allows you to set the abacus to allow negatives (resource
name "\fIsign\fP").
.TP 8
.B \-decimalPosition \fIint\fP
This option specifies the number of rails to the right of the decimal
point (normally 2) (resource name "\fIdecimalPosition\fP").
.TP 8
.B \-[no]group
This option allows you to group the displayed digits for readability
(resource name "\fIgroup\fP").
.TP 8
.B \-groupSize \fIint\fP
This option specifies the group size to the left of the decimal point
(normally 3) (resource name "\fIgroupSize\fP").
.TP 8
.B \-[no]decimalComma
This option allows you to swap "." for "," to allow for different display
format (resource name "\fIdecimalComma\fP").
.TP 8
.B \-base \fIint\fP
This option specifies the base used on abacus (default is base 10)
(resource name "\fIbase\fP").
.TP 8
.B \-[no]eighth
This option specifies the base for the Roman subdeck, (if set,
the resource is set to 8, else it is set to 12) (resource name
"\fIsubbase\fP").
.TP 8
.B \-anomaly \fIint\fP
This option specifies the offset from the base for a multiplicative
factor of the rail with the anomaly (if none, this is set to 0)
(resource name "\fIanomaly\fP").
.TP 8
.B \-shiftAnomaly \fIint\fP
This option specifies the offset from decimal point for the anomaly
(usually 2) (resource name "\fIshiftAnomaly\fP").
.TP 8
.B \-anomalySq \fIint\fP
This option specifies the offset from base for the second anomaly
(if none, this is set to 0) (resource name "\fIanomalySq\fP").
.TP 8
.B \-shiftAnomalySq \fIint\fP
This option specifies the offset in rails from the first anomaly
(usually 2) (resource name "\fIshiftAnomalySq\fP").
doing).
.TP 8
.B \-displayBase \fIint\fP
This option specifies the base displayed (default is base 10) (resource
name "\fIdisplayBase\fP").  If this is different then "\fIbase\fP"
then it is implemented using "long long" and the calculation is limited
by its bounds.  Also the fractional part does not scale with the
"\fIdisplayBase\fP" so if the "\fIdisplayBase\fP" is greater than the
"\fIbase\fP" it looses some precision.  Also no rounding is done.
.TP 8
.B \-[no]pressOffset
This option allows you to put a pixel space between all the beads so
there is room for the bead to move when pressed (resource name
"\fIpressOffset\fP").
.TP 8
.B \-[no]romanNumerals
This option allows you to set the abacus to allow Roman Numerals
(resource name "\fIromanNumerals\fP").  Roman Numerals above 3999 are
normally represented with bars on top, due to ASCII constraints this
is represented instead in lower case (historically case was ignored).
Roman Numerals above 3,999,999 were not represented historically.  Roman
numerals change with displayBase in an "experimental" way.  When used
with twelfths and subdecks, named fraction symbols are used.  Due to ASCII
constraints the sigma is represented as E, the backwards C is represented
as a Q, the mu as a u, and the Z with a \- through the center as a z.
If available, decimal input is ignored.
.TP 8
.B \-[no]latin
This option allows you to set the abacus to allow latin fractions
instead of symbolic in the Roman numeral output (resource name
"\fIlatin\fP").
.TP 8
.B \-[no]ancientRoman
This option allows you to set the abacus to allow ancient Roman numerals
instead of the modern in the Roman numeral output (resource name
"\fIancientRoman\fP").
.TP 8
.B \-[no]modernRoman
This option allows you to set the abacus to allow modern Roman numerals
instead of the ancient on the Roman Hand abacus (resource name
"\fImodernRoman\fP").
.TP 8
.B \-chinese
This option specifies the format on the abacus (resource name
"\fIformat\fP") to "Chinese" for the Chinese Suanpan.
.TP 8
.B \-japanese
This option specifies the format on the abacus (resource name
"\fIformat\fP") to "Japanese" for the Japanese post\-WWII Soroban.
This is also similar to the Roman Hand Abacus.
.TP 8
.B \-korean
This option specifies the format on the abacus (resource name
"\fIformat\fP") to "Korean" for the Korean Jupan or Japanese
pre\-WWII Soroban.
.TP 8
.B \-russian
This option specifies the format on the abacus (resource name
"\fIformat\fP") to "Russian" for the Russian Schety.  To complete,
specify \fIpiece\fP" to be 4, for the older Schety also specify
\fIpiecePercent\fP" to be 4.
.TP 8
.B \-danish
This option specifies the format of the abacus (resource name
"\fIformat\fP") to "Danish" for the Danish Elementary School
Abacus teaching aid.
.TP 8
.B \-roman
This option specifies the format on the abacus (resource name
"\fIformat\fP") to "Roman" for the Roman Hand Abacus, note beads
move in slots.  To complete, specify
\fIromanNumerals\fP".
.TP 8
.B \-medieval
This option specifies the format of the abacus (resource name
"\fIformat\fP") to "Medieval" for the Medieval Counter, with
counters instead of beads.
.TP 8
.B \-generic
This option specifies the format on the abacus (resource name
"\fIformat\fP") to "Generic".  This option specifies a format that is
more configurable by using resources, since there are few rules to
govern its behavior.
.TP 8
.B \-it
This option specifies the subformat of the abacus in Museum of
the Thermae, Rome.
.TP 8
.B \-uk
This option specifies the subformat of the abacus in British
Museum in London.
.TP 8
.B \-fr
This option specifies the subformat of the abacus in Cabinet de
medailles, Bibliotheque nationale, Paris.
.TP 8
.B \-version
This option tells you what version of xabacus you have.
.SH REFERENCES
Luis Fernandes  \fIhttp://www.ee.ryerson.ca/~elf/abacus/\fP
.sp
Lee Kai\-chen, How to Learn Lee's Abacus, 1958, 58 pages.
.sp
Abacus Guide Book, 57 pages.
.sp
Georges Ifrah, The Universal History of Numbers, Wiley Press 2000,
pp 209\-211, 288\-294.
.sp
Review of the above: http://www.ams.org/notices/200201/rev\-dauben.pdf
.sp
David Eugene Smith, History of Mathematics Volume II, Dover
Publications, Inc 1958, pp 156\-195.
.SH SEE ALSO
.LP
X(1), xcubes(6), xtriangles(6), xhexagons(6), xmlink(6), xbarrel(6),
xpanex(6), xmball(6), xpyraminx(6), xoct(6), xrubik(6), xskewb(6), xdino(6)
.SH COPYRIGHTS
.LP
\*R Copyright 1994\-2017, David A. Bagley
.LP
Luis Fernandes, <\fIelf AT ee.ryerson.ca\fP> wrote an independent program
(xabacus 1.00) with a demo mode and postscript file.  I tried, with his
permission, to take the best features of both into one program.
Also, I had help with some of the abacus in the Java version by
Sarat Chandran, <\fIsaratcmahadevan AT yahoo.com\fP> and some of these
ideas were ported back into this X version.
.SH BUG REPORTS AND PROGRAM UPDATES
.LP
Send bugs (or their reports, or fixes) to the author:
.RS
David A. Bagley, <\fIbagleyd AT verizon.net\fP>
.RE
.sp
The latest version is currently at:
.RS
\fIhttp://www.sillycycle.com/abacus.html\fP
.br
\fIhttp://ibiblio.org/pub/Linux/apps/math\fP
