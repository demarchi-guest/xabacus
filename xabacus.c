/*
 * @(#)xabacus.c
 *
 * Copyright 1993 - 2018  David A. Bagley, bagleyd AT verizon.net
 *
 * Abacus demo and neat pointers from
 * Copyright 1991 - 1998  Luis Fernandes, elf AT ee.ryerson.ca
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Driver file for Abacus */
#ifndef WINVER
#include "version.h"
static const char * aboutHelp = {
"Abacus Version " VERSION "\n"
"Send bugs (reports or fixes) to the author: "
"David Bagley <bagleyd AT verizon.net>\n"
"The latest version is at: "
"http://www.sillycycle.com/abacus.html\n"
"Some coding was also done by Luis Fernandes <elf AT ee.ryerson.ca>\n"
"and Sarat Chandran <saratcmahadevan AT yahoo.com>"
};

static const char * synopsisHelp = {
"[-geometry [{width}][x{height}] [{+-}{xoff}[{+-}{yoff}]]]\n"
"[-display [{host}]:[{vs}]] [-[no]mono] [-[no]{reverse|rv}]\n"
"[-{foreground|fg} {color}] [-{background|bg} {color}]\n"
"[-{border|bd} {color}] [-frame {color}]\n"
"[-primaryBeadColor {color}] [-leftAuxBeadColor {color}]\n"
"[-rightAuxBeadColor {color}] [-secondaryBeadColor {color}]\n"
"[-highlightBeadColor {color}]\n"
"[-primaryRailColor {color}] [-secondaryRailColor {color}]\n"
"[-highlightRailColor {color}] [-lineRailColor {color}]\n"
"[-bumpSound {filename}] [-moveSound {filename}]\n"
"[-[no]sound] [-delay msecs] [-[no]script] [-[no]demo]\n"
"[-demopath {path}] [-{demofont|demofn} {fontname}]\n"
"[-{demoforeground|demofg} {color}]\n"
"[-[no]teach] [-[no]rightToLeftAdd] [-[no]rightToLeftMult]\n"
"[-[no]lee] [-rails {int}] [-leftAuxRails {int}]\n"
"[-rightAuxRails {int}] [-[no]vertical]\n"
"[-colorScheme {int}] [-[no]slot] [-[no]diamond]\n"
"[-railIndex {int}] [-[no]topOrient] [-[no]bottomOrient]\n"
"[-topNumber {int}] [-bottomNumber {int}] [-topFactor {int}]\n"
"[-bottomFactor {int}] [-topSpaces {int}] [-bottomSpaces {int}]\n"
"[-topPiece {int}] [-bottomPiece {int}] [-topPiecePercent {int}]\n"
"[-bottomPiecePercent {int}] [-shiftPercent {int}]\n"
"[-subdeck {int}] [-subbead {int}] [-[no]sign]\n"
"[-decimalPosition {int}] [-[no]group] [-groupSize {int}]\n"
"[-[no]decimalComma] [-base {int}] [-[no]eighth]\n"
"[-anomaly {int}] [-shiftAnomaly {int}] [-anomalySq {int}]\n"
"[-shiftAnomalySq {int}] [-displayBase {int}]\n"
"[-[no]pressOffset] [-[no]romanNumerals]\n"
"[-[no]latin] [-[no]ancientRoman] [-[no]modernRoman]\n"
"[-{chinese|japanese|korean|russian|danish|roman|medieval|generic}]\n"
"[-{it|uk|fr}] [-version]"
};
#endif

#ifdef HAVE_MOTIF
static const char options1Help[] = {
"-geometry {+|-}X{+|-}Y    "
"This option sets the initial position of the abacus "
"window (resource name \"geometry\").\n"
"-display host:dpy    "
"This option specifies the X server to contact.\n"
"-[no]mono       "
"This option allows you to display the abacus window "
"on a color screen as if it were monochrome\n"
"       (resource name \"mono\").\n"
"-[no]{reverse|rv}    "
"This option allows you to see the abacus window in "
"reverse video (resource name\n"
"       \"reverseVideo\").\n"
"-{foreground|fg} color    "
"This option specifies the foreground of the abacus "
"window (resource name \"foreground\").\n"
"-{background|bg} color    "
"This option specifies the background of the abacus "
"window (resource name \"background\").\n"
"-{border|bd} color   "
"This option specifies the foreground of the border "
"of the beads (resource name \"borderColor\").\n"
"-frame color    "
"This option specifies the foreground of the frame "
"(resource name \"frameColor\").\n"
"-primaryBeadColor color   "
"This option specifies the foreground of the beads "
"(resource name \"primaryBeadColor\").\n"
"-leftAuxBeadColor color   "
"This option specifies the foreground of the beads "
"for the left auxiliary abacus in\n"
"       Lee's Abacus (resource name \"leftAuxBeadColor\").\n"
"-rightAuxBeadColor color  "
"This option specifies the foreground of the beads "
"for the right auxiliary abacus in\n"
"       Lee's Abacus (resource name \"rightAuxBeadColor\").\n"
"-secondaryBeadColor color "
"This option specifies the secondary color of the beads "
"(resource name\n"
"       \"secondaryBeadColor\").\n"
"-highlightBeadColor color "
"This option specifies the highlight color of the beads "
"(resource name\n"
"       \"highlightBeadColor\").\n"
"-primaryRailColor color   "
"This option specifies the foreground of the rails "
"(resource name \"primaryRailColor\").\n"
"-secondaryRailColor color "
"This option specifies the secondary color of the rails "
"(resource name\n"
"       \"secondaryRailColor\").\n"
"-highlightRailColor color "
"This option specifies the highlight color of the rails "
"(resource name \"highlightRailColor\").\n"
"-lineRailColor color "
"This option specifies the color of the lines when using "
"checkers (resource name \"lineRailColor\").\n"
"-bumpSound filename  "
"This option specifies the file for the bump sound "
"for the movement of the beads.\n"
"-moveSound filename  "
"This option specifies the file for the move sound "
"for the sliding of the decimal point marker.\n"
"-[no]sound      "
"This option specifies if a sliding bead should "
"make a sound or not (resource name \"sound\").\n"
"-delay msecs    "
"This option specifies the number of milliseconds it "
"takes to move a bead or a group of beads\n"
"       one space (resource name \"delay\").\n"
"-[no]script     "
"This option specifies to log application to stdout, "
"every time the user clicks to move the\n"
"       beads (resource name \"script\"). The output is a set "
"of deck, rail, beads added or subtracted, and\n"
"       the number of text lines (4).  This can be edited to add "
"text to the lesson and used as a new demo keeping\n"
"       the generated numbers and the number of lines constant."
"  (Windows version writes to abacus.xml).\n"
"-[no]demo       "
"This option specifies to run in demo mode.  In this "
"mode, the abacus is controlled by the current\n"
"       lesson (resource name \"demo\").  When started with "
"the demo option, a window contains descriptive text,\n"
"       and user prompts are displayed in this window.  Pressing "
"'q' during the demo will quit it.  Clicking the\n"
"       left mouse-button with the pointer in the window "
"will restart the demo (beginning of current lesson).\n"
"       The demo uses abacusDemo.xml and currently there are "
"4 editions possible ((Chinese, Japanese (and\n"
"       Roman), Korean, and Russian (and Danish))."
};
static const char options2Help[] = {
"-demopath path  "
"This option specifies the path for the demo, possibly "
"/usr/local/share/games/xabacus (resource\n"
"       name \"demoPath\"), with the file name of "
"abacusDemo.xml.\n"
"-{demofont|demofn} fontstring    "
"This option specifies the font for the "
"explanatory text that appears in the\n"
"       secondary window, during the demo.  The default font is "
"18 point Times-Roman (-*-times-*-r-*-*-*-180-*).\n"
"       The alternate font is 8x13 (resource name \"demoFont\").\n"
"-{demoforeground|demofg} color   "
"This option specifies the foreground of the abacus "
"demo window (resource name\n"
"       \"demoForeground\").\n"
"-{demobackground|demobg} color   "
"This option specifies the background of the abacus "
"demo window (resource\n"
"       name \"demoBackground\").\n"
"-[no]teach      "
"This option specifies to run in teach mode.  In "
"this mode, the abacus is controlled by 2 numbers\n"
"       separated by an operator: \"+\" for addition, "
"\"-\" for subtraction, \"*\" for multiplication, and \"/\" for\n"
"       division.  The square root operation is represented "
"by the number to be operated on followed by the\n"
"       character \"v\" (this leaves you with an answer "
"from which you must divide by 2).  Similarly, the cube root\n"
"       operation is represented by the number to be operated "
"on followed by the character \"u\" (this leaves you with\n"
"       an answer which from which you must divide by 3).  "
"Press return key to progress through the steps\n"
"       (resource name \"teach\").\n"
"-[no]rightToLeftAdd  "
"This option specifies the order for teach starting "
"side for addition and subtraction.  The\n"
"       default is the traditional left to right.  "
"Right to left seems easier though (resource name \"rightToLeftAdd\").\n"
"-[no]rightToLeftMult "
"This option specifies the order for teach starting "
"side for multiplication.  The default is\n"
"       the traditional left to right.  Right to left "
"seems more straight forward (resource name \"rightToLeftMult\").\n"
"-[no]lee   "
"This option allows you to turn on and off the two extra "
"auxiliary abaci (resource name \"lee\").\n"
"-rails int      "
"This option specifies the number of rails (resource "
"name \"rails\").\n"
"-leftAuxRails int "
"This option allows you to set the number of the "
"rails for the left auxiliary abacus in Lee's Abacus\n"
"       (resource name \"leftAuxRails\").\n"
"-rightAuxRails int  "
"This option allows you to set the number of the "
"rails for the right auxiliary abacus in Lee's\n"
"       Abacus (resource name \"rightAuxRails\").\n"
"-[no]vertical "
"This option allows you to have vertical rails "
"(resource name \"vertical\").\n"
"-colorScheme int    "
"This option specifies the color scheme for the "
"abacus (resource name \"colorScheme\") where\n"
"       0-> none, 1-> color middle (2 beads beads but if odd "
"color 1 bead), 2-> color first of group, 3-> both 1 and\n"
"       2, 4-> color first half (but if odd color middle "
"bead).\n"
"-[no]slot       "
"This option allows you to have either slots or rails "
"(resource name \"slot\").\n"
"-[no]diamond    "
"This option allows you to have diamond or round beads "
"(resource name \"diamond\").\n"
"-railIndex int  "
"This option specifies the index of color for the "
"rails of the abacus (resource name \"railIndex\")\n"
"       where a value is 0 or 1."
};
static const char options3Help[] = {
"-[no]topOrient    "
"This option specifies the orientation of the beads "
"on top (resource name \"topOrient\").\n"
"-[no]bottomOrient    "
"This option specifies the orientation of the beads "
"on bottom (resource name \"bottomOrient\").\n"
"-topNumber int    "
"This option specifies the number of beads on top "
"(resource name \"topNumber\").\n"
"-bottomNumber int    "
"This option specifies the number of beads on bottom "
"(resource name \"bottomNumber\").\n"
"-topFactor int    "
"This option specifies the multiply factor for the "
"beads on top (resource name \"topFactor\").\n"
"-bottomFactor int    "
"This option specifies the multiply factor for the "
"beads on bottom (resource name\n"
"       \"bottomFactor\").\n"
"-topSpaces int    "
"This option specifies the number of spaces on top "
"(resource name \"topSpaces\").\n"
"-bottomSpaces int    "
"This option specifies the number of spaces on bottom "
"(resource name \"bottomSpaces\").\n"
"-topPiece int     "
"This option specifies the number of pieces on top "
"(resource name \"topPiece\").\n"
"-bottomPiece int     "
"This option specifies the number of pieces on bottom "
"(resource name \"bottomPiece\").\n"
"-topPiecePercent int   "
"This option specifies the number of piece percents on top "
"(resource name\n"
"       \"topPiecePercent\").\n"
"-bottomPiecePercent int   "
"This option specifies the number of piece percents on bottom "
"(resource name\n"
"       \"bottomPiecePercent\").\n"
"-shiftPercent int    "
"This option specifies the shift of rails for piece percents "
"and also may influence the\n"
"       precision of the calculation (resource name "
"\"shiftPercent\").\n"
"-subdeck int    "
"This option specifies the special subdecks column "
"(resource name " "\"subdeck\").\n"
"-subbead int    "
"This option specifies the special subbeads column "
"(resource name " "\"subbead\").\n"
"-[no]sign       "
"This option allows you to set the abacus to allow "
"negatives (resource name \"sign\").\n"
"-decimalPosition int "
"This option specifies the number of rails to the "
"right of the decimal point\n"
"       (normally 2) (resource name \"decimalPosition\").\n"
"-[no]group      "
"This option allows you to group the displayed "
"digits for readability (resource name \"group\").\n"
"-groupSize int       "
"This option specifies the group size to the left of the "
"decimal point (normally 3) (resource\n"
"       name \"groupSize\").\n"
"-[no]decimalComma    "
"This option allows you to swap \".\" for \",\" "
"to allow for different display format (resource\n"
"       name \"decimalComma\").\n"
"-base int       "
"This option specifies the base used (default is base "
"10) (resource name \"base\").\n"
"-[no]eighth    "
"This option specifies the base for the Roman subdeck, "
"(if set, the resource is set to 8, else\n"
"       it is set to 12) (resource "
"name \"subbase\").\n"
"-anomaly int         "
"This option specifies the offset from base for a "
"multiplicative factor of the rail with the\n"
"       anomaly (if none, this is set to 0) "
"(resource name \"anomalySq\").\n"
"-shiftAnomaly int    "
"This option specifies the offset from decimal point "
"for the anomaly (usually 2) (resource name\n"
"       \"shiftAnomaly\").\n"
"-anomalySq int       "
"This option specifies the offset from base for the "
"second anomaly (if none, this is set to 0)\n"
"       (resource name \"anomalySq\").\n"
"-shiftAnomalySq int  "
"This option specifies the offset in rails from the "
"first anomaly (usually 2) (resource\n"
"       name \"shiftAnomalySq\")."
};
static const char options4Help[] = {
"-displayBase int     "
"This option specifies the base displayed (default is "
"base 10) (resource name \"displayBase\").\n"
"       If this is different then \"base\" then it is "
"implemented using \"long long\" and the calculation is\n"
"       limited by its bounds.  Also the fractional "
"part does not scale with the \"displayBase\" so if the\n"
"       \"displayBase\" is greater than the \"base\" it "
"looses some precision.  Also no rounding is done.\n"
"-[no]pressOffset     "
"This option allows you to put a pixel space between all the "
"beads so there is room for the bead\n"
"       to move when pressed (resource name \"pressOffset\").\n"
"-[no]romanNumerals   "
"This option allows you to set the abacus to "
"allow Roman Numerals (resource name\n"
"       \"romanNumerals\").  Roman Numerals above 3999 are "
"normally represented with bars on top, due to ASCII\n"
"       constraints this is represented instead in lower case "
"(historically case was ignored).   Roman Numerals above\n"
"       3,999,999 were not represented historically.  "
"Roman numerals change with displayBase in an\n"
"       \"experimental\" way.  When used with twelfths and "
"subdecks, named fraction symbols are used.  Due to\n"
"       ASCII constraints the sigma is represented as E, the "
"backwards C is represented as a Q, the mu as a u, and\n"
"       the Z with a - through the center as a z.  If "
"available, decimal input is ignored.\n"
"-[no]latin  "
"This option allows you to set the abacus to "
"allow latin fractions instead of a symbolic notation in the\n"
"       Roman numeral output (resource name "
"\"latin\").\n"
"-[no]ancientRoman    "
"This option allows you to set the abacus to "
"allow ancient Roman numerals instead of the\n"
"       modern in the Roman numeral output (resource name "
"\"ancientRoman\").\n"
"-[no]modernRoman     "
"This option allows you to set the abacus to "
"allow modern Roman numerals instead of the\n"
"       ancient on the Roman Hand abacus (resource name "
"\"modernRoman\").\n"
"-chinese    "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Chinese\" for the Chinese\n"
"       Suanpan.\n"
"-japanese   "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Japanese\" for the\n"
"       Japanese post-WWII Soroban.  "
"This is also similar to the Roman Hand Abacus.\n"
"-korean     "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Korean\" for the Korean\n"
"       Jupan or Japanese pre-WWII Soroban.\n"
"-russian    "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Russian\" for the Russian\n"
"       Schety.  To complete, specify \"piece\" resource to be 4.\n"
"-danish     "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Danish\" for the Danish\n"
"       Elementary School Abacus teaching aid.\n"
"-roman      "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Roman\" for the Roman\n"
"       Hand Abacus, note beads will move in slots.  "
"To complete, specify \"romanNumerals\".\n"
"-medieval   "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Medieval\" for the Medieval\n"
"       Counter, with counters instead of beads.\n"
"-generic    "
"This option specifies the format of the abacus "
"(resource name \"format\") to \"Generic\".  This option\n"
"       specifies a format that is more configurable by using "
"using resources, since there are few rules to govern its\n"
"       behavior.\n"
"-it         "
"This option specifies the country code of the museum of the "
"abacus in Museum of the Thermae, Rome.\n"
"-uk         "
"This option specifies the country code of the museum of the "
"abacus in British Museum in London.\n"
"-fr         "
"This option specifies the country code of the museum of the "
"abacus in Cabinet de medailles, Bibliotheque\n"
"       nationale, Paris.\n"
"-version    "
"This option tells you what version of xabacus you have."
};
#endif

#if defined(HAVE_MOTIF) || defined(WINVER)
static const char description1Help[] = {
"This is an implementation of the classic Chinese Abacus "
"(Suanpan) which has its origins in the 12th century.\n"
"\n"
"The device has two decks.  Each deck, separated by a "
"partition, normally has 13 rails on which are mounted beads.\n"
"Each rail on the top deck contains 1 or 2  beads, and each "
"rod on the bottom deck contains 4 or 5 beads.  Each bead on\n"
"the upper deck has a value of five, while each bead on the "
"lower deck has value of one.  Beads are considered counted,\n"
"when moved towards the partition separating the decks, i.e. "
"to add a value of one, a bead in the bottom deck is moved\n"
"up, and to add a value of 5, a bead in the top deck is moved "
"down.\n"
"\n"
"The basic operations of the abacus are addition and subtraction. "
" Multiplication can be done by mentally multiplying\n"
"the digits and adding up the intermediate results on the abacus. "
" Division would be similar where the intermediate\n"
"results are subtracted.  There are techniques like using your "
"thumb with forefinger which does not apply with mouse\n"
"entry.  Also with multiplication, one can carry out "
"calculations on different parts of the abacus for scratch work,\n"
"here it is nice to have a long abacus.\n"
"\n"
"The pre-WWII Japanese Abacus (Soroban) (or Korean Jupan) "
"is similar to the Chinese Abacus but has only one bead\n"
"per rail on the top deck.  The later Japanese Abacus was"
"further simplified to have only 4 beads per rail on the\n"
"bottom deck.\n"
"\n"
"The Russian Abacus was invented in the 17th century, here "
"the beads are moved from right to left.  It has colored\n"
"beads in the middle for ease of use.  Quarters represent "
"1/4 Rubles and are only  present historically on the Russian\n"
"Abacus (Schety).  Some of the older Schety have a extra place "
"for the 1/4 Kopek (quarter percent) as well as the 1/4\n"
"Ruble (quarter)."
};

static const char description2Help[] = {
"\n"
"The Danish Abacus was used in the early 20th century in "
"elementary schools as a teaching aid.\n"
"\n"
"The Roman Hand-Abacus predates the Chinese Abacus and is "
"very similar to the later Japanese Abacus, but seems to\n"
"have fallen out of use with the Fall of the Roman Empire "
"(at least 3 are in existence).  The Roman Abaci are brass\n"
"plates where the beads move in slots.  In addition to the "
"normal 7 columns of beads, they generally have 2 special\n"
"columns on the right side.  In two examples: the first "
"special column was for 12ths (12 uncia (ounces) = 1 as) and\n"
"had one extra bead in the bottom deck.  Also the last column "
"was a combination of halves, quarters, and twelfths of an\n"
"ounce and had no beads in the top deck and 4 beads at the "
"bottom (beads did not have to come to the top to be\n"
"counted but at one of 3 marked points, where the top bead "
"was for halves, the next bead for quarters, and the last two\n"
"beads for twelfths).  In another surviving example: the 2 "
"special columns were switched and the combination column\n"
"was broken into 3 separate slots.  If available, decimal "
"input is ignored.\n"
"\n"
"The Medieval Counter is a primitive form of the abacus and "
"was used in Europe as late as the 1600s.  It was useful\n"
"considering they were using it with Roman Numerals.   This "
"is similar to the Salamis Greek Tablet from 4th or 5th\n"
"Century BCE.\n"
"\n"
"The Mesoamerican Nepohualtzintzin is a Japanese Abacus "
"base 20.  The Mesoamericans had base 20 with the\n"
"exception of the 3rd decimal place where instead of "
"20 * 20 = 400 the third place marked 360 and the 4th place was\n"
"20 * 360, etc.  They independently created their own zero "
"(only Babylon (base 60) and India (base 10) have done this)\n"
"but the anomaly took away its true power.\n"
"\n"
"An easy way of figuring out time in seconds given hours, "
"minutes, and seconds, can be done on the abacus with\n"
"special anomaly \"watch\" settings.\n"
"\n"
"The Chinese Solid-and-Broken-Bar System is a base 12 "
"numbering system and not really an abacus.  When the\n"
"abacus is setup in this way though (topFactor 3, topNumber 3, "
"bottomNumber 2, base 12, displayBase 12), it is easy to\n"
"relate the two.\n"
"\n"
"The signed bead is an invention of the author and is not "
"present on any historical abacus (to his knowledge) and is\n"
"used to represent negatives.  \"New & Improved\" abacus "
"models have two auxiliary decks stacked above the principal\n"
"deck that enable multiplication, division, square-root, and "
"cube-root computations to be performed with equal ease as\n"
"addition and subtraction (well, so I have read)."
};

static const char featuresHelp[] = {
"Click \"mouse-left\" button on a bead you want to move.  The "
"beads will shift themselves to vacate the area of the\n"
"column that was clicked.\n"
"\n"
"Click \"mouse-right\" button, or press \"C\" or \"c\" keys, "
"to clear the abacus.\n"
"\n"
"Press \"O\" or \"o\" keys to toggle the demo mode.  "
"\n"
"Press \"$\" key to toggle the teach mode.\n"
"\n"
"In teach mode, \"+\" key toggles starting side to sum, "
"\"*\" key toggles for starting side for multiplicand.\n"
"\n"
"Press \"~\" or \"`\" keys to complement the beads on the rails.\n"
"\n"
"Press \"I\" or \"i\" keys to increment the number of rails.  "
"Press \"D\" or \"d\" keys to decrement the number of rails.\n"
"\n"
"Press \"F\" or \"f\" keys to switch between Chinese, "
"Japanese, Korean, Russian, Danish, Roman, and Medieval formats.\n"
"There is an extra \"Generic\" format, this allows one to "
"break some rules binding the other formats (for example,\n"
"if one wanted more beads on top deck than on bottom deck you "
"would use this, in addition to resource option changes).\n"
"\n"
"Press \"M\" or \"m\" keys to switch between it, uk, and fr "
"museum formats.\n"
"\n"
"Press \"V\" or \"v\" keys to toggle Roman Nvmerals.  "
"(Pardon humor/typo but ran out of letters).\n"
"\n"
"Press \"S\" or \"s\" keys to toggle the sign bead.\n"
"\n"
"Press \"U\" or \"u\" keys to toggle the availability of "
"quarter beads.  (Mutually exclusive to twelfth beads).\n"
"Intended for the Russian Abacus.\n"
"\n"
"Press \"T\" or \"t\" keys to toggle the availability of "
"twelfth beads.  (Mutually exclusive to quarter beads).\n"
"Intended for the Roman Abacus.\n"
"\n"
"Press \"P\" or \"p\" keys to toggle the availability of "
"quarter percent beads.  (Dependent on quarter beads (or\n"
"twelfth beads)).  Intended for the older Russian Abacus.\n"
"\n"
"Press \"B\" or \"b\" keys to toggle the availability of "
"subdecks.  (Dependent on twelfth beads (or quarter beads)\n"
"and Roman format).  Intended for the Roman Abacus, where "
"the lowest value of the two at bottom of the rightmost\n"
"column of beads are a twelfth of the column, second from "
"right.\n"
"\n"
"Press \"E\" or \"e\" keys to toggle the availability of "
"subdecks. (Dependent on twelfth beads (or quarter beads)\n"
"and Roman format).  Intended for the Roman Abacus, where "
"the lowest value of the three at bottom of the rightmost\n"
"column of beads are an eighth of the column, second from "
"right.\n"
"\n"
"Press \"L\" or \"l\" keys to toggle the availability of "
"anomaly bars.   Intended to used with Japanese Abacus and\n"
"base 20 for the Mesoamerican Abacus. (Mutually exclusive "
"to watch bars).\n"
"\n"
"Press \"W\" or \"w\" keys to toggle the availability of "
"watch bars.  Intended to represent seconds where hours\n"
"and minutes can be set.  (Mutually exclusive to anomaly "
"bars).\n"
"\n"
"Press \">\" or \".\" keys to speed up the movement of beads.  "
"Press \"<\" or \",\" keys to slow down the movement of beads.\n"
"\n"
"Press \"@\" key to toggle the sound.  "
"Press \"Esc\" key to hide program.\n"
"\n"
"Press \"Q\", \"q\", or \"CTRL-C\" keys to kill program.\n"
"\n"
"The abacus may be resized.  Beads will reshape depending "
"on the room they have.\n"
"\n"
"Demo Mode: in this mode, the abacus is controlled by the "
"program.  When started with the demo option, a second\n"
"window is presented that should be placed directly below the "
"abacus-window. Descriptive text, and user prompts are\n"
"displayed in this window.  Pressing 'q' during the demo "
"will quit it.  Clicking the left mouse-button with the\n"
"pointer in the window will restart the demo (beginning "
"of current lesson)."
};

static const char referencesHelp[] = {
"Luis Fernandes  http://www.ee.ryerson.ca/~elf/abacus/\n"
"Lee Kai-chen, How to Learn Lee's Abacus, 1958, 58 pages.\n"
"Abacus Guide Book, 57 pages.\n"
"Georges Ifrah, The Universal history of Numbers, Wiley Press "
"2000, pp 209-211, 288-294.\n"
"Review of above: http://www.ams.org/notices/200201/rev-dauben.pdf\n"
"David Eugene Smith, History of Mathematics Volume II, "
"Dover Publications, Inc 1958, pp 156-195."
};

#define DESCRIPTION_SIZE (sizeof (description1Help) + sizeof (description2Help) + 4)
char descriptionHelp[DESCRIPTION_SIZE];
#endif

static const char *formatChoices[] =
{
	"Chinese Suanpan",
	"Japanese Soroban",
	"Korean Jupan",
	"Russian Schety",
	"Danish School Abacus",
	"Roman Hand-abacus",
	"Medieval Counter",
	"Generic Abacus"
};

#define ENABLE_ANCIENT_ROMAN(romanNumerals) romanNumerals
#define ENABLE_LATIN(romanNumerals, bottomPiece) (romanNumerals && bottomPiece != 0)
#define ENABLE_QUARTER(bottomPiece) (bottomPiece != 6)
#define ENABLE_TWELFTH(bottomPiece) (bottomPiece != 4)
#ifdef PRECENT_DEPEND
#define ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition) \
  (bottomPiece != 0 && (((bottomPiecePercent != 0) ? 1 : 0) + decimalPosition >= 3))
#else
#define ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition) \
  (((bottomPiecePercent != 0) ? 1 : 0) + decimalPosition >= 2 + (bottomPiece != 0) ? 1 : 0)
#endif
#define ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot) \
  ((bottomPiece != 0) && (bottomPiecePercent == 0) && (decimalPosition == 3) && slot)
#define ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot) \
  (bottomPiece != 0 && bottomPiecePercent == 0 && decimalPosition == 3 && (subdeck == 0 || subbase != 8) && slot)
#define ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot) \
  (bottomPiece != 0 && bottomPiecePercent == 0 && decimalPosition == 3 && (subdeck == 0 || subbase != 12) && slot)
#define ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot) \
  ((bottomPiece != 0) && (bottomPiecePercent == 0) && (decimalPosition == 3) && (subdeck > 0) && slot)
#define ENABLE_MODERN_ROMAN(slot, anomaly, anomalySq) \
  (slot && anomaly == 0 && anomalySq == 0)
#define ENABLE_ANOMALY(anomaly, anomalySq) ((anomaly != 4) || (anomalySq != 4))
#define ENABLE_WATCH(anomaly, anomalySq) ((anomaly != 2) || (anomalySq != 0))

#define SET_QUARTER(bottomPiece, topPiece) \
  (bottomPiece == 4 && topPiece == 0)
#define SET_TWELFTH(bottomPiece, topPiece) \
  (bottomPiece == 6 && topPiece == 2)
#ifdef PRECENT_NODEPEND
#define SET_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, topPiecePercent) \
  (bottomPiece != 0 && bottomPiecePercent == 4 && topPiecePercent == 0)
#else
#define SET_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, topPiecePercent) \
  (bottomPiecePercent == 4 && topPiecePercent == 0)
#endif
#define SET_SUBDECK(bottomPiece, bottomPiecePercent, subdeck, subbase, slot) \
  (bottomPiece != 0 && bottomPiecePercent == 0 && subdeck == 3 && subbase == 12 && slot)
#define SET_EIGHTH(bottomPiece, bottomPiecePercent, subdeck, subbase, slot) \
  (bottomPiece != 0 && bottomPiecePercent == 0 && subdeck == 3 && subbase == 8 && slot)
#define SET_ANOMALY(anomaly, anomalySq) ((anomaly == 2) && (anomalySq == 0))
#define SET_WATCH(anomaly, anomalySq) ((anomaly == 4) && (anomalySq == 4))

#ifdef WINVER
#include "AbacusP.h"
#define TITLE "wabacus"

static AbacusRec widget;

char calc[120] = "";

#define MAX_DIGITS 256		/* This limits the number of rails */

static void
forceNormalParams(AbacusWidget w)
{
	int base = DEFAULT_BASE;
	int displayBase = DEFAULT_BASE;

	w->abacus.base = base;
	w->abacus.displayBase = displayBase;
}

static void
forceDemoParams(AbacusWidget w)
{
	int min;
	unsigned int mode;
	int bottomPiece, bottomPiecePercent;
	Boolean demo, sign;

	forceNormalParams(w);
	mode = w->abacus.mode;
	sign = w->abacus.sign;
	bottomPiece = w->abacus.decks[BOTTOM].piece;
	bottomPiecePercent = w->abacus.decks[BOTTOM].piecePercent;
	demo = w->abacus.demo;
	if (mode == GENERIC) {
		changeFormatAbacus(w);
	}
	min = ((sign) ? 1: 0) + ((bottomPiece) ? 1 : 0) +
		((bottomPiecePercent) ? 1 + w->abacus.shiftPercent : 0) +
		((demo) ? MIN_DEMO_RAILS : MIN_RAILS);
	while (w->abacus.rails < min) {
		incrementAbacus(w);
	}
}

static void
forceTeachParams(AbacusWidget w)
{
	w->abacus.displayBase = w->abacus.base;
	w->abacus.sign = False;
	w->abacus.anomaly = 0;
	w->abacus.anomalySq = 0;
	if (w->abacus.mode == GENERIC) {
		changeFormatAbacus(w);
	}
	(void) InvalidateRect(w->core.hWnd, NULL, TRUE);
}

void
setAbacus(AbacusWidget w, int reason)
{
	switch (reason) {
	case ACTION_SCRIPT:
#if 0
		{
			int deck, rail, number;

			deck = w->abacus.deck;
			rail = w->abacus.rail;
			number = w->abacus.number;
			(void) printf("%d %d %d %d\n", PRIMARY,
				deck, rail, number);
		}
#endif
		break;
	case ACTION_BASE_DEFAULT:
		forceNormalParams(w);
		break;
	case ACTION_DEMO_DEFAULT:
		clearAbacus(w);
		forceDemoParams(w);
		break;
	case ACTION_CLEAR:
		clearAbacus(w);
		if (w->abacus.demo) {
			clearAbacusDemo(w);
		}
		break;
	case ACTION_DECIMAL_CLEAR:
		clearDecimalAbacus(w);
		break;
	case ACTION_COMPLEMENT:
		complementAbacus(w);
		break;
	case ACTION_CLEAR_NODEMO:
		clearAbacus(w);
		break;
	case ACTION_HIDE:
		ShowWindow(w->core.hWnd, SW_SHOWMINIMIZED);
		break;
	case ACTION_CLEAR_QUERY:
		break;
	case ACTION_DEMO:
		toggleDemoAbacusDemo(w);
		break;
	case ACTION_NEXT:
		showNextAbacusDemo(w);
		break;
	case ACTION_REPEAT:
		showRepeatAbacusDemo(w);
		break;
	case ACTION_JUMP:
		showJumpAbacusDemo(w);
		break;
	case ACTION_MORE:
		showMoreAbacusDemo(w);
		break;
	case ACTION_INCREMENT:
	case ACTION_DECREMENT:
	case ACTION_FORMAT:
	case ACTION_ROMAN_NUMERALS:
	case ACTION_ANCIENT_ROMAN:
	case ACTION_LATIN:
	case ACTION_GROUP:
	case ACTION_SIGN:
	case ACTION_QUARTER:
	case ACTION_TWELFTH:
	case ACTION_QUARTER_PERCENT:
	case ACTION_SUBDECK:
	case ACTION_MUSEUM:
	case ACTION_MODERN_ROMAN:
	case ACTION_ANOMALY:
	case ACTION_WATCH:
		break;
	}
}

/* input dialog box handle */
HWND hCalcDlg = NULL;
HWND hDemoDlg = NULL;
HWND hTeachDlg = NULL;

void
setAbacusString(AbacusWidget w, int reason, char *string)
{
	(void) strncpy(calc, string, 120);
	if (reason == ACTION_SCRIPT || reason == ACTION_IGNORE) {
		char szBuf[MAX_DIGITS + 67];
		unsigned int mode = w->abacus.mode;

		(void) sprintf(szBuf, "%s  %s", string,
			(mode >= MAX_FORMATS) ? "Abacus" :
			formatChoices[mode]);
		SetWindowText(w->core.hWnd, (LPSTR) szBuf);
	}
	SetDlgItemText(hCalcDlg, ACTION_CALC, calc);
}

static LRESULT CALLBACK
about(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_COMMAND &&
			(LOWORD(wParam) == IDOK ||
			LOWORD(wParam) == IDCANCEL)) {
		(void) EndDialog(hDlg, TRUE);
		return TRUE;
	}
	return FALSE;
}

static LRESULT CALLBACK
teach(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_COMMAND) {
		if (LOWORD(wParam) == IDOK) {
			char str[120];
			GetDlgItemText(hDlg, ACTION_TEACH, str, 120);
			widget.core.hDC = GetDC(widget.core.hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			widget.abacus.teach = True;
			(void) teachStep(&widget, str, 0);
			if (strcmp(str, "q") == 0) {
				destroyAbacus(&widget, NULL);
			}
			/*setAbacusString(&widget, ACTION_IGNORE, str);*/
			(void) ReleaseDC(widget.core.hWnd,
				widget.core.hDC);
			/*SetDlgItemText(hDlg, ACTION_TEACH, calc);*/
			return TRUE;
		} else if (LOWORD(wParam) == IDCANCEL) {
			widget.abacus.teach = False;
			(void) EndDialog(hDlg, TRUE);
			hTeachDlg = NULL;
			return TRUE;
		}
	}
	return FALSE;
}

static LRESULT CALLBACK
calculation(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_COMMAND) {
		if (LOWORD(wParam) == IDOK) {
			int i, j = 0;
			char mathBuff[120];
			GetDlgItemText(hDlg, ACTION_CALC, mathBuff, 120);
			widget.core.hWnd = GetParent(hDlg);
			widget.core.hDC = GetDC(widget.core.hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			/* strip out blanks */
			for (i = 0; i < (int) strlen(mathBuff); i++) {
				if (mathBuff[i] == '[' || mathBuff[i] == ']') {
					mathBuff[j] = '\0';
					break;
				} else if (mathBuff[i] != ' ' &&
						mathBuff[i] != '\t') {
					mathBuff[j] = mathBuff[i];
					j++;
				}
			}
			calculate(&widget, mathBuff, 0);
			(void) ReleaseDC(widget.core.hWnd,
				widget.core.hDC);
			SetDlgItemText(hDlg, ACTION_CALC, calc);
			return TRUE;
		} else if (LOWORD(wParam) == IDCANCEL) {
			(void) DestroyWindow(hDlg);
			hCalcDlg = NULL;
			return TRUE;
		}
	}
	return FALSE;
}

static LRESULT CALLBACK
demo(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_COMMAND) {
		if (LOWORD(wParam) == IDOK) {
			char str[120];
			GetDlgItemText(hDlg, ACTION_DEMO1, str, 120);
			widget.core.hWnd = GetParent(hDlg);
			widget.core.hDC = GetDC(widget.core.hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			//demo(&widget, str, 0);
			(void) ReleaseDC(widget.core.hWnd,
				widget.core.hDC);
			SetDlgItemText(hDlg, ACTION_DEMO4, "fix me");
			return TRUE;
		} else if (LOWORD(wParam) == IDCANCEL) {
			widget.abacus.demo = False;
			(void) EndDialog(hDlg, TRUE);
			hDemoDlg = NULL;
			return TRUE;
		}
	}
	return FALSE;
}

static LRESULT CALLBACK
WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HBRUSH brush = (HBRUSH) NULL;
	PAINTSTRUCT paint;

	widget.core.hWnd = hWnd;
	if (GetFocus()) {
		if (!widget.abacus.focus) {
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_BRUSH));
			enterAbacus(&widget);
			(void) EndPaint(hWnd, &paint);
		}
	} else {
		if (widget.abacus.focus) {
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_BRUSH));
			leaveAbacus(&widget);
			(void) EndPaint(hWnd, &paint);
		}
	}
	switch (message) {
	case WM_CREATE:
		initializeAbacus(&widget, brush);
		widget.core.hMenu = GetMenu(widget.core.hWnd);
		EnableMenuItem(widget.core.hMenu, ACTION_ANCIENT_ROMAN,
			(unsigned) (ENABLE_ANCIENT_ROMAN(widget.abacus.romanNumerals) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_LATIN,
			(unsigned) (ENABLE_LATIN(widget.abacus.romanNumerals, widget.abacus.decks[BOTTOM].piece) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_QUARTER,
			(unsigned) (ENABLE_QUARTER(widget.abacus.decks[BOTTOM].piece) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_TWELFTH,
			(unsigned) (ENABLE_TWELFTH(widget.abacus.decks[BOTTOM].piece) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_QUARTER_PERCENT,
			(unsigned) (ENABLE_QUARTER_PERCENT(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_SUBDECK,
			(unsigned) (ENABLE_SUBDECK(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_EIGHTH,
			(unsigned) (ENABLE_EIGHTH(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_MUSEUM,
			(unsigned) (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_MODERN_ROMAN,
			(unsigned) (ENABLE_MODERN_ROMAN(widget.abacus.slot, widget.abacus.anomaly, widget.abacus.anomalySq) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_ANOMALY,
			(unsigned) (ENABLE_ANOMALY(widget.abacus.anomaly, widget.abacus.anomalySq) ?
			MF_ENABLED : MF_GRAYED));
		EnableMenuItem(widget.core.hMenu, ACTION_WATCH,
			(unsigned) (ENABLE_WATCH(widget.abacus.anomaly, widget.abacus.anomalySq) ?
			MF_ENABLED : MF_GRAYED));
		initializeAbacusDemo(&widget);
		break;
	case WM_DESTROY:
		destroyAbacus(&widget, brush);
#if 0
		if (widget.abacus.demo)
			destroyAbacusDemo();
#endif
		break;
	case WM_SIZE:
		resizeAbacus(&widget);
		(void) InvalidateRect(hWnd, NULL, TRUE);
		break;
	case WM_PAINT:
		widget.core.hDC = BeginPaint(hWnd, &paint);
		(void) SelectObject(widget.core.hDC,
			GetStockObject(NULL_PEN));
		exposeAbacus(&widget);
		if (widget.abacus.demo) {
			exposeAbacusDemo(&widget);
		}
		(void) EndPaint(hWnd, &paint);
		break;
	case WM_LBUTTONDOWN:
		widget.core.hDC = GetDC(hWnd);
		(void) SelectObject(widget.core.hDC,
			GetStockObject(NULL_PEN));
#if 0
		if (widget.abacus.demo) {
			clearAbacus(&widget);
			clearAbacusDemo(&widget);
		} else
#endif
		{
			selectAbacus(&widget, LOWORD(lParam),
				HIWORD(lParam));
		}
		(void) ReleaseDC(hWnd, widget.core.hDC);
		break;
	case WM_LBUTTONUP:
	case WM_NCMOUSEMOVE:
		widget.core.hDC = GetDC(hWnd);
		(void) SelectObject(widget.core.hDC,
			GetStockObject(NULL_PEN));
		releaseAbacus(&widget, LOWORD(lParam), HIWORD(lParam));
		(void) ReleaseDC(hWnd, widget.core.hDC);
		break;
#if (_WIN32_WINNT >= 0x0400) || (_WIN32_WINDOWS > 0x0400)
	case WM_MOUSEWHEEL:
		widget.core.hDC = GetDC(hWnd);
		(void) SelectObject(widget.core.hDC,
			GetStockObject(NULL_PEN));
		{
			int zDelta = ((short) HIWORD(wParam));
			POINT cursor, origin;

			origin.x = 0, origin.y = 0;
			ClientToScreen(hWnd, &origin);
			(void) GetCursorPos(&cursor);
			if (zDelta > (WHEEL_DELTA >> 1)) {
				moveAbacusInput(&widget, cursor.x - origin.x,
					cursor.y - origin.y, '8',
					(GetKeyState(VK_CONTROL) >> 1) ? 1 : 0);
				if (GetKeyState(VK_CONTROL) >> 1) {
					resizeAbacus(&widget);
					(void) InvalidateRect(hWnd, NULL, TRUE);
				}
			} else if (zDelta < -(WHEEL_DELTA >> 1)) {
				moveAbacusInput(&widget, cursor.x - origin.x,
					cursor.y - origin.y, '2',
					(GetKeyState(VK_CONTROL) >> 1) ? 1 : 0);
				if (GetKeyState(VK_CONTROL) >> 1) {
					resizeAbacus(&widget);
					(void) InvalidateRect(hWnd, NULL, TRUE);
				}
			}
		}
		(void) ReleaseDC(hWnd, widget.core.hDC);
		break;
#endif
	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case ACTION_EXIT:
			if (hDemoDlg != NULL) {
				(void) EndDialog(hDemoDlg, TRUE);
				hDemoDlg = NULL;
			}
			if (widget.abacus.demo)
				toggleDemoAbacusDemo(&widget);
			else
				destroyAbacus(&widget, brush);
			break;
		case ACTION_HIDE:
			hideAbacus(&widget);
			break;
		case ACTION_CLEAR:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			clearAbacus(&widget);
			if (widget.abacus.demo) {
				clearAbacusDemo(&widget);
			}
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_RIGHT:
		case ACTION_LEFT:
		case ACTION_UP:
		case ACTION_DOWN:
			{
				int action = LOWORD(wParam);
				POINT cursor, origin;
				char letter;

				if (action == ACTION_RIGHT || action == ACTION_LEFT)
					letter = ((action == ACTION_RIGHT) ? '6' : '4');
				else
					letter = ((action == ACTION_UP) ? '8' : '2');
				widget.core.hDC = GetDC(hWnd);
				(void) SelectObject(widget.core.hDC,
					GetStockObject(NULL_PEN));
				origin.x = 0, origin.y = 0;
				ClientToScreen(hWnd, &origin);
				(void) GetCursorPos(&cursor);
				(void) moveAbacusInput(&widget,
					cursor.x - origin.x,
					cursor.y - origin.y,
					letter, FALSE);
				(void) ReleaseDC(hWnd, widget.core.hDC);
			}
			break;
		case ACTION_INCX:
		case ACTION_DECX:
		case ACTION_INCY:
		case ACTION_DECY:
			{
				int action = LOWORD(wParam);
				POINT cursor, origin;
				char letter;

				if (action == ACTION_INCX || action == ACTION_DECX)
					letter = ((action == ACTION_INCX) ? '6' : '4');
				else
					letter = ((action == ACTION_INCY) ? '8' : '2');
				widget.core.hDC = GetDC(hWnd);
				(void) SelectObject(widget.core.hDC,
					GetStockObject(NULL_PEN));
				origin.x = 0, origin.y = 0;
				ClientToScreen(hWnd, &origin);
				(void) GetCursorPos(&cursor);
				(void) moveAbacusInput(&widget,
					cursor.x - origin.x,
					cursor.y - origin.y,
					letter, TRUE);
				(void) ReleaseDC(hWnd, widget.core.hDC);
			}
			break;
		case ACTION_COMPLEMENT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			complementAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_INCREMENT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			incrementAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_DECREMENT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			decrementAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_FORMAT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			if (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, !widget.abacus.slot)) {
				clearDecimalAbacus(&widget);
			}
			changeFormatAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_SUBDECK,
				(unsigned) (ENABLE_SUBDECK(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_EIGHTH,
				(unsigned) (ENABLE_EIGHTH(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_MUSEUM,
				(unsigned) (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_MODERN_ROMAN,
				(unsigned) (ENABLE_MODERN_ROMAN(widget.abacus.slot, widget.abacus.anomaly, widget.abacus.anomalySq) ?
				MF_ENABLED : MF_GRAYED));
			if (widget.abacus.demo) {
				clearAbacusDemo(&widget);
				clearAbacus(&widget);
			}
			break;
		case ACTION_ROMAN_NUMERALS:
			toggleRomanNumeralsAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_ANCIENT_ROMAN,
				(unsigned) (ENABLE_ANCIENT_ROMAN(widget.abacus.romanNumerals) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_LATIN,
				(unsigned) (ENABLE_LATIN(widget.abacus.romanNumerals, widget.abacus.decks[BOTTOM].piece) ?
				MF_ENABLED : MF_GRAYED));
			break;
		case ACTION_ANCIENT_ROMAN:
			toggleAncientRomanAbacus(&widget);
			break;
		case ACTION_LATIN:
			toggleLatinAbacus(&widget);
			break;
		case ACTION_GROUP:
			toggleGroupingAbacus(&widget);
			break;
		case ACTION_SIGN:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			toggleNegativeSignAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_QUARTER:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			if (ENABLE_MUSEUM(((widget.abacus.decks[BOTTOM].piece != 4) ? 4 : 0), widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot)) {
				clearDecimalAbacus(&widget);
			}
			toggleQuartersAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_LATIN,
				(unsigned) (ENABLE_LATIN(widget.abacus.romanNumerals, widget.abacus.decks[BOTTOM].piece) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_TWELFTH,
				(unsigned) (ENABLE_TWELFTH(widget.abacus.decks[BOTTOM].piece) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_QUARTER_PERCENT,
				(unsigned) (ENABLE_QUARTER_PERCENT(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_SUBDECK,
				(unsigned) (ENABLE_SUBDECK(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_EIGHTH,
				(unsigned) (ENABLE_EIGHTH(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_MUSEUM,
				(unsigned) (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_TWELFTH:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			if (ENABLE_MUSEUM(((widget.abacus.decks[BOTTOM].piece != 6) ? 6 : 0), widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot)) {
				clearDecimalAbacus(&widget);
			}
			toggleTwelfthsAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_LATIN,
				(unsigned) (ENABLE_LATIN(widget.abacus.romanNumerals, widget.abacus.decks[BOTTOM].piece) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_QUARTER,
				(unsigned) (ENABLE_QUARTER(widget.abacus.decks[BOTTOM].piece) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_QUARTER_PERCENT,
				(unsigned) (ENABLE_QUARTER_PERCENT(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_SUBDECK,
				(unsigned) (ENABLE_SUBDECK(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_EIGHTH,
				(unsigned) (ENABLE_EIGHTH(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_MUSEUM,
				(unsigned) (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_QUARTER_PERCENT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			if (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, ((widget.abacus.decks[BOTTOM].piecePercent != 4) ? 4 : 0), widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot)) {
				clearDecimalAbacus(&widget);
			}
			toggleQuarterPercentsAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_SUBDECK,
				(unsigned) (ENABLE_SUBDECK(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_EIGHTH,
				(unsigned) (ENABLE_EIGHTH(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_MUSEUM,
				(unsigned) (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_SUBDECK:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			if (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, ((widget.abacus.subdeck != 3) ? 3 : 0), widget.abacus.slot)) {
				clearDecimalAbacus(&widget);
			}
			toggleSubdecksAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_EIGHTH,
				(unsigned) (ENABLE_EIGHTH(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_EIGHTH:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			if (ENABLE_MUSEUM(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, ((widget.abacus.subdeck != 3) ? 3 : 0), widget.abacus.slot)) {
				clearDecimalAbacus(&widget);
			}
			toggleEighthsAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_SUBDECK,
				(unsigned) (ENABLE_SUBDECK(widget.abacus.decks[BOTTOM].piece, widget.abacus.decks[BOTTOM].piecePercent, widget.abacus.decimalPosition, widget.abacus.subdeck, widget.abacus.subbase, widget.abacus.slot) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_MUSEUM:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			changeMuseumAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_MODERN_ROMAN:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			toggleModernRomanAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_ANOMALY:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			toggleAnomalyAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_MODERN_ROMAN,
				(unsigned) (ENABLE_MODERN_ROMAN(widget.abacus.slot, widget.abacus.anomaly, widget.abacus.anomalySq) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_WATCH,
				(unsigned) (ENABLE_WATCH(widget.abacus.anomaly, widget.abacus.anomalySq) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_WATCH:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			toggleWatchAbacus(&widget);
			EnableMenuItem(widget.core.hMenu, ACTION_MODERN_ROMAN,
				(unsigned) (ENABLE_MODERN_ROMAN(widget.abacus.slot, widget.abacus.anomaly, widget.abacus.anomalySq) ?
				MF_ENABLED : MF_GRAYED));
			EnableMenuItem(widget.core.hMenu, ACTION_ANOMALY,
				(unsigned) (ENABLE_ANOMALY(widget.abacus.anomaly, widget.abacus.anomalySq) ?
				MF_ENABLED : MF_GRAYED));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_RIGHT_TO_LEFT_ADD:
			toggleRightToLeftAddAbacus(&widget);
			break;
		case ACTION_RIGHT_TO_LEFT_MULT:
			toggleRightToLeftMultAbacus(&widget);
			break;
#if 0
		case ACTION_VERTICAL:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			VerticalAbacus(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
#endif
		case ACTION_DEMO:
#define DEMO_DIALOG
#ifdef DEMO_DIALOG
			if (hDemoDlg != NULL) {
				(void) DestroyWindow(hDemoDlg);
				hDemoDlg = NULL;
				widget.abacus.demo = False;
			}
			hDemoDlg = CreateDialog(widget.core.hInstance,
				"Demo", hWnd,
				(DLGPROC) demo);
#endif
			if (!widget.abacus.demo)
				toggleDemoAbacusDemo(&widget);
			resizeAbacus(&widget);
			(void) InvalidateRect(hWnd, NULL,
				TRUE);
			break;
		case ACTION_NEXT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			/* showNextAbacus(&widget); */
			if (widget.abacus.demo)
				showNextAbacusDemo(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_REPEAT:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			/* showRepeatAbacus(&widget); */
			if (widget.abacus.demo)
				showRepeatAbacusDemo(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_JUMP:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			/* showJumpAbacus(&widget); */
			if (widget.abacus.demo)
				showJumpAbacusDemo(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_CHAPTER1:
		case ACTION_CHAPTER2:
		case ACTION_CHAPTER3:
		case ACTION_CHAPTER4:
		case ACTION_CHAPTER5:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			/* showChapterAbacus(&widget); */
			if (widget.abacus.demo)
				showChapterAbacusDemo(&widget,
					(unsigned int) (LOWORD(wParam) - ACTION_CHAPTER1));
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_MORE:
			widget.core.hDC = GetDC(hWnd);
			(void) SelectObject(widget.core.hDC,
				GetStockObject(NULL_PEN));
			/* showMoreAbacus(&widget); */
			if (widget.abacus.demo)
				showMoreAbacusDemo(&widget);
			(void) ReleaseDC(hWnd,
				widget.core.hDC);
			break;
		case ACTION_SOUND:
			toggleSoundAbacus(&widget);
			break;
		case ACTION_SPEED_UP:
			speedUpAbacus(&widget);
			break;
		case ACTION_SLOW_DOWN:
			slowDownAbacus(&widget);
			break;
		case ACTION_CALC:
			if (hCalcDlg != NULL) {
				(void) DestroyWindow(hCalcDlg);
				hCalcDlg = NULL;
			}
			hCalcDlg = CreateDialog(widget.core.hInstance,
				"Calculate", hWnd,
				(DLGPROC) calculation);
			break;
		case ACTION_TEACH:
			/*(void) DialogBox(widget.core.hInstance,
				"Teach", hWnd,
				(DLGPROC) teach);*/
			forceTeachParams(&widget);
			if (hTeachDlg != NULL) {
				(void) DestroyWindow(hTeachDlg);
				hTeachDlg = NULL;
				widget.abacus.teach = False;
			}
			hTeachDlg = CreateDialog(widget.core.hInstance,
				"Teach", hWnd,
				(DLGPROC) teach);
			break;
		case ACTION_DESCRIPTION:
			strcpy(descriptionHelp, description1Help);
			strcat(descriptionHelp, "\n");
			strcat(descriptionHelp, description2Help);
			(void) MessageBox(hWnd, descriptionHelp,
				"Description", MB_OK | MB_ICONQUESTION);
			break;
		case ACTION_FEATURES:
			(void) MessageBox(hWnd, featuresHelp,
				"Features", MB_OK | MB_ICONEXCLAMATION);
			break;
		case ACTION_REFERENCES:
			(void) MessageBox(hWnd, referencesHelp,
				"References", MB_OK | MB_ICONINFORMATION);
			break;
		case ACTION_ABOUT:
			(void) DialogBox(widget.core.hInstance,
				"About", hWnd, (DLGPROC) about);
			break;
		}
		break;
	default:
		return (DefWindowProc(hWnd, message, wParam, lParam));
	}
	return FALSE;
}

void drawDemoText(const char* line, int i)
{
	if (hDemoDlg == NULL || line == NULL)
		return;
	SetDlgItemText(hDemoDlg, ACTION_DEMO1 + i, line);
}

void drawTeachText(const char* line, int i)
{
	if (hTeachDlg == NULL || line == NULL)
		return;
	SetDlgItemText(hTeachDlg, ACTION_TEACH1 + i, line);
}

int WINAPI
WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,
		int numCmdShow)
{
	HWND hWnd;
	MSG msg;
	WNDCLASS wc;
	HACCEL hAccel;

	if (!hPrevInstance) {
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = WindowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = LoadIcon(hInstance, TITLE);
		wc.hCursor = LoadCursor((HINSTANCE) NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) GetStockObject(GRAY_BRUSH);
		wc.lpszMenuName = TITLE;
		wc.lpszClassName = TITLE;
		if (!RegisterClass(&wc))
			return FALSE;
	}
	widget.core.hInstance = hInstance;
	hWnd = CreateWindow(TITLE,
		TITLE,
		WS_OVERLAPPEDWINDOW,
		(signed) CW_USEDEFAULT,
		(signed) CW_USEDEFAULT,
		(signed) CW_USEDEFAULT,
		(signed) CW_USEDEFAULT,
		HWND_DESKTOP,
		(HMENU) NULL,
		hInstance,
		(XtPointer) NULL);
	if (!hWnd)
		return FALSE;
	hAccel = (HACCEL) LoadAccelerators(hInstance, TITLE);
	(void) ShowWindow(hWnd, numCmdShow);
	(void) UpdateWindow(hWnd);
	while (GetMessage(&msg, (HWND) NULL, 0, 0)) {
		if (!IsDialogMessage(hCalcDlg, &msg) &&
				!IsDialogMessage(hTeachDlg, &msg)) {
			if (!TranslateAccelerator(hWnd, hAccel, &msg)) {
				(void) TranslateMessage(&msg);
				(void) DispatchMessage(&msg);
			}
		}
	}
	return (int) msg.wParam;
}

#else
#include "xwin.h"
#include <X11/Shell.h>
#include <X11/cursorfont.h>
#ifdef HAVE_MOTIF
#include <Xm/PanedW.h>
#include <Xm/RowColumn.h>
#include <Xm/LabelG.h>
#include <Xm/MessageB.h>
#include <Xm/PushBG.h>
#include <Xm/CascadeB.h>
#include <Xm/Label.h>
#if defined(USE_SPIN) || defined(USE_BASE_SPIN) || defined(USE_DELAY_SPIN)
#include <Xm/SSpinB.h>
#endif
#if !defined(USE_SPIN) || !defined(USE_BASE_SPIN) || !defined(USE_DELAY_SPIN)
#include <Xm/Scale.h>
#endif
#include <Xm/ToggleB.h>
#include <Xm/ToggleBG.h>
#include <Xm/Text.h>
#include <Xm/Form.h>
#ifdef MOUSEBITMAPS
#include "pixmaps/mouse-l.xbm"
#include "pixmaps/mouse-r.xbm"
#endif
#endif
#include "Abacus.h"
#ifdef HAVE_XPM
#include <X11/xpm.h>
#ifdef CONSTPIXMAPS
#include "16x16/abacus.xpm"
#include "22x22/abacus.xpm"
#include "24x24/abacus.xpm"
#include "32x32/abacus.xpm"
#include "48x48/abacus.xpm"
#include "64x64/abacus.xpm"
#else
#include "pixmaps/16x16/abacus.xpm"
#include "pixmaps/22x22/abacus.xpm"
#include "pixmaps/24x24/abacus.xpm"
#include "pixmaps/32x32/abacus.xpm"
#include "pixmaps/48x48/abacus.xpm"
#include "pixmaps/64x64/abacus.xpm"
#endif
#define RESIZE_XPM(s) ((char **) (((s)<=32)?\
(((s)<=22)?(((s)<=16)?abacus_16x16:abacus_22x22):\
(((s)<=24)?abacus_24x24:abacus_32x32)):\
(((s)<=48)?abacus_48x48:abacus_64x64)))
#endif
#include "pixmaps/64x64/abacus.xbm"
#define DEFINE_XBM (char *) abacus_64x64_bits, abacus_64x64_width, abacus_64x64_height

#define FILE_NAME_LENGTH 1024

#ifdef HAVE_MOTIF
#ifdef USE_SPIN
#define MAX_RAILS 99		/* Higher because of roll over */
#else
#define MAX_RAILS 24		/* Arbitrary but lower for precision of slider */
#endif
#define SCROLL_SIZE 30		/* A page */

static const char *museumChoices[] =
{
	"it", "uk", "fr"
};

#else
/* Needs Motif */
#ifdef LEE_ABACUS
#undef LEE_ABACUS
#endif
#define MAX_DIGITS 256		/* This limits the number of rails */
#define TITLE_LENGTH (MAX_DIGITS+FILE_NAME_LENGTH+3)
#endif

#ifdef HAVE_MOTIF
static Widget mainPanel, teachRowCol;
static Widget sizeChanger, abacusBaseChanger, displayBaseChanger;
static Widget delayChanger;
static Widget formatMenu, formatSubMenu;
static Widget formatOptions[MAX_MODES];
#ifdef LEE_ABACUS
static Widget leftAuxAbacus = NULL, rightAuxAbacus = NULL;
static Widget leftAuxTracker = NULL, rightAuxTracker = NULL;
#endif
static Widget romanNumeralsMenuItem = NULL, ancientRomanMenuItem = NULL;
static Widget latinMenuItem = NULL, groupMenuItem = NULL, signMenuItem = NULL;
static Widget quarterMenuItem = NULL, twelfthMenuItem = NULL;
static Widget quarterPercentMenuItem = NULL;
static Widget secondaryRailsMenu = NULL;
static Widget subdeckMenuItem = NULL, eighthMenuItem = NULL;
static Widget museumMenu = NULL;
static Widget modernRomanMenuItem = NULL;
static Widget anomalyMenuItem = NULL, watchMenuItem = NULL;
static Widget rightToLeftAddMenuItem = NULL, rightToLeftMultMenuItem = NULL;
static Widget soundMenuItem = NULL;
static Widget tracker, teachTracker = NULL, teachLine[3];
static Widget baseDialog = NULL, delayDialog = NULL;
static Widget descriptionDialog, featuresDialog;
static Widget synopsisDialog, optionsDialog;
static Widget referencesDialog, aboutDialog;
static Widget clearDialog;
static char *mathBuffer = NULL;
static Boolean baseSet = False;
static Arg args[10];
#else
static Widget shell = NULL;
static char titleDsp[TITLE_LENGTH];
#endif
static Pixmap pixmap = None;
static char titleDspDemo[FILE_NAME_LENGTH + 6];
static Widget topLevel, abacus, abacusDemo = NULL;
static char *progDsp;

static void
usage(char *programName)
{
	unsigned int i;

	(void) fprintf(stderr, "usage: %s\n", programName);
	for (i = 0; i < strlen(synopsisHelp); i++) {
		if (i == 0 || synopsisHelp[i - 1] == '\n')
			(void) fprintf(stderr, "\t");
		(void) fprintf(stderr, "%c", (synopsisHelp[i]));
	}
	(void) fprintf(stderr, "\n");
	exit(1);
}

static XrmOptionDescRec options[] =
{
	{(char *) "-mono", (char *) "*abacus.mono", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nomono", (char *) "*abacus.mono", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-rv", (char *) "*abacus.reverseVideo", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-reverse", (char *) "*abacus.reverseVideo", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-norv", (char *) "*abacus.reverseVideo", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-noreverse", (char *) "*abacus.reverseVideo", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-foreground", (char *) "*abacus.Foreground", XrmoptionSepArg, NULL},
	{(char *) "-fg", (char *) "*abacus.Foreground", XrmoptionSepArg, NULL},
	{(char *) "-background", (char *) "*Background", XrmoptionSepArg, NULL},
	{(char *) "-bg", (char *) "*Background", XrmoptionSepArg, NULL},
	{(char *) "-border", (char *) "*abacus.borderColor", XrmoptionSepArg, NULL},
	{(char *) "-bd", (char *) "*abacus.borderColor", XrmoptionSepArg, NULL},
	{(char *) "-frame", (char *) "*abacus.frameColor", XrmoptionSepArg, NULL},
	{(char *) "-primaryBeadColor", (char *) "*abacus.primaryBeadColor", XrmoptionSepArg, NULL},
	{(char *) "-leftAuxBeadColor", (char *) "*abacus.leftAuxBeadColor", XrmoptionSepArg, NULL},
	{(char *) "-rightAuxBeadColor", (char *) "*abacus.rightAuxBeadColor", XrmoptionSepArg, NULL},
	{(char *) "-secondaryBeadColor", (char *) "*abacus.secondaryBeadColor", XrmoptionSepArg, NULL},
	{(char *) "-highlightBeadColor", (char *) "*abacus.highlightBeadColor", XrmoptionSepArg, NULL},
	{(char *) "-primaryRailColor", (char *) "*abacus.primaryRailColor", XrmoptionSepArg, NULL},
	{(char *) "-secondaryRailColor", (char *) "*abacus.secondaryRailColor", XrmoptionSepArg, NULL},
	{(char *) "-highlightRailColor", (char *) "*abacus.highlightRailColor", XrmoptionSepArg, NULL},
	{(char *) "-lineRailColor", (char *) "*abacus.lineRailColor", XrmoptionSepArg, NULL},
	{(char *) "-bumpSound", (char *) "*abacus.bumpSound", XrmoptionSepArg, NULL},
	{(char *) "-moveSound", (char *) "*abacus.moveSound", XrmoptionSepArg, NULL},
	{(char *) "-sound", (char *) "*abacus.sound", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nosound", (char *) "*abacus.sound", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-delay", (char *) "*abacus.delay", XrmoptionSepArg, NULL},
	{(char *) "-demo", (char *) "*abacus.demo", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nodemo", (char *) "*abacus.demo", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-script", (char *) "*abacus.script", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-noscript", (char *) "*abacus.script", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-demopath", (char *) "*abacus.demoPath", XrmoptionSepArg, NULL},
	{(char *) "-demofont", (char *) "*abacus.demoFont", XrmoptionSepArg, NULL},
	{(char *) "-demofn", (char *) "*abacus.demoFont", XrmoptionSepArg, NULL},
	{(char *) "-demoforeground", (char *) "*abacus.demoForeground", XrmoptionSepArg, NULL},
	{(char *) "-demofg", (char *) "*abacus.demoForeground", XrmoptionSepArg, NULL},
	{(char *) "-demobackground", (char *) "*abacus.demoBackground", XrmoptionSepArg, NULL},
	{(char *) "-demobg", (char *) "*abacus.demoBackground", XrmoptionSepArg, NULL},
	{(char *) "-teach", (char *) "*abacus.teach", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-noteach", (char *) "*abacus.teach", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-rightToLeftAdd", (char *) "*abacus.rightToLeftAdd", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-norightToLeftAdd", (char *) "*abacus.rightToLeftAdd", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-rightToLeftMult", (char *) "*abacus.rightToLeftMult", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-norightToLeftMult", (char *) "*abacus.rightToLeftMult", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-lee", (char *) "*abacus.lee", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nolee", (char *) "*abacus.lee", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-rails", (char *) "*abacus.rails", XrmoptionSepArg, NULL},
	{(char *) "-rightAuxRails", (char *) "*abacus.rightAuxRails", XrmoptionSepArg, NULL},
	{(char *) "-leftAuxRails", (char *) "*abacus.leftAuxRails", XrmoptionSepArg, NULL},
	{(char *) "-vertical", (char *) "*abacus.vertical", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-novertical", (char *) "*abacus.vertical", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-colorScheme", (char *) "*abacus.colorScheme", XrmoptionSepArg, NULL},
	{(char *) "-slot", (char *) "*abacus.slot", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-noslot", (char *) "*abacus.slot", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-diamond", (char *) "*abacus.diamond", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nodiamond", (char *) "*abacus.diamond", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-railIndex", (char *) "*abacus.railIndex", XrmoptionSepArg, NULL},
	{(char *) "-topOrient", (char *) "*abacus.topOrient", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-notopOrient", (char *) "*abacus.topOrient", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-bottomOrient", (char *) "*abacus.bottomOrient", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nobottomOrient", (char *) "*abacus.bottomOrient", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-topNumber", (char *) "*abacus.topNumber", XrmoptionSepArg, NULL},
	{(char *) "-bottomNumber", (char *) "*abacus.bottomNumber", XrmoptionSepArg, NULL},
	{(char *) "-topFactor", (char *) "*abacus.topFactor", XrmoptionSepArg, NULL},
	{(char *) "-bottomFactor", (char *) "*abacus.bottomFactor", XrmoptionSepArg, NULL},
	{(char *) "-topSpaces", (char *) "*abacus.topSpaces", XrmoptionSepArg, NULL},
	{(char *) "-bottomSpaces", (char *) "*abacus.bottomSpaces", XrmoptionSepArg, NULL},
	{(char *) "-topPiece", (char *) "*abacus.topPiece", XrmoptionSepArg, NULL},
	{(char *) "-bottomPiece", (char *) "*abacus.bottomPiece", XrmoptionSepArg, NULL},
	{(char *) "-topPiecePercent", (char *) "*abacus.topPiecePercent", XrmoptionSepArg, NULL},
	{(char *) "-bottomPiecePercent", (char *) "*abacus.bottomPiecePercent", XrmoptionSepArg, NULL},
	{(char *) "-shiftPercent", (char *) "*abacus.shiftPercent", XrmoptionSepArg, NULL},
	{(char *) "-subdeck", (char *) "*abacus.subdeck", XrmoptionSepArg, NULL},
	{(char *) "-subbead", (char *) "*abacus.subbead", XrmoptionSepArg, NULL},
	{(char *) "-sign", (char *) "*abacus.sign", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nosign", (char *) "*abacus.sign", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-decimalPosition", (char *) "*abacus.decimalPosition", XrmoptionSepArg, NULL},
	{(char *) "-group", (char *) "*abacus.group", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nogroup", (char *) "*abacus.group", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-groupSize", (char *) "*abacus.groupSize", XrmoptionSepArg, NULL},
	{(char *) "-decimalComma", (char *) "*abacus.decimalComma", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nodecimalComma", (char *) "*abacus.decimalComma", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-base", (char *) "*abacus.base", XrmoptionSepArg, NULL},
	{(char *) "-eighth", (char *) "*abacus.subbase", XrmoptionNoArg, (char *) "8"},
	{(char *) "-noeighth", (char *) "*abacus.subbase", XrmoptionNoArg, (char *) "12"},
	{(char *) "-anomaly", (char *) "*abacus.anomaly", XrmoptionSepArg, NULL},
	{(char *) "-shiftAnomaly", (char *) "*abacus.shiftAnomaly", XrmoptionSepArg, NULL},
	{(char *) "-anomalySq", (char *) "*abacus.anomalySq", XrmoptionSepArg, NULL},
	{(char *) "-shiftAnomalySq", (char *) "*abacus.shiftAnomalySq", XrmoptionSepArg, NULL},
	{(char *) "-displayBase", (char *) "*abacus.displayBase", XrmoptionSepArg, NULL},
	{(char *) "-pressOffset", (char *) "*abacus.pressOffset", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nopressOffset", (char *) "*abacus.pressOffset", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-romanNumerals", (char *) "*abacus.romanNumerals", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-noromanNumerals", (char *) "*abacus.romanNumerals", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-latin", (char *) "*abacus.latin", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nolatin", (char *) "*abacus.latin", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-ancientRoman", (char *) "*abacus.ancientRoman", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-noancientRoman", (char *) "*abacus.ancientRoman", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-modernRoman", (char *) "*abacus.modernRoman", XrmoptionNoArg, (char *) "TRUE"},
	{(char *) "-nomodernRoman", (char *) "*abacus.modernRoman", XrmoptionNoArg, (char *) "FALSE"},
	{(char *) "-chinese", (char *) "*abacus.format", XrmoptionNoArg, (char *) "chinese"},
	{(char *) "-japanese", (char *) "*abacus.format", XrmoptionNoArg, (char *) "japanese"},
	{(char *) "-korean", (char *) "*abacus.format", XrmoptionNoArg, (char *) "korean"},
	{(char *) "-roman", (char *) "*abacus.format", XrmoptionNoArg, (char *) "roman"},
	{(char *) "-russian", (char *) "*abacus.format", XrmoptionNoArg, (char *) "russian"},
	{(char *) "-danish", (char *) "*abacus.format", XrmoptionNoArg, (char *) "danish"},
	{(char *) "-medieval", (char *) "*abacus.format", XrmoptionNoArg, (char *) "medieval"},
	{(char *) "-generic", (char *) "*abacus.format", XrmoptionNoArg, (char *) "generic"},
	{(char *) "-it", (char *) "*abacus.museum", XrmoptionNoArg, (char *) "it"},
	{(char *) "-uk", (char *) "*abacus.museum", XrmoptionNoArg, (char *) "uk"},
	{(char *) "-fr", (char *) "*abacus.museum", XrmoptionNoArg, (char *) "fr"},
	{(char *) "-version", (char *) "*abacus.versionOnly", XrmoptionNoArg, (char *) "TRUE"}
};

static void
printState(Widget w, char *msg
#ifndef HAVE_MOTIF
, int mode
#endif
)
{
#ifdef HAVE_MOTIF
	XtVaSetValues(w,
		XmNvalue, msg, NULL);
/* label widget and draw the strings on it.
  use the (void) XmStringDrawImage(XtDisplay(topLevel),
			XtWindow(topLevel), fontlist, "Primary",
			gc, x, y, width,
			XmALIGNMENT_BEGINNING, XmSTRING_DIRECTION_L_TO_R, NULL); */
#else
	(void) sprintf(titleDsp, "%s  %s",
		msg,
		(mode < 0 || mode >= MAX_FORMATS) ? "Abacus" : formatChoices[mode]);
	XtVaSetValues(w, XtNtitle, titleDsp, NULL);
#endif
}

#ifdef LEE_ABACUS
#define AUXWIN(n) (n == LEFT_AUX) ? ((leftAuxAbacus) ? leftAuxAbacus : abacus) : ((n == RIGHT_AUX ) ? ((rightAuxAbacus) ? rightAuxAbacus : abacus) : abacus)
#else
#define AUXWIN(n) abacus
#endif

/* There's probably a better way to assure that they are the same but I do
 * not know it off hand. */
static void
initializeDemo(void)
{
	int mode;
	Boolean mono, reverse;
	Pixel demoForeground, demoBackground;
	String demoPath, demoFont;

	XtVaGetValues(abacus,
		XtNmono, &mono,
		XtNreverseVideo, &reverse,
		XtNmode, &mode,
		XtNdemoForeground, &demoForeground,
		XtNdemoBackground, &demoBackground,
		XtNdemoPath, &demoPath,
		XtNdemoFont, &demoFont, NULL);
	XtVaSetValues(abacusDemo,
		XtNmono, mono,
		XtNreverseVideo, reverse,
		XtNmode, mode,
		XtNdemoForeground, demoForeground,
		XtNdemoBackground, demoBackground,
		XtNdemoPath, demoPath,
		XtNdemoFont, demoFont,
		XtNframed, True, NULL);
}

#ifdef LEE_ABACUS
static void
initializeAux(void)
{
	Boolean mono, reverse, script;
	Pixel frameColor;

	XtVaGetValues(abacus,
		XtNmono, &mono,
		XtNreverseVideo, &reverse,
		XtNscript, &script,
		XtNframeColor, &frameColor, NULL);
	if (leftAuxAbacus) {
		XtVaSetValues(leftAuxAbacus,
			XtNmono, mono,
			XtNreverseVideo, reverse,
			XtNscript, script,
			XtNframeColor, frameColor, NULL);
		XtVaSetValues(abacus,
			XtNleftAuxAbacus, leftAuxAbacus, NULL);
	}
	if (rightAuxAbacus) {
		XtVaSetValues(rightAuxAbacus,
			XtNmono, mono,
			XtNreverseVideo, reverse,
			XtNscript, script,
			XtNframeColor, frameColor, NULL);
		XtVaSetValues(abacus,
			XtNrightAuxAbacus, rightAuxAbacus, NULL);
	}
}
#endif

static void
callbackAbacusDemo(Widget w, caddr_t clientData, abacusCallbackStruct *callData)
{
#ifndef HAVE_MOTIF
	int mode;

	XtVaGetValues(w,
		XtNmode, &mode, NULL);
#endif
	switch (callData->reason) {
	case ACTION_BASE_DEFAULT:
#ifdef HAVE_MOTIF
#ifdef USE_BASE_SPIN
		XtVaSetValues(abacusBaseChanger, XmNposition, DEFAULT_BASE, NULL);
#else
		XmScaleSetValue(abacusBaseChanger, DEFAULT_BASE);
#endif
#endif
		break;
	case ACTION_HIDE:
		(void) XIconifyWindow(XtDisplay(topLevel),
			XtWindow(topLevel),
			XScreenNumberOfScreen(XtScreen(topLevel)));
#ifndef HAVE_MOTIF
		(void) XIconifyWindow(XtDisplay(shell),
			XtWindow(shell),
			XScreenNumberOfScreen(XtScreen(shell)));
#endif
		break;
	case ACTION_MOVE:
#ifndef LEE_ABACUS
		if (callData->aux == PRIMARY)
#endif
		{
			XtVaSetValues(AUXWIN(callData->aux),
				XtNdeck, callData->deck,
				XtNrail, callData->rail,
				XtNnumber, callData->number, NULL);
		}
		break;
	case ACTION_HIGHLIGHT_RAIL:
#ifndef LEE_ABACUS
		if (callData->aux == PRIMARY)
#endif
		{
			XtVaSetValues(AUXWIN(callData->aux),
				XtNdeck, HIGHLIGHT_DECK,
				XtNrail, callData->rail, NULL);
		}
		break;
	case ACTION_UNHIGHLIGHT_RAIL:
#ifndef LEE_ABACUS
		if (callData->aux == PRIMARY)
#endif
		{
			XtVaSetValues(AUXWIN(callData->aux),
				XtNdeck, UNHIGHLIGHT_DECK,
				XtNrail, callData->rail, NULL);
		}
		break;
	case ACTION_HIGHLIGHT_RAILS:
#ifndef LEE_ABACUS
		if (callData->aux == PRIMARY)
#endif
		{
			XtVaSetValues(AUXWIN(callData->aux),
				XtNdeck, HIGHLIGHTS_DECK, NULL);
		}
		break;
	case ACTION_CLEAR_NODEMO:
	case ACTION_CLEAR:
		XtVaSetValues(abacus, XtNdeck, CLEAR_DECK, NULL);
#ifdef LEE_ABACUS
		/* Abacus demo clear is more complete */
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNdeck, CLEAR_DECK, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNdeck, CLEAR_DECK, NULL);
#endif
		break;
	case ACTION_DECIMAL_CLEAR:
		XtVaSetValues(abacus, XtNdeck, CLEAR_DECIMAL_DECK, NULL);
		break;
	case ACTION_DEMO:
		XtDestroyWidget(abacusDemo);
#ifdef HAVE_MOTIF
		/*XtVaSetValues(choiceMenu,
			XmNmenuHistory, choiceOptions[NORMAL], NULL);*/
#else
		/* Destroying shell is causing trouble, so lets not do that */
		/* Warning: XtRemoveGrab asked to remove a widget not on the list */
		/* http://www.mail-archive.com/lesstif@hungry.com/msg00535.html */
		/*XtDestroyWidget(shell);
		shell = NULL;*/
		XtUnrealizeWidget(shell);
#endif
		XtVaSetValues(abacus,
			XtNdemo, False, NULL);
#ifdef LEE_ABACUS
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNdemo, False, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNdemo, False, NULL);
#endif
		break;
	}
	if (callData->reason == ACTION_SCRIPT || callData->reason == ACTION_IGNORE) {
#ifdef HAVE_MOTIF
#ifdef LEE_ABACUS
		if (leftAuxTracker && leftAuxAbacus && w == leftAuxAbacus)
			printState(leftAuxTracker, callData->buffer);
		else if (rightAuxTracker && rightAuxAbacus && w == rightAuxAbacus)
			printState(rightAuxTracker, callData->buffer);
		else
#endif
			printState(tracker, callData->buffer);
#else
		printState(XtParent(w), callData->buffer, mode);
#endif
	}
}

static void
forceNormalParams(Widget w)
{
	int base = DEFAULT_BASE;
	int displayBase = DEFAULT_BASE;

#ifdef HAVE_MOTIF
	if (baseSet) {
#ifdef USE_BASE_SPIN
		XtVaSetValues(abacusBaseChanger, XmNposition, base, NULL);
		XtVaSetValues(displayBaseChanger, XmNposition, displayBase, NULL);
#else
		XmScaleSetValue(abacusBaseChanger, base);
		XmScaleSetValue(displayBaseChanger, displayBase);
#endif
	}
#endif
	XtVaSetValues(abacus,
		XtNbase, base,
		XtNdisplayBase, displayBase, NULL);
#ifdef LEE_ABACUS
	if (leftAuxAbacus)
		XtVaSetValues(leftAuxAbacus,
			XtNbase, base,
			XtNdisplayBase, displayBase, NULL);
	if (rightAuxAbacus)
		XtVaSetValues(rightAuxAbacus,
			XtNbase, base,
			XtNdisplayBase, displayBase, NULL);
#endif
}

static void
forceDemoParams(Widget w)
{
	int rails, min, mode;
	int bottomPiece, bottomPiecePercent, shiftPercent;
	Boolean demo, teach, sign;

	forceNormalParams(w);
	XtVaGetValues(abacus,
		XtNmode, &mode,
		XtNrails, &rails,
		XtNsign, &sign,
		XtNbottomPiece, &bottomPiece,
		XtNbottomPiecePercent, &bottomPiecePercent,
		XtNshiftPercent, &shiftPercent,
		XtNdemo, &demo,
		XtNteach, &teach, NULL);
	if (mode == GENERIC) {
		mode = CHINESE;
		XtVaSetValues(abacus,
			XtNmode, mode, NULL);
#ifdef HAVE_MOTIF
		XtVaSetValues(formatMenu,
			XmNmenuHistory, formatOptions[mode], NULL);
#endif
	}
	min = ((sign) ? 1: 0) + ((bottomPiece != 0) ? 1 : 0) +
		((bottomPiecePercent != 0) ? 1 + shiftPercent : 0) +
		((demo || teach) ? MIN_DEMO_RAILS : MIN_RAILS);
	if (rails < min) {
#ifdef HAVE_MOTIF
#ifdef USE_SPIN
		XtVaSetValues(sizeChanger, XmNposition, min, NULL);
#else
		XmScaleSetValue(sizeChanger, min);
#endif
#endif
		XtVaSetValues(abacus, XtNrails, min, NULL);
	}
}

#ifdef HAVE_MOTIF
static void
forceTeachAbacus(Widget abacus, int base)
{
	int anomaly, anomalySq, displayBase, mode;
	Boolean sign;

	XtVaGetValues(abacus,
		XtNsign, &sign,
		XtNanomaly, &anomaly,
		XtNanomalySq, &anomalySq,
		XtNmode, &mode,
		XtNdisplayBase, &displayBase, NULL);
	if (sign)
		XtVaSetValues(abacus,
			XtNsign, False, NULL);
	if (anomaly != 0)
		XtVaSetValues(abacus,
			XtNanomaly, 0, NULL);
	if (anomalySq != 0)
		XtVaSetValues(abacus,
			XtNanomalySq, 0, NULL);
	if (displayBase != base)
		XtVaSetValues(abacus,
			XtNdisplayBase, base, NULL);
	if (mode == GENERIC) {
		mode = CHINESE;
		XtVaSetValues(abacus,
			XtNmode, mode, NULL);
		XtVaSetValues(formatMenu,
			XmNmenuHistory, formatOptions[mode], NULL);
	}
}

static void
forceTeachParams(Widget w)
{
	int base;

	XtVaGetValues(abacus,
		XtNbase, &base, NULL);
	forceTeachAbacus(abacus, base);
	if (baseSet) {
#ifdef USE_BASE_SPIN
		XtVaSetValues(displayBaseChanger, XmNposition, base, NULL);
#else
		XmScaleSetValue(displayBaseChanger, base);
#endif
	}
#ifdef LEE_ABACUS
	if (leftAuxAbacus)
		forceTeachAbacus(leftAuxAbacus, base);
	if (rightAuxAbacus)
		forceTeachAbacus(rightAuxAbacus, base);
#endif
}
#endif

static void
initialize(Widget w)
{
	Boolean versionOnly;

	XtVaGetValues(w, XtNversionOnly, &versionOnly, NULL);
	if (versionOnly) {
		(void) printf("%s\n", aboutHelp);
		exit(0);
	}
#ifdef LEE_ABACUS
	initializeAux();
#endif
}

static void
createDemo(void)
{
#ifdef HAVE_MOTIF
	/* This is to work around bug in LessTif # 2801540 */
	/* Can not really use LESSTIF_VERSION as one may swap out lib */
	int height, subheight = 134, newHeight = 0;

	if (XtIsRealized(topLevel))
		XtVaGetValues(topLevel, XtNheight, &height, NULL);
	(void) sprintf(titleDspDemo, "%s-demo", progDsp);
	abacusDemo = XtCreateManagedWidget(titleDspDemo,
		abacusDemoWidgetClass, mainPanel, NULL, 0);
	if (XtIsRealized(topLevel)) {
		XtVaGetValues(abacusDemo, XtNheight, &newHeight, NULL);
		if (newHeight <= 1)
			XtVaSetValues(topLevel, XtNheight, height + subheight, NULL);
	}
	XtAddCallback(abacusDemo, XtNselectCallback,
		(XtCallbackProc) callbackAbacusDemo, (XtPointer) NULL);
#else
	if (shell == NULL)
		shell = XtCreateApplicationShell(titleDsp,
			topLevelShellWidgetClass, NULL, 0);
	(void) sprintf(titleDspDemo, "%s-demo", progDsp);
	XtVaSetValues(shell,
		XtNiconPixmap, pixmap,
		XtNinput, True,
		XtNtitle, titleDspDemo, NULL);
	abacusDemo = XtCreateManagedWidget("abacusDemo",
		abacusDemoWidgetClass, shell, NULL, 0);
	XtAddCallback(abacusDemo, XtNselectCallback,
		(XtCallbackProc) callbackAbacusDemo,
		(XtPointer) NULL);
#endif
	initializeDemo();
}

static void
realizeDemo(void)
{
#ifndef HAVE_MOTIF
	XtRealizeWidget(shell);
#endif
	VOID XGrabButton(XtDisplay(abacusDemo), (unsigned int) AnyButton,
		AnyModifier, XtWindow(abacusDemo), TRUE,
		(unsigned int) (ButtonPressMask |
		ButtonMotionMask | ButtonReleaseMask),
		GrabModeAsync, GrabModeAsync, XtWindow(abacusDemo),
		XCreateFontCursor(XtDisplay(abacusDemo), XC_hand2));
}

#ifdef HAVE_MOTIF
static void
abacusTeachListener(Widget w, caddr_t clientData, abacusCallbackStruct *callData)
{
	Boolean demo;
	char * str = XmTextGetString(w);

	XtVaGetValues(abacus,
		XtNdemo, &demo, NULL);
	if (demo)
		return;
	if (str != NULL && strlen((char *) str) > 0)
		XtVaSetValues(abacus,
			XtNdeck, TEACH_DECK,
			XtNteachBuffer, str, NULL);
	free(str);
}

static void
createTeach(void)
{
	int columns = 120, subheight = 100;
	/* This is to work around bug in LessTif # 2801540 */
	/* Can not really use LESSTIF_VERSION as one may swap out lib */
	int height = 1, newHeight = 1;

	if (XtIsRealized(topLevel))
		XtVaGetValues(topLevel, XtNheight, &height, NULL);
	teachRowCol = XtVaCreateManagedWidget("teachRowCol",
		xmRowColumnWidgetClass, mainPanel,
		XmNborderWidth, 1,
		XmNheight, subheight, NULL);
	if (XtIsRealized(topLevel)) {
		XtVaGetValues(teachRowCol, XtNheight, &newHeight, NULL);
		if (newHeight <= 1)
			XtVaSetValues(topLevel, XtNheight, height + subheight, NULL);
	}

	teachTracker = XtCreateManagedWidget("0",
		xmTextWidgetClass, teachRowCol, NULL, 0);
	teachLine[0] = XtVaCreateManagedWidget("teachText1",
		xmLabelGadgetClass, teachRowCol,
		XtVaTypedArg, XmNlabelString,
		XmRString, TEACH_STRING0, columns, NULL);
	teachLine[1] = XtVaCreateManagedWidget("teachText2",
		xmLabelGadgetClass, teachRowCol,
		XtVaTypedArg, XmNlabelString,
		XmRString, TEACH_STRING1, columns, NULL);
	teachLine[2] = XtVaCreateManagedWidget("teachText3",
		xmLabelGadgetClass, teachRowCol,
		XtVaTypedArg, XmNlabelString,
		XmRString, " ", columns, NULL);
	XtAddCallback(teachTracker, XmNactivateCallback,
		(XtCallbackProc) abacusTeachListener, (XtPointer) NULL);
}

static void
destroyTeach(void)
{
	XtDestroyWidget(teachTracker);
	XtDestroyWidget(teachLine[0]);
	XtDestroyWidget(teachLine[1]);
	XtDestroyWidget(teachLine[2]);
	XtDestroyWidget(teachRowCol);
	teachTracker = NULL;
}
#endif

extern Pixel darker(Widget w, Pixel pixel);

static void
callbackAbacus(Widget w, caddr_t clientData, abacusCallbackStruct *callData)
{
	int rails, decimalPosition, mode;
	Boolean slot, romanNumerals, ancientRoman, latin, modernRoman;
	Boolean group, sign, demo, teach;
	int bottomPiece, topPiece, bottomPiecePercent, topPiecePercent;
	int subdeck, subbase, museum, anomaly, anomalySq;
	Boolean rightToLeftAdd, rightToLeftMult;

	XtVaGetValues(w,
		XtNdemo, &demo,
		XtNteach, &teach,
		XtNmode, &mode, NULL);
	switch (callData->reason) {
	case ACTION_HIDE:
		(void) XIconifyWindow(XtDisplay(topLevel),
			XtWindow(topLevel),
			XScreenNumberOfScreen(XtScreen(topLevel)));
#ifndef HAVE_MOTIF
		if (demo)
			(void) XIconifyWindow(XtDisplay(shell),
				XtWindow(shell),
				XScreenNumberOfScreen(XtScreen(shell)));
#endif
		break;
	case ACTION_CLEAR_QUERY:
#ifdef HAVE_MOTIF
		XtManageChild(clearDialog);
#else
		XtVaSetValues(w, XtNmenu, ACTION_CLEAR, NULL);
#endif
		break;
	case ACTION_SCRIPT:
		if (callData->deck != 0 || callData->number != 0) {
			(void) printf("  <move>\n");
			(void) printf("    <code>%d %d %d %d 4</code>\n",
				callData->aux, callData->deck,
				callData->rail, callData->number);
			(void) printf("    <text>\n");
			(void) printf("      Lesson\n\n\n");
			(void) printf("      Press Space-bar to Continue\n");
			(void) printf("    </text>\n");
			(void) printf("  </move>\n");
		}
		break;
	case ACTION_MOVE:
#ifndef LEE_ABACUS
		if (callData->aux == PRIMARY)
#endif
		{
			XtVaSetValues(AUXWIN(callData->aux),
				XtNdeck, callData->deck,
				XtNrail, callData->rail,
				XtNnumber, callData->number, NULL);
		}
		break;
	case ACTION_CLEAR:
		if (callData->aux == PRIMARY)
			XtVaSetValues(abacusDemo, XtNdeck, CLEAR_DECK, NULL);
		else
			XtVaSetValues(AUXWIN(callData->aux),
				XtNdeck, CLEAR_DECK, NULL);
		break;
	case ACTION_DEMO:
		demo = !demo;
		if (demo) {
#ifdef HAVE_MOTIF
			if (baseSet) {
				forceNormalParams(abacus);
				baseSet = False;
			}
#endif
			forceDemoParams(abacus);
			createDemo();
			realizeDemo();
		} else {
			XtDestroyWidget(abacusDemo);
#ifndef HAVE_MOTIF
			/* Destroying shell is causing trouble, so lets not do that */
			/* Warning: XtRemoveGrab asked to remove a widget not on the list */
			/* http://www.mail-archive.com/lesstif@hungry.com/msg00535.html */
			/*XtDestroyWidget(shell);
			shell = NULL;*/
			XtUnrealizeWidget(shell);
#endif
		}
		XtVaSetValues(abacus, XtNdemo, demo, NULL);
#ifdef LEE_ABACUS
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNdemo, demo, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNdemo, demo, NULL);
#endif
#ifdef HAVE_MOTIF
		/*XtVaSetValues(choiceMenu,
			XmNmenuHistory, choiceOptions[(demo) ? DEMO : NORMAL], NULL);*/
#endif
		break;
#ifdef HAVE_MOTIF
	case ACTION_TEACH:
		if (teachTracker == NULL) {
			forceTeachParams(abacus);
			createTeach();
		} else {
			destroyTeach();
		}
		XtVaSetValues(abacus, XtNteach,
			(teachTracker == NULL) ? False : True, NULL);
		/*XtVaSetValues(choiceMenu,
			XmNmenuHistory, choiceOptions[(demo) ? TEACH : NORMAL], NULL);*/
		break;
	case ACTION_TEACH_LINE:
		if (callData->line >= 0 && callData->line <= 2) {
			/*static XmStringCharSet charSet =
				(XmStringCharSet) XmSTRING_DEFAULT_CHARSET;*/
			/*static XmStringCharSet charSet = "UTF-8";*/
			XmString teachBuffer = NULL;

			if (teachTracker == NULL)
				createTeach();
			/*teachBuffer = XmStringCreate(callData->teachBuffer, charSet);*/
			/*teachBuffer = XmStringCreateLtoR(callData->teachBuffer, charSet);*/
			teachBuffer = XmStringCreateLocalized(callData->teachBuffer);
			/*teachBuffer = XmStringGenerate(callData->teachBuffer, NULL, XmWIDECHAR_TEXT, NULL);*/
			XtVaSetValues(teachLine[callData->line],
				XmNlabelString, teachBuffer,
				NULL);
			XmStringFree(teachBuffer);
		}
		break;
#endif
	case ACTION_NEXT:
		XtVaSetValues(abacusDemo, XtNdeck, NEXT_DECK, NULL);
		break;
	case ACTION_REPEAT:
		XtVaSetValues(abacusDemo, XtNdeck, REPEAT_DECK, NULL);
		break;
	case ACTION_JUMP:
		XtVaSetValues(abacusDemo, XtNdeck, JUMP_DECK, NULL);
		break;
	case ACTION_MORE:
		XtVaSetValues(abacusDemo, XtNdeck, MORE_DECK, NULL);
		break;
	case ACTION_PLACE:
		XtVaGetValues(w,
			XtNslot, &slot,
			XtNdecimalPosition, &decimalPosition,
			XtNbottomPiece, &bottomPiece,
			XtNbottomPiecePercent, &bottomPiecePercent,
			XtNsubdeck, &subdeck,
			XtNsubbase, &subbase,
			XtNmuseum, &museum,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			XtSetSensitive(quarterPercentMenuItem,
				ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition));
			XtSetSensitive(secondaryRailsMenu,
				ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
			XtSetSensitive(subdeckMenuItem,
				ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(eighthMenuItem,
				ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
		}
#endif
		break;
	case ACTION_INCREMENT:
		if (w == abacus) {
			XtVaGetValues(w,
				XtNrails, &rails, NULL);
#ifdef HAVE_MOTIF
			if (rails > MAX_RAILS)
				XtVaSetValues(sizeChanger,
#ifdef USE_SPIN
					XmNmaximumValue, rails,
#else
					XmNmaximum, rails,
#endif
					NULL);
#ifdef USE_SPIN
			XtVaSetValues(sizeChanger, XmNposition, rails, NULL);
#else
			XmScaleSetValue(sizeChanger, rails);
#endif
#endif
		}
		break;
	case ACTION_DECREMENT:
		if (w == abacus) {
			XtVaGetValues(w,
				XtNrails, &rails, NULL);
#ifdef HAVE_MOTIF
#ifdef USE_SPIN
			XtVaSetValues(sizeChanger, XmNposition, rails, NULL);
#else
			XmScaleSetValue(sizeChanger, rails);
#endif
			if (rails >= MAX_RAILS)
				XtVaSetValues(sizeChanger,
#ifdef USE_SPIN
					XmNmaximumValue, rails,
#else
					XmNmaximum, rails,
#endif
					NULL);
#endif
		}
		break;
	case ACTION_FORMAT:
		if (w == abacus) {
#ifdef HAVE_MOTIF
			XtVaGetValues(w,
				XtNslot, &slot,
				XtNdecimalPosition, &decimalPosition,
				XtNbottomPiece, &bottomPiece,
				XtNbottomPiecePercent, &bottomPiecePercent,
				XtNsubdeck, &subdeck,
				XtNsubbase, &subbase,
				XtNmuseum, &museum,
				XtNanomaly, &anomaly,
				XtNanomalySq, &anomalySq,
				NULL);
			XtVaSetValues(formatMenu,
				XmNmenuHistory, formatOptions[mode],
				NULL);
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			if (demo) {
				XtVaSetValues(abacusDemo,
					XtNdeck, CLEAR_DECK,
					XtNmode, mode, NULL);
			}
			XtSetSensitive(secondaryRailsMenu,
				ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
			XtSetSensitive(subdeckMenuItem,
				ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(eighthMenuItem,
				ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
			XtSetSensitive(modernRomanMenuItem,
				ENABLE_MODERN_ROMAN(slot, anomaly, anomalySq));
#endif
		}
		break;
	case ACTION_ROMAN_NUMERALS:
		XtVaGetValues(w,
			XtNromanNumerals, &romanNumerals,
			XtNbottomPiece, &bottomPiece,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(romanNumeralsMenuItem,
				romanNumerals, False);
			XtSetSensitive(ancientRomanMenuItem,
				ENABLE_ANCIENT_ROMAN(romanNumerals));
			XtSetSensitive(latinMenuItem,
				ENABLE_LATIN(romanNumerals, bottomPiece));
		}
#endif
		break;
	case ACTION_ANCIENT_ROMAN:
		XtVaGetValues(w,
			XtNancientRoman, &ancientRoman,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(ancientRomanMenuItem,
				ancientRoman, False);
		}
#endif
		break;
	case ACTION_LATIN:
		XtVaGetValues(w,
			XtNlatin, &latin,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(latinMenuItem,
				latin, False);
		}
#endif
		break;
	case ACTION_GROUP:
		XtVaGetValues(w,
			XtNgroup, &group,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(groupMenuItem,
				group, False);
		}
#endif
		break;
	case ACTION_SIGN:
		XtVaGetValues(w,
			XtNsign, &sign, NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(signMenuItem,
				sign, False);
		}
#endif
		break;
	case ACTION_QUARTER:
		XtVaGetValues(w,
			XtNromanNumerals, &romanNumerals,
			XtNdecimalPosition, &decimalPosition,
			XtNslot, &slot,
			XtNbottomPiece, &bottomPiece,
			XtNtopPiece, &topPiece,
			XtNbottomPiecePercent, &bottomPiecePercent,
			XtNtopPiecePercent, &topPiecePercent,
			XtNsubdeck, &subdeck,
			XtNsubbase, &subbase,
			XtNmuseum, &museum,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(quarterMenuItem,
				SET_QUARTER(bottomPiece, topPiece), False);
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			XtSetSensitive(latinMenuItem,
				ENABLE_LATIN(romanNumerals, bottomPiece));
			XtSetSensitive(twelfthMenuItem,
				ENABLE_TWELFTH(bottomPiece));
			XtSetSensitive(secondaryRailsMenu,
				ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
			XtSetSensitive(quarterPercentMenuItem,
				ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition));
			XtSetSensitive(subdeckMenuItem,
				ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(eighthMenuItem,
				ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
		}
#endif
		break;
	case ACTION_TWELFTH:
		XtVaGetValues(w,
			XtNromanNumerals, &romanNumerals,
			XtNdecimalPosition, &decimalPosition,
			XtNslot, &slot,
			XtNbottomPiece, &bottomPiece,
			XtNtopPiece, &topPiece,
			XtNbottomPiecePercent, &bottomPiecePercent,
			XtNsubdeck, &subdeck,
			XtNsubbase, &subbase,
			XtNmuseum, &museum,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(twelfthMenuItem,
				SET_TWELFTH(bottomPiece, topPiece), False);
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			XtSetSensitive(latinMenuItem,
				ENABLE_LATIN(romanNumerals, bottomPiece));
			XtSetSensitive(quarterMenuItem,
				ENABLE_QUARTER(bottomPiece));
			XtSetSensitive(quarterPercentMenuItem,
				ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition));
			XtSetSensitive(secondaryRailsMenu,
				ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
			XtSetSensitive(subdeckMenuItem,
				ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(eighthMenuItem,
				ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
		}
#endif
		break;
	case ACTION_QUARTER_PERCENT:
		XtVaGetValues(w,
			XtNdecimalPosition, &decimalPosition,
			XtNbottomPiece, &bottomPiece,
			XtNbottomPiecePercent, &bottomPiecePercent,
			XtNtopPiecePercent, &topPiecePercent,
			XtNsubdeck, &subdeck,
			XtNsubbase, &subbase,
			XtNmuseum, &museum,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(quarterPercentMenuItem,
				SET_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, topPiecePercent), False);
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			XtSetSensitive(secondaryRailsMenu,
				ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
			XtSetSensitive(subdeckMenuItem,
				ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(eighthMenuItem,
				ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
		}
#endif
		break;
	case ACTION_SUBDECK:
		XtVaGetValues(w,
			XtNslot, &slot,
			XtNdecimalPosition, &decimalPosition,
			XtNbottomPiece, &bottomPiece,
			XtNbottomPiecePercent, &bottomPiecePercent,
			XtNsubdeck, &subdeck,
			XtNsubbase, &subbase,
			XtNmuseum, &museum,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			XtSetSensitive(quarterPercentMenuItem,
				ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition));
			XtSetSensitive(eighthMenuItem,
				ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
		}
#endif
		break;
	case ACTION_EIGHTH:
		XtVaGetValues(w,
			XtNslot, &slot,
			XtNdecimalPosition, &decimalPosition,
			XtNbottomPiece, &bottomPiece,
			XtNbottomPiecePercent, &bottomPiecePercent,
			XtNsubdeck, &subdeck,
			XtNsubbase, &subbase,
			XtNmuseum, &museum,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
			XtSetSensitive(quarterPercentMenuItem,
				ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition));
			XtSetSensitive(subdeckMenuItem,
				ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
			XtSetSensitive(museumMenu,
				ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
		}
#endif
		break;
	case ACTION_MUSEUM:
		XtVaGetValues(w,
			XtNmuseum, &museum,
			NULL);
		break;
	case ACTION_MODERN_ROMAN:
		XtVaGetValues(w,
			XtNmodernRoman, &modernRoman,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(modernRomanMenuItem,
				modernRoman, False);
		}
#endif
		break;
	case ACTION_ANOMALY:
		XtVaGetValues(w,
			XtNslot, &slot,
			XtNanomaly, &anomaly,
			XtNanomalySq, &anomalySq,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(anomalyMenuItem,
				SET_ANOMALY(anomaly, anomalySq), False);
			XtSetSensitive(modernRomanMenuItem,
				ENABLE_MODERN_ROMAN(slot, anomaly, anomalySq));
			XtSetSensitive(watchMenuItem,
				ENABLE_WATCH(anomaly, anomalySq));
		}
#endif
		break;
	case ACTION_WATCH:
		XtVaGetValues(w,
			XtNmodernRoman, &modernRoman,
			XtNanomaly, &anomaly,
			XtNanomalySq, &anomalySq,
			NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(watchMenuItem,
				SET_WATCH(anomaly, anomalySq), False);
			XtSetSensitive(modernRomanMenuItem,
				ENABLE_MODERN_ROMAN(slot, anomaly, anomalySq));
			XtSetSensitive(anomalyMenuItem,
				ENABLE_ANOMALY(anomaly, anomalySq));
		}
#endif
		break;
#if 0
	case ACTION_VERTICAL:
		XtVaGetValues(w,
			XtNvertical, &vertical, NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(verticalMenuItem,
				vertical, False);
		}
#endif
		break;
#endif
	case ACTION_RIGHT_TO_LEFT_ADD :
		XtVaGetValues(w,
			XtNrightToLeftAdd, &rightToLeftAdd, NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(rightToLeftAddMenuItem,
				rightToLeftAdd, False);
		}
#endif
		break;
	case ACTION_RIGHT_TO_LEFT_MULT:
		XtVaGetValues(w,
			XtNrightToLeftMult, &rightToLeftMult, NULL);
#ifdef HAVE_MOTIF
		if (w == abacus) {
			XmToggleButtonSetState(rightToLeftMultMenuItem,
				rightToLeftMult, False);
		}
#endif
		break;
	}

	if (/*callData->reason == ACTION_SCRIPT || */callData->reason == ACTION_IGNORE) {
#ifdef HAVE_MOTIF
#ifdef LEE_ABACUS
		int initialValue = False;

		if (strcmp(callData->buffer, "0.0") == 0 ||
				strcmp(callData->buffer, "0") == 0) {
			initialValue = True;
		}	
		if (leftAuxTracker && leftAuxAbacus && w == leftAuxAbacus) {
			static Pixel leftAuxOrigColor;
			static Pixel leftAuxBeadColor;
			static int leftAuxFirstTime = True;
			static int leftAuxInitialColor = True;

			if (leftAuxFirstTime) {
				XtVaGetValues(w,
					XtNforeground, &leftAuxOrigColor,
					XtNleftAuxBeadColor, &leftAuxBeadColor, NULL);
				leftAuxBeadColor = darker(leftAuxAbacus, darker(leftAuxAbacus, leftAuxBeadColor));
				leftAuxFirstTime = False;
			}
			if (initialValue && leftAuxInitialColor) {
				XtVaSetValues(leftAuxTracker, XtNforeground, leftAuxBeadColor, NULL);
				printState(leftAuxTracker, "Left Auxiliary");
			} else {
				XtVaSetValues(leftAuxTracker, XtNforeground, leftAuxOrigColor, NULL);
				printState(leftAuxTracker, callData->buffer);
				leftAuxInitialColor = False;
			}
		} else if (rightAuxTracker && rightAuxAbacus && w == rightAuxAbacus) {
			static Pixel rightAuxOrigColor;
			static Pixel rightAuxBeadColor;
			static int rightAuxFirstTime = True;
			static int rightAuxInitialColor = True;

			if (rightAuxFirstTime) {
				XtVaGetValues(w,
					XtNforeground, &rightAuxOrigColor,
					XtNrightAuxBeadColor, &rightAuxBeadColor, NULL);
				rightAuxBeadColor = darker(rightAuxAbacus, darker(rightAuxAbacus, rightAuxBeadColor));
				rightAuxFirstTime = False;
			}
			if (initialValue && rightAuxInitialColor) {
				XtVaSetValues(rightAuxTracker, XtNforeground, rightAuxBeadColor, NULL);
				printState(rightAuxTracker, "Right Auxiliary");
			} else {
				XtVaSetValues(rightAuxTracker, XtNforeground, rightAuxOrigColor, NULL);
				printState(rightAuxTracker, callData->buffer);
				rightAuxInitialColor = False;
			}
		} else if (rightAuxAbacus) {
			/* just have to check one widget to make sure its lee */
			static Pixel primaryOrigColor;
			static Pixel primaryBeadColor;
			static int primaryFirstTime = True;
			static int primaryInitialColor = True;

			if (primaryFirstTime) {
				XtVaGetValues(w,
					XtNforeground, &primaryOrigColor,
					XtNprimaryBeadColor, &primaryBeadColor, NULL);
				primaryBeadColor = darker(abacus, darker(abacus, primaryBeadColor));
				primaryFirstTime = False;
			}
			if (initialValue && primaryInitialColor) {
				XtVaSetValues(tracker, XtNforeground, primaryBeadColor, NULL);
				printState(tracker, "Primary");
			} else {
				XtVaSetValues(tracker, XtNforeground, primaryOrigColor, NULL);
				printState(tracker, callData->buffer);
				primaryInitialColor = False;
			}
		} else {
			printState(tracker, callData->buffer);
		}
#else
		printState(tracker, callData->buffer);
#endif
#else
		printState(XtParent(w), callData->buffer, mode);
#endif
	}
}

#ifdef HAVE_MOTIF
static void
abacusMathListener(Widget w, caddr_t clientData, abacusCallbackStruct *callData)
{
	Boolean demo, teach;
	unsigned int i, j = 0;
	int aux;

	XtVaGetValues(abacus,
		XtNdemo, &demo,
		XtNteach, &teach, NULL);
	if (demo || teach)
		return;
#ifdef LEE_ABACUS
	if (leftAuxTracker && leftAuxAbacus && w == leftAuxTracker) {
		aux = LEFT_AUX;
	} else if (rightAuxTracker && rightAuxAbacus &&
			w == rightAuxTracker) {
		aux = RIGHT_AUX;
	} else
#endif
	{
		aux = PRIMARY;
	}
	if (mathBuffer)
		free(mathBuffer);
	mathBuffer = XmTextGetString(w);
	/* strip out blanks */
	for (i = 0; i < strlen(mathBuffer); i++) {
		if (mathBuffer[i] == '[' || mathBuffer[i] == ']') {
			mathBuffer[j] = '\0';
			break;
		} else if (mathBuffer[i] != ' ' &&
				mathBuffer[i] != '\t') {
			mathBuffer[j] = mathBuffer[i];
			j++;
		}
	}
	/* check for math ops */
	XtVaSetValues(abacus,
		XtNaux, aux,
		XtNdeck, CALC_DECK,
		XtNmathBuffer, mathBuffer, NULL);
}

static void
abacusClearListener(Widget w, XtPointer clientData, XmAnyCallbackStruct *cbs)
{
	if (cbs->reason == XmCR_OK) {
		XtVaSetValues(abacus, XtNmenu, ACTION_CLEAR, NULL);
	}
}

static void
railChangeListener(Widget w, XtPointer clientData,
#ifdef USE_SPIN
		XmSpinBoxCallbackStruct *cbs
#else
		XmScaleCallbackStruct *cbs
#endif
		)
{
	int rails, old, min;
	int bottomPiece, bottomPiecePercent, shiftPercent;
	Boolean sign, demo;

#ifdef USE_SPIN
	rails = cbs->position;
#else
	rails = cbs->value;
#endif
	XtVaGetValues(abacus,
		XtNrails, &old,
		XtNsign, &sign,
		XtNbottomPiece, &bottomPiece,
		XtNbottomPiecePercent, &bottomPiecePercent,
		XtNshiftPercent, &shiftPercent,
		XtNdemo, &demo, NULL);
	min = ((sign) ? 1 : 0) + ((bottomPiece == 0) ? 0 : 1) +
		((bottomPiecePercent == 0) ? 0 : 1 + shiftPercent) +
		((demo) ? MIN_DEMO_RAILS : MIN_RAILS);
	if (rails < min) {
#ifdef USE_SPIN
		XtVaSetValues(sizeChanger, XmNposition, old, NULL);
#else
		XmScaleSetValue(sizeChanger, old);
#endif
	} else if (old != rails)
		XtVaSetValues(abacus,
			XtNrails, rails, NULL);
}

static void
abacusBaseChangeListener(Widget w, XtPointer clientData,
#ifdef USE_BASE_SPIN
		XmSpinBoxCallbackStruct *cbs
#else
		XmScaleCallbackStruct *cbs
#endif
		)
{
	int base, old, bottomSpaces, mode;
	int bottomPiece, bottomPiecePercent;
	Boolean demo, teach;

#ifdef USE_BASE_SPIN
	base = cbs->position;
#else
	base = cbs->value;
#endif
	XtVaGetValues(abacus,
		XtNbase, &old,
		XtNbottomPiece, &bottomPiece,
		XtNbottomPiecePercent, &bottomPiecePercent,
		XtNbottomSpaces, &bottomSpaces,
		XtNmode, &mode,
		XtNdemo, &demo,
		XtNteach, &teach, NULL);
	if (demo) {
		base = DEFAULT_BASE;
#ifdef USE_BASE_SPIN
		XtVaSetValues(abacusBaseChanger, XmNposition, base, NULL);
#else
		XmScaleSetValue(abacusBaseChanger, base);
#endif
	/* Odd bases produce round-off errors but OK */
	/* When base divisible by 4, kind of silly but OK */
	/* Well some of these have enough room in Russian but not others */
	} else if ((base == 2 || base == 4) && bottomSpaces < 3) {
		XtVaSetValues(abacus,
			XtNbottomSpaces, 3,
			XtNbase, base, NULL);
	} else if ((base == 3 || base == 6 || base == 9) && bottomSpaces < 2) {
		XtVaSetValues(abacus,
			XtNbottomSpaces, 2,
			XtNbase, base, NULL);
	} else {
		XtVaSetValues(abacus,
			XtNbase, base, NULL);
	}
	if (demo || teach) {
#ifdef USE_BASE_SPIN
		XtVaSetValues(displayBaseChanger, XmNposition, base, NULL);
#else
		XmScaleSetValue(displayBaseChanger, base);
#endif
		XtVaSetValues(abacus,
			XtNdisplayBase, base, NULL);
#ifdef LEE_ABACUS
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNbase, base,
				XtNdisplayBase, base, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNbase, base,
				XtNdisplayBase, base, NULL);
#endif
#ifdef LEE_ABACUS
	} else {
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNbase, base, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNbase, base, NULL);
#endif
	}
}

static void
displayBaseChangeListener(Widget w, XtPointer clientData,
#ifdef USE_BASE_SPIN
		XmSpinBoxCallbackStruct *cbs
#else
		XmScaleCallbackStruct *cbs
#endif
		)
{
	int displayBase, old, base;
	Boolean demo, teach;

#ifdef USE_BASE_SPIN
	displayBase = cbs->position;
#else
	displayBase = cbs->value;
#endif
	XtVaGetValues(abacus,
		XtNbase, &base,
		XtNdisplayBase, &old,
		XtNdemo, &demo,
		XtNteach, &teach, NULL);
	if (demo)
		base = DEFAULT_BASE;
	if (demo || teach) {
#ifdef USE_BASE_SPIN
		XtVaSetValues(displayBaseChanger, XmNposition, base, NULL);
#else
		XmScaleSetValue(displayBaseChanger, base);
#endif
#ifdef LEE_ABACUS
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNbase, base,
				XtNdisplayBase, displayBase, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNbase, base,
				XtNdisplayBase, displayBase, NULL);
#endif
	} else {
		XtVaSetValues(abacus,
			XtNdisplayBase, displayBase, NULL);
#ifdef LEE_ABACUS
		if (leftAuxAbacus)
			XtVaSetValues(leftAuxAbacus,
				XtNdisplayBase, displayBase, NULL);
		if (rightAuxAbacus)
			XtVaSetValues(rightAuxAbacus,
				XtNdisplayBase, displayBase, NULL);
#endif
	}
}

static void
delayChangeListener(Widget w, XtPointer clientData,
#ifdef USE_DELAY_SPIN
		XmSpinBoxCallbackStruct *cbs
#else
		XmScaleCallbackStruct *cbs
#endif
		)
{
	int delay, old; /* min? */

#ifdef USE_DELAY_SPIN
	delay = cbs->position;
#else
	delay = cbs->value;
#endif
	XtVaGetValues(abacus,
		XtNdelay, &old, NULL);
	if (old != delay)
		XtVaSetValues(abacus,
			XtNdelay, delay, NULL);
}

#if 0
static void
verticalToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNvertical, value, NULL);
}
#endif

static void
romanNumeralsToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNancientRoman, False,
		XtNromanNumerals, value, NULL);
}

static void
ancientRomanToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNancientRoman, value, NULL);
}

static void
latinToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNlatin, value, NULL);
}

static void
groupToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNgroup, value, NULL);
}

static void
signToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNsign, value, NULL);
}

static void
quarterToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNtopPiece, 0,
		XtNbottomPiece, (value) ? 4 : 0, NULL);
}

static void
quarterPercentToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNtopPiecePercent, 0,
		XtNbottomPiecePercent, (value) ? 4 : 0, NULL);
}

static void
twelfthToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNtopPiece, 2,
		XtNbottomPiece, (value) ? 6 : 0, NULL);
}

static void
subdeckToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNsubbase, 12,
		XtNsubdeck, (value) ? DEFAULT_SUBDECKS : 0,
		XtNsubbead, DEFAULT_SUBBEADS, NULL);
}

static void
eighthToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNsubbase, 8,
		XtNsubdeck, (value) ? DEFAULT_SUBDECKS : 0,
		XtNsubbead, DEFAULT_SUBBEADS, NULL);
}

static void
museumChoice(Widget w, XtPointer clientData, XtPointer callData)
{
	int value = (int) ((size_t) value);

	XtVaSetValues(abacus,
		XtNmuseum, museumChoices[value], NULL);
}

static void
modernRomanToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNmodernRoman, value, NULL);
}

static void
anomalyToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNanomaly, (value) ? 2 : 0,
		XtNanomalySq, 0, NULL);
}

static void
watchToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNanomaly, (value) ? 4 : 0,
		XtNanomalySq, (value) ? 4 : 0, NULL);
}

static void
rightToLeftAddToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNrightToLeftAdd, value, NULL);
}

static void
rightToLeftMultToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNrightToLeftMult, value, NULL);
}

static void
soundToggle(Widget w, XtPointer clientData,
		XmToggleButtonCallbackStruct *cbs)
{
	Boolean value = cbs->set;

	XtVaSetValues(abacus,
		XtNsound, value, NULL);
}

static void
formatListener(Widget w, XtPointer value, XtPointer clientData)
{
	Boolean demo, slot;
	int mode = (int) ((size_t) value), oldMode;
	int decimalPosition, bottomPiece, bottomPiecePercent;
	int subdeck, subbase, museum, anomaly, anomalySq;

	XtVaGetValues(abacus,
		XtNdecimalPosition, &decimalPosition,
		XtNbottomPiece, &bottomPiece,
		XtNbottomPiecePercent, &bottomPiecePercent,
		XtNsubdeck, &subdeck,
		XtNsubbase, &subbase,
		XtNmuseum, &museum,
		XtNslot, &slot,
		XtNanomaly, &anomaly,
		XtNanomalySq, &anomalySq,
		XtNmode, &oldMode,
		XtNdemo, &demo, NULL);
	if (mode < 0 || mode > MAX_FORMATS)
		mode = GENERIC;
	if (demo && mode == GENERIC) {
		XtVaSetValues(formatMenu,
			XmNmenuHistory, formatOptions[oldMode], NULL);
		return;
	}
	if (oldMode == ROMAN && mode != ROMAN && subdeck) {
		XtVaSetValues(abacus,
			XtNdeck, CLEAR_DECIMAL_DECK,
			NULL);
	}
	if (mode != GENERIC)
		slot = (mode == ROMAN); /* since just changed */
	XtVaSetValues(abacus, XtNmode, mode, NULL);
	XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
	if (demo) {
		XtVaSetValues(abacusDemo,
			XtNdeck, CLEAR_DECK,
			XtNmode, mode,
			NULL);
	}
	XtSetSensitive(secondaryRailsMenu,
		ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
	XtSetSensitive(subdeckMenuItem,
		ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
	XtSetSensitive(eighthMenuItem,
		ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
	XtSetSensitive(modernRomanMenuItem,
		ENABLE_MODERN_ROMAN(slot, anomaly, anomalySq));
	XtSetSensitive(museumMenu,
		ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
}

static void
fileMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	if (val == 0)
		exit(0);
}

static Widget
createBaseChangers(Widget w, char *title)
{
	Widget button, dialog, pane;
	Widget changerRowCol;
	Widget changerDisplayRowCol;
	Widget changerAbacusRowCol;
	int base, displayBase;
	char titleDsp[FILE_NAME_LENGTH + 8];
	XmString titleString = NULL;

	(void) sprintf(titleDsp, "%s: %s\n", progDsp, title);
	titleString = XmStringCreateSimple((char *) titleDsp);
	XtSetArg(args[0], XmNdialogTitle, titleString);
	dialog = XmCreateMessageDialog(w, title, args, 1);
	XmStringFree(titleString);
	pane = XtVaCreateWidget("pane", xmPanedWindowWidgetClass, dialog,
		XmNsashWidth, 1,
		XmNsashHeight, 1,
		NULL);
	changerRowCol = XtVaCreateManagedWidget("changerRowCol",
		xmRowColumnWidgetClass, pane,
		XmNnumColumns, 2,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		NULL);
	changerDisplayRowCol = XtVaCreateManagedWidget("changerDisplayRowCol",
		xmRowColumnWidgetClass, changerRowCol,
		XmNnumColumns, 2,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		NULL);
	XtVaGetValues(abacus,
		XtNdisplayBase, &displayBase,
		XtNbase, &base,
		NULL);
	XtManageChild(XmCreateLabel(changerDisplayRowCol, "Display Base:", NULL, 0));
	displayBaseChanger = XtVaCreateManagedWidget("displayBase",
#ifdef USE_BASE_SPIN
		xmSimpleSpinBoxWidgetClass, changerDisplayRowCol,
		XmNspinBoxChildType, XmNUMERIC,
		XmNminimumValue, MIN_BASE,
		XmNmaximumValue, MAX_BASE,
		XmNposition, displayBase,
		XmNincrementValue, 1,
		XmNcolumns, 2,
#else
		xmScaleWidgetClass, changerDisplayRowCol,
		XmNminimum, MIN_BASE,
		XmNmaximum, MAX_BASE,
		XmNvalue, displayBase,
		XmNscaleWidth, (MAX_BASE - MIN_BASE + 1) * 4,
#endif
		/*XtVaTypedArg, XmNtitleString, XmRString, "      Display Base", 18,*/
		XmNshowValue, True,
		XmNorientation, XmHORIZONTAL,
		NULL);
	changerAbacusRowCol = XtVaCreateManagedWidget("changerAbacusRowCol",
		xmRowColumnWidgetClass, changerRowCol,
		XmNnumColumns, 2,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		NULL);
	XtManageChild(XmCreateLabel(changerAbacusRowCol, " Abacus Base:", NULL, 0));
	XtAddCallback(displayBaseChanger, XmNvalueChangedCallback,
		(XtCallbackProc) displayBaseChangeListener, (XtPointer) NULL);
	abacusBaseChanger = XtVaCreateManagedWidget("abacusBase",
#ifdef USE_BASE_SPIN
		xmSimpleSpinBoxWidgetClass, changerAbacusRowCol,
		XmNspinBoxChildType, XmNUMERIC,
		XmNminimumValue, MIN_BASE,
		XmNmaximumValue, MAX_BASE,
		XmNposition, base,
		XmNincrementValue, 1,
		XmNcolumns, 2,
#else
		xmScaleWidgetClass, changerAbacusRowCol,
		XmNminimum, MIN_BASE,
		XmNmaximum, MAX_BASE,
		XmNvalue, base,
		XmNscaleWidth, (MAX_BASE - MIN_BASE + 1) * 4,
#endif
		/*XtVaTypedArg, XmNtitleString, XmRString, "      Abacus Base", 18,*/
		XmNshowValue, True,
		XmNorientation, XmHORIZONTAL,
		NULL);
	XtAddCallback(abacusBaseChanger, XmNvalueChangedCallback,
		(XtCallbackProc) abacusBaseChangeListener, (XtPointer) NULL);
	XtManageChild(pane);
	button = XmMessageBoxGetChild(dialog, XmDIALOG_CANCEL_BUTTON);
	XtUnmanageChild(button);
	button = XmMessageBoxGetChild(dialog, XmDIALOG_HELP_BUTTON);
	XtUnmanageChild(button);
	return dialog;
}

static void
controlMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	if (val != 1)
		return;
	if (baseDialog != NULL)
		XtManageChild(baseDialog);
}

static void
beadControlMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus, XtNmenu, val + ACTION_CLEAR, NULL);
}

#ifdef EXTRA
static void
basicFormatMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus, XtNmenu, val + ACTION_FORMAT, NULL);
}
#endif

static void
displayFormatMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus, XtNmenu, val + ACTION_ROMAN_NUMERALS, NULL);
}

static void
specialRailsMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus, XtNmenu, val + ACTION_SIGN, NULL);
}

static void
secondaryRailsMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus, XtNmenu, val + ACTION_SUBDECK, NULL);
}

static void
museumMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus,
		XtNmuseum, museumChoices[val], NULL);
}

static void
teachOptionsMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	XtVaSetValues(abacus, XtNmenu, val + ACTION_RIGHT_TO_LEFT_ADD, NULL);
}

static Widget
createQuery(Widget w, char *text, char *title, XtCallbackProc callback)
{
	Widget button, messageBox;
	char titleDsp[FILE_NAME_LENGTH + 8];
	XmString titleString = NULL, messageString = NULL;
	static XmStringCharSet charSet =
		(XmStringCharSet) XmSTRING_DEFAULT_CHARSET;

	messageString = XmStringCreateLtoR(text, charSet);
	(void) sprintf(titleDsp, "%s: %s", progDsp, title);
	titleString = XmStringCreateSimple((char *) titleDsp);
	XtSetArg(args[0], XmNdialogTitle, titleString);
	XtSetArg(args[1], XmNmessageString, messageString);
	messageBox = XmCreateWarningDialog(w, (char *) "queryBox",
		args, 2);
	button = XmMessageBoxGetChild(messageBox, XmDIALOG_HELP_BUTTON);
	XtUnmanageChild(button);
	XmStringFree(titleString);
	XmStringFree(messageString);
	XtAddCallback(messageBox, XmNokCallback,
		(XtCallbackProc) abacusClearListener, (XtPointer) NULL);
	XtAddCallback(messageBox, XmNcancelCallback,
		(XtCallbackProc) abacusClearListener, (XtPointer) NULL);
	return messageBox;
}

static Widget
createDelayChanger(Widget w, char *title)
{
	Widget dialog, pane, button;
	Widget changerRowCol;
	int delay;
	char titleDsp[FILE_NAME_LENGTH + 8];
	XmString titleString = NULL;

	(void) sprintf(titleDsp, "%s: %s\n", progDsp, title);
	titleString = XmStringCreateSimple((char *) titleDsp);
	dialog = XmCreateMessageDialog(w, title, NULL, 0);
	XmStringFree(titleString);
	pane = XtVaCreateWidget("pane", xmPanedWindowWidgetClass, dialog,
		XmNsashWidth, 1,
		XmNsashHeight, 1,
		NULL);
	changerRowCol = XtVaCreateManagedWidget("changerRowCol",
		xmRowColumnWidgetClass, pane,
		XmNnumColumns, 1,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		NULL);
	XtVaGetValues(abacus,
		XtNdelay, &delay, NULL);
	XtManageChild(XmCreateLabel(changerRowCol, "msec Delay:", NULL, 0));
	delayChanger = XtVaCreateManagedWidget("delayChanger",
#ifdef USE_DELAY_SPIN
		xmSimpleSpinBoxWidgetClass, changerRowCol,
		XmNspinBoxChildType, XmNUMERIC,
		XmNminimumValue, 0,
		XmNmaximumValue, 500,
		XmNposition, delay,
		XmNincrementValue, 1,
		XmNcolumns, 3,
#else
		xmScaleWidgetClass, changerRowCol,
		XmNminimum, 0,
		XmNmaximum, 500,
		XmNvalue, delay,
		XmNscaleWidth, 128,
#endif
		/*XtVaTypedArg, XmNtitleString, XmRString, "msec Delay", 11,*/
		XmNshowValue, True,
		XmNorientation, XmHORIZONTAL,
		NULL);
	XtAddCallback(delayChanger, XmNvalueChangedCallback,
		(XtCallbackProc) delayChangeListener, (XtPointer) NULL);
	XtManageChild(pane);
	button = XmMessageBoxGetChild(dialog, XmDIALOG_CANCEL_BUTTON);
	XtUnmanageChild(button);
	button = XmMessageBoxGetChild(dialog, XmDIALOG_HELP_BUTTON);
	XtUnmanageChild(button);
	return dialog;
}

static void
effectsMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	if (val == 0) {
		XtVaSetValues(abacus, XtNmenu, val + ACTION_SOUND, NULL);
	} else if (val == 1) {
		if (delayDialog != NULL)
			XtManageChild(delayDialog);
	}
}

static void
learnMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	Boolean wasDemo, wasTeach;
	int val = (int) ((size_t) value);

	XtVaGetValues(abacus,
		XtNdemo, &wasDemo, NULL);
	XtVaGetValues(abacus,
		XtNteach, &wasTeach, NULL);
	if (wasDemo) {
		XtVaSetValues(abacus, XtNdemo, !wasDemo, NULL);
		XtDestroyWidget(abacusDemo);
		return;
	}
	if (wasTeach) {
		XtVaSetValues(abacus, XtNteach, !wasTeach, NULL);
		destroyTeach();
		return;
	}
	if (val == 0) {
		if (baseSet) {
			forceNormalParams(abacus);
			baseSet = False;
		}
		forceDemoParams(abacus);
		XtVaSetValues(abacus, XtNdemo, !wasDemo, NULL);
		createDemo();
		realizeDemo();
	} else if (val == 1) {
		forceTeachParams(abacus);
		XtVaSetValues(abacus, XtNteach, !wasTeach, NULL);
		createTeach();
	}
}

static Widget
createHelp(Widget w, char *text, char *title)
{
	Widget button, messageBox;
	char titleDsp[FILE_NAME_LENGTH + 8];
	XmString titleString = NULL, messageString = NULL, buttonString = NULL;
	static XmStringCharSet charSet =
		(XmStringCharSet) XmSTRING_DEFAULT_CHARSET;

	messageString = XmStringCreateLtoR(text, charSet);
	(void) sprintf(titleDsp, "%s: %s", progDsp, title);
	titleString = XmStringCreateSimple((char *) titleDsp);
	buttonString = XmStringCreateSimple((char *) "OK");
	XtSetArg(args[0], XmNdialogTitle, titleString);
	XtSetArg(args[1], XmNokLabelString, buttonString);
	XtSetArg(args[2], XmNmessageString, messageString);
	messageBox = XmCreateInformationDialog(w, (char *) "helpBox",
		args, 3);
	button = XmMessageBoxGetChild(messageBox, XmDIALOG_CANCEL_BUTTON);
	XtUnmanageChild(button);
	button = XmMessageBoxGetChild(messageBox, XmDIALOG_HELP_BUTTON);
	XtUnmanageChild(button);
	XmStringFree(titleString);
	XmStringFree(buttonString);
	XmStringFree(messageString);
	return messageBox;
}

/*http://www.ist.co.uk/motif/books/vol6A/ch-5.fm.html*/
static Widget
createScrollHelp(Widget w, const char *text, char *title)
{
	Widget dialog, pane, scrolledText, form, label, button;
	int n = 0;
	char titleDsp[FILE_NAME_LENGTH + 8];
	XmString titleString = NULL;

	(void) sprintf(titleDsp, "%s: %s", progDsp, title);
	dialog = XmCreateMessageDialog(w, titleDsp, NULL, 0);
	titleString = XmStringCreateSimple((char *) titleDsp);
	pane = XtVaCreateWidget("pane", xmPanedWindowWidgetClass, dialog,
		XmNsashWidth, 1, XmNsashHeight, 1, NULL);
	form = XtVaCreateWidget("form", xmFormWidgetClass, pane, NULL);
	label = XtVaCreateManagedWidget("label", xmLabelGadgetClass, form,
		XmNlabelType, XmPIXMAP,
		XmNlabelPixmap, pixmap,
		XmNleftAttachment, XmATTACH_FORM,
		XmNtopAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		NULL);
	XtSetArg(args[n], XmNdialogTitle, titleString); n++;
	XtSetArg(args[n], XmNscrollVertical, True); n++;
	XtSetArg(args[n], XmNscrollHorizontal, False); n++;
	XtSetArg(args[n], XmNeditMode, XmMULTI_LINE_EDIT); n++;
	XtSetArg(args[n], XmNeditable, False); n++;
	XtSetArg(args[n], XmNcursorPositionVisible, False); n++;
	XtSetArg(args[n], XmNwordWrap, False); n++;
	XtSetArg(args[n], XmNresizeWidth, True); n++;
	XtSetArg(args[n], XmNvalue, text); n++;
	XtSetArg(args[n], XmNrows, SCROLL_SIZE); n++;
	scrolledText = XmCreateScrolledText(form, "helpText", args, n);
	XtVaSetValues(XtParent(scrolledText),
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, label,
		XmNtopAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		NULL);
	XmStringFree(titleString);
	XtManageChild(scrolledText);
	XtManageChild(form);
	XtManageChild(pane);
	button = XmMessageBoxGetChild(dialog, XmDIALOG_CANCEL_BUTTON);
	XtUnmanageChild(button);
	button = XmMessageBoxGetChild(dialog, XmDIALOG_HELP_BUTTON);
	XtUnmanageChild(button);
	return dialog;
}

static void
helpMenuListener(Widget w, XtPointer value, XtPointer clientData)
{
	int val = (int) ((size_t) value);

	switch (val) {
	case 0:
		XtManageChild(descriptionDialog);
		break;
	case 1:
		XtManageChild(featuresDialog);
		break;
	case 2:
		XtManageChild(synopsisDialog);
		break;
	case 3:
		XtManageChild(optionsDialog);
		break;
	case 4:
		XtManageChild(referencesDialog);
		break;
	case 5:
		XtManageChild(aboutDialog);
		break;
	default:
		{
			char *buf;

			intCat(&buf, "helpMenuListener: %d", val);
			XtWarning(buf);
			free(buf);
		}
	}
}

#endif

int
main(int argc, char **argv)
{
	Boolean demo;
	int pixmapSize;
#ifdef HAVE_MOTIF
	Boolean teach, slot;
	int base = DEFAULT_BASE, displayBase = DEFAULT_BASE;
	int decimalPosition = DEFAULT_DECIMAL_POSITION;
	Widget menuBar, pullDownMenu, popupMenu, widget;
	Widget specialRailsMenu, museumMenuItem;
	Widget menuBarPanel, controlPanel;
	Widget controlRowCol, trackerRowCol;
	Widget auxForm;
	Widget displayFormatMenu;
	Widget teachMenu, effectsMenu;
#ifdef LEE_ABACUS
	Widget auxTrackerForm;
	Boolean lee;
	Pixel leftAuxColor, rightAuxColor;
	int leftAuxRails, rightAuxRails;
#endif
	Boolean rightToLeftAdd, rightToLeftMult;
	Boolean sound;
	XmString fileString, controlsString, learnString;
	XmString quitString, clearString, complementString;
	/*XmString modeString, incrementString, decrementString;*/
	XmString romanNumeralsString, ancientRomanString;
	XmString latinString, groupString, signString;
	XmString quarterString, quarterPercentString;
	XmString twelfthString, subdeckString, eighthString;
	XmString modernRomanString;
	XmString anomalyString, watchString;
	XmString rightToLeftAddString, rightToLeftMultString;
	XmString soundString, delayString;
	XmString formatString, formatStrings[MAX_MODES], museumString;
	XmString itString, ukString, frString;
	XmString beadControlString/*, basicFormatString*/;
	XmString baseSettingsString, displayFormatString;
	XmString specialRailsString, secondaryRailsString;
	XmString demoString, teachString;
	XmString teachOptionsString, effectsString;
#define OPTIONS_SIZE (sizeof (options1Help) + sizeof (options2Help) + sizeof (options3Help) + sizeof (options4Help) + 4)
	char optionsHelp[OPTIONS_SIZE];
	int rails, mode;
	int bottomPiece, topPiece, bottomPiecePercent, topPiecePercent;
	int subdeck, subbase, museum, anomaly, anomalySq;
	Boolean romanNumerals, ancientRoman, latin, modernRoman;
	Boolean group, sign;
	unsigned int i;
#endif

	progDsp = argv[0];
	topLevel = XtInitialize(argv[0], "Abacus",
		options, XtNumber(options), &argc, argv);
	if (argc != 1)
		usage(argv[0]);

#ifdef HAVE_MOTIF
	menuBarPanel = XtVaCreateManagedWidget("menuBarPanel",
		xmPanedWindowWidgetClass, topLevel,
		XmNseparatorOn, False,
		XmNsashWidth, 1,
		XmNsashHeight, 1,
		NULL);
	fileString = XmStringCreateSimple((char *) "File");
	controlsString = XmStringCreateSimple((char *) "Controls");
	learnString = XmStringCreateSimple((char *) "Learn");
	menuBar = XmVaCreateSimpleMenuBar(menuBarPanel, (char *) "menuBar",
		XmVaCASCADEBUTTON, fileString, 'F',
		XmVaCASCADEBUTTON, controlsString, 'C',
		XmVaCASCADEBUTTON, learnString, 'L',
		NULL);
	XmStringFree(fileString);
	XmStringFree(controlsString);
	quitString = XmStringCreateSimple((char *) "Exit");
	(void) XmVaCreateSimplePulldownMenu(menuBar, (char *) "fileMenu",
		0, fileMenuListener,
		XmVaSEPARATOR,
		XmVaPUSHBUTTON, quitString, 'x', NULL, NULL,
		NULL);
	XmStringFree(quitString);
	beadControlString = XmStringCreateSimple((char *) "Bead Control");
	baseSettingsString = XmStringCreateSimple((char *) "Base Settings");
#ifdef EXTRA
	basicFormatString = XmStringCreateSimple((char *) "Basic Format");
#endif
	displayFormatString = XmStringCreateSimple((char *) "Display Format");
	specialRailsString = XmStringCreateSimple((char *) "Special Rails");
	teachOptionsString = XmStringCreateSimple((char *) "Teach Options");
	effectsString = XmStringCreateSimple((char *) "Effects");
	clearString = XmStringCreateSimple((char *) "Clear");
	complementString = XmStringCreateSimple((char *) "Complement ~");
	/*modeString = XmStringCreateSimple((char *) "Format");
	incrementString = XmStringCreateSimple((char *) "Increment");
	decrementString = XmStringCreateSimple((char *) "Decrement");*/
	romanNumeralsString = XmStringCreateSimple((char *) "Roman Nvmerals");
	ancientRomanString = XmStringCreateSimple((char *) "Ancient Roman (");
	latinString = XmStringCreateSimple((char *) "Latin ~");
	groupString = XmStringCreateSimple((char *) "Group");
	signString = XmStringCreateSimple((char *) "Sign");
	quarterString = XmStringCreateSimple((char *) "Quarter");
	twelfthString = XmStringCreateSimple((char *) "Twelfth");
	secondaryRailsString = XmStringCreateSimple((char *) "Secondary Rails");
	quarterPercentString = XmStringCreateSimple((char *) "Quarter Percent");
	subdeckString = XmStringCreateSimple((char *) "Subdeck");
	eighthString = XmStringCreateSimple((char *) "Eighth");
	museumString = XmStringCreateSimple((char *) "Museum");
	itString = XmStringCreateSimple((char *) "Italian");
	ukString = XmStringCreateSimple((char *) "British");
	frString = XmStringCreateSimple((char *) "French");
	modernRomanString = XmStringCreateSimple((char *) "Modern Roman )");
	anomalyString = XmStringCreateSimple((char *) "Anomaly");
	watchString = XmStringCreateSimple((char *) "Watch");
	rightToLeftAddString = XmStringCreateSimple((char *) "Right To Left Add +");
	rightToLeftMultString = XmStringCreateSimple((char *) "Right To Left Multiplicand *");
	soundString = XmStringCreateSimple((char *) "Sound @");
	delayString = XmStringCreateSimple((char *) "Delay");
	demoString = XmStringCreateSimple((char *) "Demo");
	teachString = XmStringCreateSimple((char *) "Teach $");
	popupMenu = XmVaCreateSimplePulldownMenu(menuBar, (char *) "controlsMenu",
		1, controlMenuListener,
		XmVaCASCADEBUTTON, beadControlString, 'B',
#ifdef EXTRA
		XmVaCASCADEBUTTON, basicFormatString, 'F',
#endif
		XmVaPUSHBUTTON, baseSettingsString, 'S', NULL, NULL,
		XmVaCASCADEBUTTON, displayFormatString, 'D',
		XmVaCASCADEBUTTON, specialRailsString, 'R',
		XmVaCASCADEBUTTON, teachOptionsString, 'T',
		XmVaCASCADEBUTTON, effectsString, 'E',
		NULL);
	XmStringFree(beadControlString);
#ifdef EXTRA
	XmStringFree(basicFormatString);
#endif
	XmStringFree(baseSettingsString);
	XmStringFree(displayFormatString);
	XmStringFree(specialRailsString);
	XmStringFree(teachOptionsString);
	XmStringFree(effectsString);
	(void) XmVaCreateSimplePulldownMenu(popupMenu, (char *) "BeadControlMenu",
		0, beadControlMenuListener,
		XmVaPUSHBUTTON, clearString, 'C', NULL, NULL,
		XmVaPUSHBUTTON, complementString, '~', NULL, NULL,
		NULL);
	XmStringFree(clearString);
	XmStringFree(complementString);
#ifdef EXTRA
	(void) XmVaCreateSimplePulldownMenu(popupMenu, (char *) "BasicFormatMenu",
		1, basicFormatMenuListener,
		XmVaPUSHBUTTON, modeString, 'F', NULL, NULL,
		XmVaPUSHBUTTON, incrementString, 'I', NULL, NULL,
		XmVaPUSHBUTTON, decrementString, 'D', NULL, NULL,
		NULL);
	XmStringFree(modeString);
	XmStringFree(incrementString);
	XmStringFree(decrementString);
#endif
	displayFormatMenu = XmVaCreateSimplePulldownMenu(popupMenu, (char *) "DisplayFormatMenu",
		2, displayFormatMenuListener,
		XmVaTOGGLEBUTTON, romanNumeralsString, 'v', NULL, NULL,
		XmVaTOGGLEBUTTON, ancientRomanString, '(', NULL, NULL,
		XmVaTOGGLEBUTTON, latinString, '~', NULL, NULL,
		XmVaTOGGLEBUTTON, groupString, 'G', NULL, NULL,
		NULL);
	XmStringFree(romanNumeralsString);
	XmStringFree(ancientRomanString);
	XmStringFree(latinString);
	XmStringFree(groupString);
	specialRailsMenu = XmVaCreateSimplePulldownMenu(popupMenu, (char *) "SpecialRailsMenu",
		3, specialRailsMenuListener,
		XmVaTOGGLEBUTTON, signString, 'S', NULL, NULL,
		XmVaTOGGLEBUTTON, quarterString, 'u', NULL, NULL,
		XmVaTOGGLEBUTTON, quarterPercentString, 'P', NULL, NULL,
		XmVaTOGGLEBUTTON, twelfthString, 'T', NULL, NULL,
		XmVaCASCADEBUTTON, secondaryRailsString, 'R',
		XmVaTOGGLEBUTTON, modernRomanString, ')', NULL, NULL,
		XmVaTOGGLEBUTTON, anomalyString, 'l', NULL, NULL,
		XmVaTOGGLEBUTTON, watchString, 'W', NULL, NULL,
		NULL);
	XmStringFree(signString);
	XmStringFree(quarterString);
	XmStringFree(twelfthString);
	XmStringFree(secondaryRailsString);
	XmStringFree(modernRomanString);
	XmStringFree(anomalyString);
	XmStringFree(watchString);
	secondaryRailsMenu = XmVaCreateSimplePulldownMenu(specialRailsMenu, (char *) "SecondaryRailsMenu",
		4, secondaryRailsMenuListener,
		XmVaTOGGLEBUTTON, subdeckString, 'b', NULL, NULL,
		XmVaTOGGLEBUTTON, eighthString, 'E', NULL, NULL,
		XmVaCASCADEBUTTON, museumString, 'M',
		NULL);
	XmStringFree(quarterPercentString);
	XmStringFree(subdeckString);
	XmStringFree(eighthString);
	XmStringFree(museumString);
	museumMenu = XmVaCreateSimplePulldownMenu(secondaryRailsMenu, (char *) "MuseumMenu",
		2, museumMenuListener,
		XmVaRADIOBUTTON, itString, 'I', NULL, NULL,
		XmVaRADIOBUTTON, ukString, 'B', NULL, NULL,
		XmVaRADIOBUTTON, frString, 'F', NULL, NULL,
		XmNradioBehavior, True,
		XmNradioAlwaysOne, True,
		NULL);
	XmStringFree(itString);
	XmStringFree(ukString);
	XmStringFree(frString);
	teachMenu = XmVaCreateSimplePulldownMenu(popupMenu, (char *) "TeachMenu",
		4, teachOptionsMenuListener,
		XmVaTOGGLEBUTTON, rightToLeftAddString, '+', NULL, NULL,
		XmVaTOGGLEBUTTON, rightToLeftMultString, '*', NULL, NULL,
		NULL);
	XmStringFree(rightToLeftAddString);
	XmStringFree(rightToLeftMultString);
	effectsMenu = XmVaCreateSimplePulldownMenu(popupMenu, (char *) "EffectsMenu",
		5, effectsMenuListener,
		XmVaTOGGLEBUTTON, soundString, '@', NULL, NULL,
		XmVaPUSHBUTTON, delayString, 'D', NULL, NULL,
		NULL);
	XmStringFree(soundString);
	XmStringFree(delayString);
	learnString = XmStringCreateSimple((char *) "Learn");
	(void) XmVaCreateSimplePulldownMenu(menuBar, (char *) "LearnMenu",
		2, learnMenuListener,
		XmVaPUSHBUTTON, demoString, 'o', NULL, NULL,
		XmVaPUSHBUTTON, teachString, '$', NULL, NULL,
		NULL);
	XmStringFree(demoString);
	XmStringFree(teachString);
	XmStringFree(learnString);
	pullDownMenu = XmCreatePulldownMenu(menuBar,
		(char *) "helpPullDown", NULL, 0);
	widget = XtVaCreateManagedWidget("Help",
		xmCascadeButtonWidgetClass, menuBar,
		XmNsubMenuId, pullDownMenu,
		XmNmnemonic, 'H', NULL); /* mnemonic error on Cygwin */
	XtVaSetValues(menuBar, XmNmenuHelpWidget, widget, NULL);
	widget = XtVaCreateManagedWidget("Description",
		xmPushButtonGadgetClass, pullDownMenu,
		XmNmnemonic, 'D', NULL);
	XtAddCallback(widget, XmNactivateCallback, helpMenuListener, (char *) 0);
	widget = XtVaCreateManagedWidget("Features",
		xmPushButtonGadgetClass, pullDownMenu,
		XmNmnemonic, 'F', NULL);
	XtAddCallback(widget, XmNactivateCallback, helpMenuListener, (char *) 1);
	widget = XtVaCreateManagedWidget("Synopsis",
		xmPushButtonGadgetClass, pullDownMenu,
		XmNmnemonic, 'S', NULL);
	XtAddCallback(widget, XmNactivateCallback, helpMenuListener, (char *) 2);
	widget = XtVaCreateManagedWidget("Options",
		xmPushButtonGadgetClass, pullDownMenu,
		XmNmnemonic, 'O', NULL);
	XtAddCallback(widget, XmNactivateCallback, helpMenuListener, (char *) 3);
	widget = XtVaCreateManagedWidget("References",
		xmPushButtonGadgetClass, pullDownMenu,
		XmNmnemonic, 'R', NULL);
	XtAddCallback(widget, XmNactivateCallback, helpMenuListener, (char *) 4);
	widget = XtVaCreateManagedWidget("About",
		xmPushButtonGadgetClass, pullDownMenu,
		XmNmnemonic, 'A', NULL);
	XtAddCallback(widget, XmNactivateCallback, helpMenuListener, (char *) 5);
	XtManageChild(menuBar);
	clearDialog = createQuery(topLevel,
		(char *) "Are you sure you want to destroy the calculation?",
		(char *) "Clear Query",
		(XtCallbackProc) abacusClearListener);
	mainPanel = XtCreateManagedWidget("mainPanel",
		xmPanedWindowWidgetClass, menuBarPanel,
		NULL, 0);
	controlPanel = XtVaCreateManagedWidget("controlPanel",
		xmPanedWindowWidgetClass, mainPanel,
		XmNseparatorOn, False,
		XmNsashWidth, 1,
		XmNsashHeight, 1,
		NULL);
#ifdef MOUSEBITMAPS
	{
		/* Takes up valuable real estate. */
		Widget bitmapRowCol;
		Pixmap mouseLeftCursor, mouseRightCursor;
		Pixel fg, bg;

		bitmapRowCol = XtVaCreateManagedWidget("bitmapRowCol",
			xmRowColumnWidgetClass, controlPanel,
			XmNnumColumns, 4,
			XmNpacking, XmPACK_TIGHT,
			NULL);
		(void) XtVaGetValues(bitmapRowCol,
			XmNforeground, &fg,
			XmNbackground, &bg,
			NULL);
		mouseLeftCursor = XCreatePixmapFromBitmapData(
			XtDisplay(bitmapRowCol),
			RootWindowOfScreen(XtScreen(bitmapRowCol)),
			(char *) mouse_left_bits,
			mouse_left_width, mouse_left_height, fg, bg,
			DefaultDepthOfScreen(XtScreen(bitmapRowCol)));
		mouseRightCursor = XCreatePixmapFromBitmapData(
			XtDisplay(bitmapRowCol),
			RootWindowOfScreen(XtScreen(bitmapRowCol)),
			(char *) mouse_right_bits,
			mouse_right_width, mouse_right_height, fg, bg,
			DefaultDepthOfScreen(XtScreen(bitmapRowCol)));
		(void) XtVaCreateManagedWidget("mouseLeftText",
			xmLabelGadgetClass, bitmapRowCol,
			XtVaTypedArg, XmNlabelString,
			XmRString, "Move bead", 10,
			NULL);
		(void) XtVaCreateManagedWidget("mouseLeft",
			xmLabelGadgetClass, bitmapRowCol,
			XmNlabelType, XmPIXMAP,
			XmNlabelPixmap, mouseLeftCursor,
			NULL);
		(void) XtVaCreateManagedWidget("mouseRightText",
			xmLabelGadgetClass, bitmapRowCol,
			XtVaTypedArg, XmNlabelString,
			XmRString, "    Clear", 10,
			NULL);
		(void) XtVaCreateManagedWidget("mouseRight",
			xmLabelGadgetClass, bitmapRowCol,
			XmNlabelType, XmPIXMAP,
			XmNlabelPixmap, mouseRightCursor,
			NULL);
	}
#endif
	controlRowCol = XtVaCreateManagedWidget("controlRowCol",
		xmRowColumnWidgetClass, controlPanel,
		XmNnumColumns, 1,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		XmNisAligned, True,
		NULL);
	auxForm = XtVaCreateManagedWidget("auxForm",
		xmFormWidgetClass, mainPanel,
		NULL);
	abacus = XtVaCreateManagedWidget("abacus",
#ifdef LEE_KO
		XtNformat, "Korean",
#endif
		abacusWidgetClass, auxForm,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNtopAttachment, XmATTACH_POSITION,
		NULL);
	trackerRowCol = XtVaCreateManagedWidget("trackerRowCol",
		xmRowColumnWidgetClass, controlPanel, NULL);
#ifdef LEE_ABACUS
	XtVaGetValues(abacus,
		XtNlee, &lee,
		XtNleftAuxBeadColor, &leftAuxColor,
		XtNrightAuxBeadColor, &rightAuxColor,
		XtNleftAuxRails, &leftAuxRails,
		XtNrightAuxRails, &rightAuxRails,
		XtNdecimalPosition, &decimalPosition,
		XtNbase, &base,
		XtNdisplayBase, &displayBase,
		NULL);
	if (lee) {
		XtVaSetValues(auxForm,
			XmNfractionBase, leftAuxRails + rightAuxRails,
			NULL);
		auxTrackerForm = XtVaCreateManagedWidget("auxTrackerRowCol",
			xmFormWidgetClass, trackerRowCol,
			XmNfractionBase, leftAuxRails + rightAuxRails,
			NULL);
		leftAuxTracker = XtVaCreateManagedWidget("0.0",
			xmTextWidgetClass, auxTrackerForm,
			XmNtopAttachment, XmATTACH_FORM,
			XmNleftAttachment, XmATTACH_FORM,
			XmNrightAttachment, XmATTACH_POSITION,
			XmNbottomAttachment, XmATTACH_FORM,
			XmNrightPosition, leftAuxRails,
			NULL);
		XtAddCallback(leftAuxTracker, XmNactivateCallback,
			(XtCallbackProc) abacusMathListener, (XtPointer) NULL);
		rightAuxTracker = XtVaCreateManagedWidget("0.0",
			xmTextWidgetClass, auxTrackerForm,
			XmNtopAttachment, XmATTACH_FORM,
			XmNrightAttachment, XmATTACH_FORM,
			XmNleftAttachment, XmATTACH_WIDGET,
			XmNbottomAttachment, XmATTACH_FORM,
			XmNleftWidget, leftAuxTracker,
			NULL);
		XtAddCallback(rightAuxTracker, XmNactivateCallback,
			(XtCallbackProc) abacusMathListener, (XtPointer) NULL);
	}
#endif
	tracker = XtCreateManagedWidget("0.0",
		xmTextWidgetClass, trackerRowCol, NULL, 0);
	XtAddCallback(tracker, XmNactivateCallback,
		(XtCallbackProc) abacusMathListener, (XtPointer) NULL);
#ifdef LEE_ABACUS
	if (lee) {
		XtVaSetValues(abacus,
			XmNtopPosition, 5,
			NULL);
	}
	if (lee) {
		leftAuxAbacus = XtVaCreateManagedWidget("leftAuxAbacus",
			abacusWidgetClass, auxForm,
			XmNtopAttachment, XmATTACH_FORM,
			XmNleftAttachment, XmATTACH_FORM,
			XmNbottomAttachment, XmATTACH_WIDGET,
			XmNrightAttachment, XmATTACH_POSITION,
			XmNbottomWidget, abacus,
			XtNformat, "Japanese",
			XtNaux, LEFT_AUX,
			XtNprimaryBeadColor, leftAuxColor,
			XmNrightPosition, leftAuxRails,
			XtNrails, leftAuxRails,
			XtNdecimalPosition, decimalPosition,
			XtNbase, base,
			XtNdisplayBase, displayBase,
			NULL);
		XtAddCallback(leftAuxAbacus, XtNselectCallback,
			(XtCallbackProc) callbackAbacus, (XtPointer) NULL);
		rightAuxAbacus = XtVaCreateManagedWidget("rightAuxAbacus",
			abacusWidgetClass, auxForm,
			XmNtopAttachment, XmATTACH_FORM,
			XmNrightAttachment, XmATTACH_FORM,
			XmNbottomAttachment, XmATTACH_WIDGET,
			XmNbottomWidget, abacus,
			XmNleftAttachment, XmATTACH_WIDGET,
			XmNleftWidget, leftAuxAbacus,
			XtNformat, "Japanese",
			XtNaux, RIGHT_AUX,
			XtNprimaryBeadColor, rightAuxColor,
			XtNrails, rightAuxRails,
			XtNdecimalPosition, decimalPosition,
			XtNbase, base,
			XtNdisplayBase, displayBase,
			NULL);
		XtAddCallback(rightAuxAbacus, XtNselectCallback,
			(XtCallbackProc) callbackAbacus, (XtPointer) NULL);
	}
#endif
#else
	abacus = XtCreateManagedWidget("abacus",
		abacusWidgetClass, topLevel, NULL, 0);
#endif
	XtVaGetValues(abacus,
#ifdef HAVE_MOTIF
		XtNrails, &rails,
		XtNromanNumerals, &romanNumerals,
		XtNancientRoman, &ancientRoman,
		XtNlatin, &latin,
		XtNgroup, &group,
		XtNsign, &sign,
		XtNbottomPiece, &bottomPiece,
		XtNtopPiece, &topPiece,
		XtNbottomPiecePercent, &bottomPiecePercent,
		XtNtopPiecePercent, &topPiecePercent,
		XtNsubdeck, &subdeck,
		XtNmuseum, &museum,
		XtNmodernRoman, &modernRoman,
		XtNanomaly, &anomaly,
		XtNanomalySq, &anomalySq,
		XtNrightToLeftAdd, &rightToLeftAdd,
		XtNrightToLeftMult, &rightToLeftMult,
		XtNslot, &slot,
		XtNsubbase, &subbase,
		XtNbase, &base,
		XtNdisplayBase, &displayBase,
		XtNmode, &mode,
		XtNteach, &teach,
		XtNsound, &sound,
#endif
		XtNdemo, &demo,
		XtNpixmapSize, &pixmapSize,
		NULL);
#ifdef HAVE_XPM
	{
		XpmAttributes xpmAttributes;
		XpmColorSymbol transparentColor[1] = {{NULL,
			(char *) "none", 0 }};
		Pixel bg;

		xpmAttributes.valuemask = XpmColorSymbols | XpmCloseness;
		xpmAttributes.colorsymbols = transparentColor;
		xpmAttributes.numsymbols = 1;
		xpmAttributes.closeness = 40000;
		XtVaGetValues(topLevel, XtNbackground, &bg, NULL);
		transparentColor[0].pixel = bg;
		(void) XpmCreatePixmapFromData(XtDisplay(topLevel),
			RootWindowOfScreen(XtScreen(topLevel)),
			RESIZE_XPM(pixmapSize), &pixmap, NULL,
			&xpmAttributes);
	}
	if (pixmap == (Pixmap) NULL)
#endif
		pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
			RootWindowOfScreen(XtScreen(topLevel)),
			(char *) abacus_64x64_bits,
			abacus_64x64_width, abacus_64x64_height);
	XtVaSetValues(topLevel,
#ifdef HAVE_MOTIF
		XmNkeyboardFocusPolicy, XmPOINTER, /* not XmEXPLICIT */
#else
		XtNinput, True,
#endif
		XtNiconPixmap, pixmap, NULL);
	XtAddCallback(abacus, XtNselectCallback,
		(XtCallbackProc) callbackAbacus, (XtPointer) NULL);

#ifdef HAVE_MOTIF
	strcpy(descriptionHelp, description1Help);
	strcat(descriptionHelp, "\n");
	strcat(descriptionHelp, description2Help);
	descriptionDialog = createScrollHelp(menuBar, (char *) descriptionHelp,
		(char *) "Description");
	featuresDialog = createScrollHelp(menuBar, (char *) featuresHelp,
		(char *) "Features");
	synopsisDialog = createHelp(menuBar, (char *) synopsisHelp,
		(char *) "Synopsis");
	strcpy(optionsHelp, options1Help);
	strcat(optionsHelp, "\n");
	strcat(optionsHelp, options2Help);
	strcat(optionsHelp, "\n");
	strcat(optionsHelp, options3Help);
	strcat(optionsHelp, "\n");
	strcat(optionsHelp, options4Help);
	optionsDialog = createScrollHelp(menuBar, (char *) optionsHelp,
		(char *) "Options");
	referencesDialog = createHelp(menuBar, (char *) referencesHelp,
		(char *) "References");
	aboutDialog = createHelp(menuBar, (char *) aboutHelp,
		(char *) "About");
	baseDialog = createBaseChangers(topLevel, (char *) "Base");
	delayDialog = createDelayChanger(topLevel, (char *) "Slider");
	XtManageChild(XmCreateLabel(controlRowCol, "Abacus Size:", NULL, 0));
	sizeChanger = XtVaCreateManagedWidget("rails",
#ifdef USE_SPIN
		xmSimpleSpinBoxWidgetClass, controlRowCol,
		XmNspinBoxChildType, XmNUMERIC,
		XmNminimumValue, MIN_RAILS,
		XmNmaximumValue, MAX_RAILS,
		XmNposition, rails,
		XmNincrementValue, 1,
		XmNcolumns, 2,
#else
		xmScaleWidgetClass, controlRowCol,
		XmNminimum, MIN_RAILS,
		XmNmaximum, MAX_RAILS,
		XmNvalue, rails,
		XmNscaleWidth, (MAX_RAILS - MIN_RAILS + 1) * 6,
#endif
		/*XtVaTypedArg, XmNtitleString, XmRString, "      Abacus Size", 18,*/
		XmNshowValue, True,
		XmNorientation, XmHORIZONTAL, NULL);
	XtAddCallback(sizeChanger, XmNvalueChangedCallback,
		(XtCallbackProc) railChangeListener, (XtPointer) NULL);
	baseSet = ((base != DEFAULT_BASE || displayBase != DEFAULT_BASE) &&
		!demo);
	if (mode < 0 || mode > MAX_FORMATS)
		mode = GENERIC;
	if (demo && mode == GENERIC)
		mode = CHINESE;
	formatString = XmStringCreateSimple((char *) " Format:");
	formatSubMenu = XmCreatePulldownMenu(controlRowCol,
		(char *) "formatSubMenu", NULL, 0);
	for (i = 0; i < MAX_MODES; i++) {
		formatStrings[i] =
			XmStringCreateSimple((char *) formatChoices[i]);
		XtSetArg(args[0], XmNlabelString, formatStrings[i]);
		if (i == ROMAN) {
			XtSetArg(args[1], XmNmnemonic, 'H'); /* Hand */
		} else {
			XtSetArg(args[1], XmNmnemonic, formatChoices[i][0]);
		}
		formatOptions[i] = XmCreatePushButtonGadget(formatSubMenu,
			(char *) formatChoices[i], args, 2);
		if (i == GENERIC)
			XtSetSensitive(formatOptions[i], False);
		XtAddCallback(formatOptions[i],
			XmNactivateCallback, formatListener, (char *) ((size_t) i));
	}
	XtManageChildren(formatOptions, MAX_MODES);
	XtSetArg(args[0], XmNlabelString, formatString);
	XtSetArg(args[1], XmNmnemonic, 'F');
	XtSetArg(args[2], XmNsubMenuId, formatSubMenu);
	XtSetArg(args[3], XmNmenuHistory, formatOptions[mode]);
	formatMenu = XmCreateOptionMenu(controlRowCol, (char *) "FormatMenu",
		args, 4);
	for (i = 0; i < MAX_MODES; i++)
		XmStringFree(formatStrings[i]);
	XmStringFree(formatString);
	XtManageChild(formatMenu);
	if (!baseSet) {
		forceNormalParams(abacus);
	}
	if ((romanNumeralsMenuItem = XtNameToWidget(displayFormatMenu, "button_0")) !=
			(Widget) 0) {
		if (romanNumerals)
			XtVaSetValues(romanNumeralsMenuItem,
				XmNset, romanNumerals, NULL);
		XtAddCallback(romanNumeralsMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) romanNumeralsToggle, (XtPointer) NULL);
	}
	if ((ancientRomanMenuItem = XtNameToWidget(displayFormatMenu, "button_1")) !=
			(Widget) 0) {
		if (ancientRoman)
			XtVaSetValues(ancientRomanMenuItem,
				XmNset, ancientRoman, NULL);
		XtAddCallback(ancientRomanMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) ancientRomanToggle, (XtPointer) NULL);
	}
	if ((latinMenuItem = XtNameToWidget(displayFormatMenu, "button_2")) !=
			(Widget) 0) {
		if (latin)
			XtVaSetValues(latinMenuItem,
				XmNset, latin, NULL);
		XtAddCallback(latinMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) latinToggle, (XtPointer) NULL);
	}
	if ((groupMenuItem = XtNameToWidget(displayFormatMenu, "button_3")) !=
			(Widget) 0) {
		if (group)
			XtVaSetValues(groupMenuItem,
				XmNset, group, NULL);
		XtAddCallback(groupMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) groupToggle, (XtPointer) NULL);
	}
	if ((signMenuItem = XtNameToWidget(specialRailsMenu, "button_0")) !=
			(Widget) 0) {
		if (sign)
			XtVaSetValues(signMenuItem,
				XmNset, sign, NULL);
		XtAddCallback(signMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) signToggle, (XtPointer) NULL);
	}
	if ((quarterMenuItem = XtNameToWidget(specialRailsMenu, "button_1")) !=
			(Widget) 0) {
		if (SET_QUARTER(bottomPiece, topPiece))
			XtVaSetValues(quarterMenuItem,
				XmNset, True, NULL);
		XtAddCallback(quarterMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) quarterToggle, (XtPointer) NULL);
	}
	if ((quarterPercentMenuItem = XtNameToWidget(specialRailsMenu, "button_2")) !=
			(Widget) 0) {
		if (SET_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, topPiecePercent))
			XtVaSetValues(quarterPercentMenuItem,
				XmNset, True, NULL);
		XtAddCallback(quarterPercentMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) quarterPercentToggle, (XtPointer) NULL);
	}
	if ((twelfthMenuItem = XtNameToWidget(specialRailsMenu, "button_3")) !=
			(Widget) 0) {
		if (SET_TWELFTH(bottomPiece, topPiece))
			XtVaSetValues(twelfthMenuItem,
				XmNset, True, NULL);
		XtAddCallback(twelfthMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) twelfthToggle, (XtPointer) NULL);
	}
	if ((subdeckMenuItem = XtNameToWidget(secondaryRailsMenu, "button_0")) !=
			(Widget) 0) {
		if (SET_SUBDECK(bottomPiece, bottomPiecePercent, subdeck, subbase, slot))
			XtVaSetValues(subdeckMenuItem,
				XmNset, True, NULL);
		XtAddCallback(subdeckMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) subdeckToggle, (XtPointer) NULL);
	}
	if ((eighthMenuItem = XtNameToWidget(secondaryRailsMenu, "button_1")) !=
			(Widget) 0) {
		if (SET_EIGHTH(bottomPiece, bottomPiecePercent, subdeck, subbase, slot))
			XtVaSetValues(eighthMenuItem,
				XmNset, True, NULL);
		XtAddCallback(eighthMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) eighthToggle, (XtPointer) NULL);
	}
	if ((museumMenuItem = XtNameToWidget(museumMenu, "button_0"))
			!= (Widget) 0) {
		XtVaSetValues(museumMenuItem,
			XmNset, museum, NULL);
		XtAddCallback(museumMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) museumChoice, (XtPointer) NULL);
	}
	if ((modernRomanMenuItem = XtNameToWidget(specialRailsMenu, "button_5")) !=
			(Widget) 0) {
		if (modernRoman)
			XtVaSetValues(modernRomanMenuItem,
				XmNset, modernRoman, NULL);
		XtAddCallback(modernRomanMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) modernRomanToggle, (XtPointer) NULL);
	}
	if ((anomalyMenuItem = XtNameToWidget(specialRailsMenu, "button_6")) !=
			(Widget) 0) {
		if (SET_ANOMALY(anomaly, anomalySq))
			XtVaSetValues(anomalyMenuItem,
				XmNset, True, NULL);
		XtAddCallback(anomalyMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) anomalyToggle, (XtPointer) NULL);
	}
	if ((watchMenuItem = XtNameToWidget(specialRailsMenu, "button_7")) !=
			(Widget) 0) {
		if (SET_WATCH(anomaly, anomalySq))
			XtVaSetValues(watchMenuItem,
				XmNset, True, NULL);
		XtAddCallback(watchMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) watchToggle, (XtPointer) NULL);
	}
	if ((rightToLeftAddMenuItem = XtNameToWidget(teachMenu, "button_0")) !=
			(Widget) 0) {
		if (rightToLeftAdd)
			XtVaSetValues(rightToLeftAddMenuItem,
				XmNset, rightToLeftAdd, NULL);
		XtAddCallback(rightToLeftAddMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) rightToLeftAddToggle, (XtPointer) NULL);
	}
	if ((rightToLeftMultMenuItem = XtNameToWidget(teachMenu, "button_1")) !=
			(Widget) 0) {
		if (rightToLeftMult)
			XtVaSetValues(rightToLeftMultMenuItem,
				XmNset, rightToLeftMult, NULL);
		XtAddCallback(rightToLeftMultMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) rightToLeftMultToggle, (XtPointer) NULL);
	}
	if ((soundMenuItem = XtNameToWidget(effectsMenu, "button_0"))
			!= (Widget) 0) {
		if (sound)
			XtVaSetValues(soundMenuItem,
				XmNset, sound, NULL);
		XtAddCallback(soundMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) soundToggle, (XtPointer) NULL);
	}
#if 0
	if ((verticalMenuItem = XtNameToWidget(effectsMenu, "button_0"))
			!= (Widget) 0) {
		if (vertical)
			XtVaSetValues(verticalMenuItem,
				XmNset, vertical, NULL);
		XtAddCallback(verticalMenuItem, XmNvalueChangedCallback,
			(XtCallbackProc) verticalToggle, (XtPointer) NULL);
	}
#endif
	XmToggleButtonSetState(subdeckMenuItem,
		SET_SUBDECK(bottomPiece, bottomPiecePercent, subdeck, subbase, slot), False);
	XmToggleButtonSetState(eighthMenuItem,
		SET_EIGHTH(bottomPiece, bottomPiecePercent, subdeck, subbase, slot), False);
	XmToggleButtonSetState(anomalyMenuItem,
		SET_ANOMALY(anomaly, anomalySq), False);
	XmToggleButtonSetState(watchMenuItem,
		SET_WATCH(anomaly, anomalySq), False);
	XtVaSetValues(museumMenu, XmNmenuHistory, museum, NULL);
	XtSetSensitive(ancientRomanMenuItem,
		ENABLE_ANCIENT_ROMAN(romanNumerals));
	XtSetSensitive(latinMenuItem,
		ENABLE_LATIN(romanNumerals, bottomPiece));
	XtSetSensitive(quarterMenuItem,
		ENABLE_QUARTER(bottomPiece));
	XtSetSensitive(quarterPercentMenuItem,
		ENABLE_QUARTER_PERCENT(bottomPiece, bottomPiecePercent, decimalPosition));
	XtSetSensitive(twelfthMenuItem,
		ENABLE_TWELFTH(bottomPiece));
	XtSetSensitive(secondaryRailsMenu,
		ENABLE_SECONDARY(bottomPiece, bottomPiecePercent, decimalPosition, slot));
	XtSetSensitive(subdeckMenuItem,
		ENABLE_SUBDECK(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
	XtSetSensitive(eighthMenuItem,
		ENABLE_EIGHTH(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, subbase, slot));
	XtSetSensitive(museumMenu,
		ENABLE_MUSEUM(bottomPiece, bottomPiecePercent, decimalPosition, subdeck, slot));
	XtSetSensitive(modernRomanMenuItem,
		ENABLE_MODERN_ROMAN(slot, anomaly, anomalySq));
	XtSetSensitive(anomalyMenuItem,
		ENABLE_ANOMALY(anomaly, anomalySq));
	XtSetSensitive(watchMenuItem,
		ENABLE_WATCH(anomaly, anomalySq));
#endif
	initialize(abacus);
	if (demo)
		forceDemoParams(abacus);
#ifdef HAVE_MOTIF
	else if (teach)
		forceTeachParams(abacus);
#endif
	if (demo) {
		createDemo();
#ifdef HAVE_MOTIF
	} else if (teach) {
		createTeach();
#endif
	}
	XtRealizeWidget(topLevel);
	VOID XGrabButton(XtDisplay(abacus), (unsigned int) AnyButton,
		AnyModifier, XtWindow(abacus), TRUE,
		(unsigned int) (ButtonPressMask |
		ButtonMotionMask | ButtonReleaseMask),
		GrabModeAsync, GrabModeAsync, XtWindow(abacus),
		XCreateFontCursor(XtDisplay(abacus), XC_hand2));
	if (demo) {
		realizeDemo();
	}
	XtMainLoop();

#ifdef VMS
	return 1;
#else
	return 0;
#endif
}
#endif
