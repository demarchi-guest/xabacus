xabacus
=======

An Abacus Widget

Web: http://www.sillycycle.com/abacus.html

Download: Primary site: http://www.sillycycle.com/abacus/
          Secondary site: http://ibiblio.org/pub/Linux/apps/math

Maintainer: David A. Bagley <bagleyd AT verizon.net>

  By default, this is an implementation of the classic Chinese abacus
  (Suanpan).  Other implementations are easily configurable including
  the Japanese Abacus (Soroban), the Korean Abacus (Jupan), the Roman
  Hand-abacus, the Russian Abacus (Schety), and the Danish Learning
  Abacus.

  Essentially, this is a proof by induction that a computer is more
  powerful than an abacus, since a computer program can contain an abacus.
  (But then again, you can simulate a computer within a computer, so what
  does that show).  Actually, with a real abacus, one can move more than
  one row at a time with 10 fingers.  But on the other hand, a real abacus
  does not have the current sum displayed as an integer.

  To build you need at minimum both of these, X11 and Xt (unless you are
  looking to build win32).
    sudo apt-get install libx11-dev
    sudo apt-get install libxt-dev
  You may need to install one of these to build Motif version:
    sudo apt-get install libmotif-dev (strongly recommended)
    sudo apt-get install lesstif2-dev (defunct)
  You need to install XPM if you want color icons:
    sudo apt-get install libxpm-dev (strongly recommended)
  To install XMU if you want editres:
    sudo apt-get install libxmu-dev (probably not useful)
  To install sound libraries (defunct):
    sudo apt-get install librplay3-dev
    sudo apt-get install libesd0-dev
    sudo apt-get install libaudiofile-dev
    sudo apt-get install nas-dev
    sudo apt-get install pulse-dev
      In place of these it is just configured by default with
      --enable-def-play=play.sh
  You need to install XML reader for demo:
    sudo apt-get install libxml2-dev (recommended)
      Without this compiled in it falls back to a brief static demo.
      With this compiled in it parses the abacusDemo.xml file to read in
      all the demos.
  Then should be a simple
    configure; make

  Try resizing the abacus. Notice the shape and size change of the beads.

  Try editing the file Abacus.ad to get a certain feature and then copying
  to $(HOME)/Abacus (where Abacus is the file name) and running xabacus
  again.  There is also a Windows port (wabacus), though not all features
  are included.

  Keep in mind that the Abacus is a widget that can be simply stuck in
  other X programs. (If you find a use for this, let me know).

  It has a built-in demo-mode that instructs the user:
  - how to count (from 1 to 20) using the abacus
  - how to perform simple additions
  - how to perform simple subtractions.
  - how to multiply... works best on Lee's Abacus

  There is also a teach-mode feature that teaches the user the proper way
  to carry out calculations supplied by the user for:
  - addition
  - subtraction
  - multiplication
  - division
  - square root
  - cube root

  In addition, there is a rudimentary facility for generating data that
  may be used to write customized scripts for teaching other concepts.

  The PostScript document, Abacus.ps, included in the distribution,
  describes every possible technique for performing additions and
  subtractions using an abacus.

  Refer to the man page for detailed command line options.

FOUND A BUG?

  Send bugs reports and fixes to the author. For bugs, please include
  as much information as to their location as possible, since I may not
  be able to duplicate them.
