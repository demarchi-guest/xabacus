/*
 * @(#)sound.h
 *
 * Taken from xlock, many authors...
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Header file for sound */

#ifndef _sound_h
#define _sound_h

#if defined(USE_RPLAY) || defined(USE_NAS) || defined(USE_VMSPLAY) || defined(USE_ESOUND) || defined (WINVER) || defined(DEF_PLAY)
#define USE_SOUND

#ifndef SOUNDPATH
#ifdef WINVER
#ifdef UNIXDELIM
#define SOUNDPATH "c:/WINDOWS"
#else
#define SOUNDPATH "c:\\WINDOWS"
#endif
#else
#ifdef VMS
#define SOUNDPATH "[.]"
#else
#if 0
#define SOUNDPATH "/usr/share/games/xabacus"
#else
#define SOUNDPATH "/usr/local/share/games/xabacus"
#endif
#endif
#endif
#endif

#ifndef SOUNDEXT
#ifdef WINVER
#define SOUNDEXT ".wav"
#else
#define SOUNDEXT ".au"
#endif
#endif

#ifdef USE_ESOUND
extern int init_sound(void);
extern void shutdown_sound(void);
#endif

extern void playSound(const char *fileName);
#endif

#endif /* _sound_h */
